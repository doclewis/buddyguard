#ifdef __OBJC__
#import <UIKit/UIKit.h>
#endif

#import "AFHTTPRequestOperationLogger.h"

FOUNDATION_EXPORT double AFHTTPRequestOperationLoggerVersionNumber;
FOUNDATION_EXPORT const unsigned char AFHTTPRequestOperationLoggerVersionString[];

