//
//  Utility.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 5/5/17.
//  Copyright © 2017 Stewart Boyd. All rights reserved.
//

import Foundation


class Utility {
    
    
    static func Read(fpath: String?) -> String?{
        
        if let f = fpath {
            let url = URL(fileURLWithPath: f)
            //reading
            do {
                return try String(contentsOf: url, encoding: String.Encoding.utf8)
            }
            catch {
                return nil
            }
        }
        return nil
    }
}
