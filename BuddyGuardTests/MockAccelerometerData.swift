//
//  MockAccelerometerData.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 12/12/16.
//  Copyright © 2016 Stewart Boyd. All rights reserved.
//

import Foundation
import CoreMotion

class MockAccelerometerData : CMAccelerometerData {
    
    var _acceleration : CMAcceleration = CMAcceleration(x: 0, y: 0, z: -1)
    override var acceleration : CMAcceleration {
        return _acceleration
    }
    init?(acceleration : CMAcceleration){
        _acceleration = acceleration
        let aDecoder = MockNSCoder()
        super.init(coder: aDecoder)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
