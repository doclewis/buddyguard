//
//  MockAlarmPlayOnDelayCallback.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 12/30/16.
//  Copyright © 2016 Stewart Boyd. All rights reserved.
//

import Foundation
import XCTest

class MockAlarmPlayOnDelayCallback{
    var finishedPlayingExpectation : XCTestExpectation!;
    var didFinishPlaying = false

    func playOnDelayCallback(_ flag: Bool?) {
        if (flag == true){
            self.finishedPlayingExpectation.fulfill()
            didFinishPlaying = true
        }
        else {
            print("flag was fucked -> \(flag)")
        }
    }
}
