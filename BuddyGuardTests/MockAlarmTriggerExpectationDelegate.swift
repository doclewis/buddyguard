//
//  MockAlarmTriggerExpectationDelegate.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 12/12/16.
//  Copyright © 2016 Stewart Boyd. All rights reserved.
//

import Foundation
import XCTest
@testable import BuddyGuard

class MockAlarmTriggerExpectationDelegate : AlarmTriggerableDelegate{
    var isTriggerCalled = false
    var alarmTriggeredExpectation : XCTestExpectation?
    
    func startedReading(){}
    
    func stoppedReading(){}
    
    func initializedInitialState(_ initialState : GeneralState?){}
    
    func triggered(){
        print("triggered")
        alarmTriggeredExpectation?.fulfill()
        isTriggerCalled = true
    }
}
