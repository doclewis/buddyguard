//
//  MockMotionManager.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 12/12/16.
//  Copyright © 2016 Stewart Boyd. All rights reserved.
//  This Mock CMMotionManager class uses data from a csv
//  to mock the data that generally comes from an instance of
//  CMMotionManger

import Foundation
import CoreMotion

class MockMotionManager : CMMotionManager {
    
    var csvContentsConverted : [[Double]] = [[]]
    var currentCount = 0
    var isAccelerometerUpdating : Bool = false
    var dataTuple : (Double, Double, Double) {
        /*
         ``Returns``
         This method uses the currentCount property in conjuction with
         the dataCount property to return a tuple of the accelerometer
         data stored in a csv file. If the currentCount > dataCount (past
         end of csv data) then the last item is returned. If there is no data
         then a standard 1g down data is returned.
         */
        if dataCount == 0 {
            return (0.0, 0.0, -1.0)
        }
        else{
            if currentCount >= dataCount {
                return (csvContentsConverted[dataCount - 1][1], csvContentsConverted[dataCount - 1][2], csvContentsConverted[dataCount - 1][3])
            }
            else{
                return (csvContentsConverted[currentCount][1], csvContentsConverted[currentCount][2], csvContentsConverted[currentCount][3])
            }
        }
    }
    
    var x : Double{
        
        return dataTuple.0
    }
    
    var y : Double {
        return dataTuple.1
    }
    
    var z : Double {
        return dataTuple.2
    }
    override var isAccelerometerAvailable : Bool {
        return true
    }
    
    var dataCount: Int {
        return csvContentsConverted.count
    }
    
    
    override func startAccelerometerUpdates(to queue: OperationQueue, withHandler handler: @escaping CMAccelerometerHandler) {
        print("startAccelerometerUpdatesToQueue")
        currentCount = 0
        isAccelerometerUpdating = true
        let operation = BlockOperation{
            print("start block")
            while self.isAccelerometerUpdating {
                print("\(self.x), \(self.y), \(self.z)")
                let acceleration = CMAcceleration(x: self.x, y: self.y, z: self.z)
                let accelerometerData = MockAccelerometerData(acceleration: acceleration)
                handler(accelerometerData, nil)
                self.currentCount += 1
            }
        }
        queue.addOperation(operation)
    }
    
    override func stopAccelerometerUpdates() {
        isAccelerometerUpdating = false
    }
    
    func csvContents(_ fileURL : URL){
        csvContentsConverted = []
        if let data = try? Data(contentsOf: fileURL) {
            if let content = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
                let lines:[String] = content.components(separatedBy: CharacterSet.newlines) as [String]
                //existing code
                var lineNumber = 0
                for line in lines {
                    if line != ""{
                        let split_line = line.components(separatedBy: ",")
                        assert(split_line.count == 4)
                        var out_line : [Double] = []
                        for elem in split_line{
                            let trimmedElem = elem.strip()
                            if let double = Double(trimmedElem){
                                out_line.append(double)
                            }
                        }
                        
                        if out_line.count == 4{
                            csvContentsConverted.append(out_line)
                            //print(csvContentsConverted[lineNumber])
                            lineNumber += 1
                        }
                    }
                }
            }
        }
    }
}
