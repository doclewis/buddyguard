//
//  MockAVAudioPlayerDelegate.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 12/12/16.
//  Copyright © 2016 Stewart Boyd. All rights reserved.
//

import Foundation
import AVFoundation
import XCTest

class MockAVAudioPlayerDelegate: UIViewController, AVAudioPlayerDelegate {
    var finishedPlayingExpectation : XCTestExpectation!;
    var didFinishPlaying = false
    @objc func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        
        if (flag == true){
            print("Did finish playing")
            finishedPlayingExpectation.fulfill()
            didFinishPlaying = true
        }
    }
}
