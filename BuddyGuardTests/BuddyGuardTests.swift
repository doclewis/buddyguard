//
//  BuddyGuardTests.swift
//  BuddyGuardTests
//
//  Created by Stewart Boyd on 5/23/16.
//  Copyright © 2016 Stewart Boyd. All rights reserved.
//

import XCTest
import AVFoundation
import UIKit
import CoreMotion
import Foundation
@testable import BuddyGuard



class BuddyGuardTests: XCTestCase {

    var viewController : MainViewController!
    var filePath: String = Bundle.main.path(forResource: "test", ofType: "wav")!
    var audioPlayer: AudioPlayer!
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        viewController = MainViewController()
        filePath = Bundle.main.path(forResource: "test", ofType: "wav")!
        audioPlayer = try! AudioPlayer(filepath: filePath)
        //need to access view to trigger viewDidLoad
        let _ = viewController.view
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testAlarmInit() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let alarm = Alarm(audioPlayer: audioPlayer)
        if let played = alarm.start() {
            XCTAssertTrue(played, "play should return true");
        } else {
            XCTAssertTrue(false, "play should return a bool not nil")
        }
    }

    func testAlarmStop() {
        let alarm = Alarm(audioPlayer: audioPlayer, delay: 0.5, doesLEDDisplay: false, doesVibrate: false, runLoop: RunLoop.main)
        alarm.delay = 0.3
        alarm.startOnDelay(runLoop: RunLoop.main)
    }

    /// There was a bug where if startOnDelay was executed
    /// and stop was then executed before the delay had occurred
    /// This tests for that
    func testAlarmStopIfStartedOnDelayButNotTrulyStarted() {
        
        let finishedPlayingExpectation = expectation(description: "The play should finish")
        let alarm = Alarm(audioPlayer: audioPlayer, delay: 0.3, doesLEDDisplay: false, doesVibrate: false, runLoop: RunLoop.main)
        let mockAlarmPlayOnDelayCallback = MockAlarmPlayOnDelayCallback()
        mockAlarmPlayOnDelayCallback.finishedPlayingExpectation = finishedPlayingExpectation
        alarm.playOnDelayCallback = mockAlarmPlayOnDelayCallback.playOnDelayCallback
        alarm.startOnDelay(runLoop: RunLoop.main)
        alarm.stop()
        let when = DispatchTime.now() + alarm.delay + 0.1 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            mockAlarmPlayOnDelayCallback.finishedPlayingExpectation.fulfill()
        }
        //exepcation should have been fufiled by DispatchQueue
        waitForExpectations(timeout: alarm.delay + 0.2) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
        //confirm that didFinishPlaying is still false and thus was never called
        XCTAssertFalse(mockAlarmPlayOnDelayCallback.didFinishPlaying)
    }

    func testAudioPlayerIntConvenience() {
        //When constructiting using convenience class a fader
        //attribute is automatically made
        let fromVolume : Float = 0.2;
        let toVolume : Float = 0.8;
        let audioPlayer = try! AudioPlayer(filepath: filePath, fromVolume: fromVolume, toVolume: toVolume)
        //let alarm = try! Alarm(fromVolume: fromVolume, toVolume: toVolume)
        XCTAssertNotNil(audioPlayer.fader);
        XCTAssertEqual(audioPlayer.fader?.fromVolume, fromVolume)
        XCTAssertEqual(audioPlayer.fader?.toVolume, toVolume);
    }

    func testAudioPlayerRepeatCalculatedProperty() {
        // -1 means repat indefintely for the AVAudioPlayer class
        let audioPlayer = try! AudioPlayer(filepath: filePath, volume: 1.0, numberOfLoops: -1)
        XCTAssertTrue(audioPlayer.doesRepeatIndefinitely)
        //COnfirm that the default doesn't loop indefinitely
        let audioPlayer1 = try! AudioPlayer(filepath: filePath, volume: 1.0)
        //let alarm1 = try! Alarm(volume : 0.9)
        XCTAssertFalse(audioPlayer1.doesRepeatIndefinitely)
        //verify taht afintie number doesn't return that  aloop happens indefintely
        let audioPlayer2 = try! AudioPlayer(filepath: filePath, volume: 1.0, numberOfLoops: 4)
        XCTAssertFalse(audioPlayer2.doesRepeatIndefinitely)
    }

    func testAudioPlayerWithBrokenFilePath(){
        do{
            //this should raise an exception
            let _ = try AudioPlayer(filepath: "fake_file_path")
            XCTFail();
        }
        catch let error as NSError {
            XCTAssertTrue(true);
        }
        
            //XCTAssertFalse(alarm.play()!, "play should fail because not an extant resource")
    }

    func testAlarmWithAbove1Vol(){
        let audioPlayer = try! AudioPlayer(filepath: filePath, volume : 3.0);
        XCTAssertEqual(audioPlayer.volume, VolumeLimit.maxVolume);
    }

    func testAlarmBelow0Vol(){
        let audioPlayer = try! AudioPlayer(filepath: filePath, volume : -1.0);
        XCTAssertEqual(audioPlayer.volume, VolumeLimit.minVolume);
    }

    /*
    func testAlarmPlay(){
        let finishedPlayingExpectation = expectation(description: "The play should finish")

        class MockAVAudioPlayerDelegate: UIViewController, AVAudioPlayerDelegate {
            var finishedPlayingExpectation : XCTestExpectation!;

            @objc func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
                
                if (flag == true){
                    finishedPlayingExpectation.fulfill()
                }
            }
        }
        let delegate = MockAVAudioPlayerDelegate(nibName: nil, bundle: nil)
        let fpath = Bundle.main.path(forResource: "test", ofType: "wav")!;
        let alarm = try! Alarm(fpath: fpath, volume: 1.0)
        delegate.finishedPlayingExpectation = finishedPlayingExpectation;
        XCTAssertGreaterThan(alarm.duration, 0.9)
        alarm.delegate = delegate
        //Stewart Plays the Alarm
        alarm.play()
        //The alarm play is asynchronous. This method will block until the
        //expecation is fufilled (done by delegate)
        waitForExpectations(timeout: alarm.duration + 0.2) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    */
    func testAudioPlayerWithDelay(){
        let finishedPlayingExpectation = expectation(description: "The play should finish")
        let delay = 0.2
        
        class MockTestCB{
            var finishedPlayingExpectation : XCTestExpectation!;
            var timeSinceInitiation : Double?
            fileprivate let timeInitiated : Date
            init(){
                //Get the time the object was created
                timeInitiated = Date()
            }
            func startedPlaying(_ flag: Bool?) {
                
                if (flag == true){
                    self.timeSinceInitiation = Date().timeIntervalSince(self.timeInitiated)
                    self.finishedPlayingExpectation.fulfill()
                }
            }
        }

        let fpath = Bundle.main.path(forResource: "test", ofType: "wav")!
        let audioPlayer = try! AudioPlayer(filepath: fpath, volume: 1.0)
        let alarm = Alarm(audioPlayer: audioPlayer, delay: delay)
        let mockTestCB = MockTestCB()
        mockTestCB.finishedPlayingExpectation = finishedPlayingExpectation
        alarm.playOnDelayCallback = mockTestCB.startedPlaying
        //Stewart Plays the Alarm
        alarm.startOnDelay()
        //The alarm play is asynchronous. This method will block until the
        //expecation is fufilled (callback function will fufill))
        waitForExpectations(timeout: delay) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
        //Assert that delay before play was at least what was prescribed
        
        XCTAssertGreaterThan(mockTestCB.timeSinceInitiation!, delay)
    }

    func testAudioPlayerSetFpath(){
        let fpath = Bundle.main.path(forResource: "test", ofType: "mp3")
        let audioPlayer = try! AudioPlayer(filepath: fpath!, volume: 1.0)
        XCTAssertEqual(audioPlayer.filepath!, fpath!)
        XCTAssertEqual(audioPlayer.volume, 1.0);
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
}


class AlarmTests: XCTestCase {

    var alarm: Alarm!
    var audioPlayer: AudioPlayer!
    var filePath: String = Bundle.main.path(forResource: "test", ofType: "wav")!
    var mockAlarmPlayOnDelayCallback: MockAlarmPlayOnDelayCallback!
    var specifiedDelay = 0.5
    override func setUp() {
        super.setUp()
        audioPlayer = try! AudioPlayer(filepath: filePath)
        alarm = Alarm(audioPlayer: audioPlayer, delay: specifiedDelay, percepts: [])
        mockAlarmPlayOnDelayCallback = MockAlarmPlayOnDelayCallback()
        alarm.playOnDelayCallback = mockAlarmPlayOnDelayCallback.playOnDelayCallback
    }

    override func tearDown() {
        alarm.stop()
    }

    func testAlarmInit() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let alarm = try! Alarm(audioPlayer: audioPlayer, delay: 2.0)
        XCTAssertTrue(alarm.start()!, "play should return true");
    }

    func testAlarmInitDefaultPercepts() {
        let alarm = try! Alarm(audioPlayer: audioPlayer, delay: 0.0, doesLEDDisplay: true, doesVibrate: true, runLoop: RunLoop.main)
        //this test confirms that the bools doesLEDDIsplya and doesVibrate show up as percepts
        XCTAssertEqual(alarm.percepts.count, 2)
        XCTAssertNotNil(alarm.ledPercept)
        XCTAssertNotNil(alarm.vibratePercept)
    }

    func testAlarmInitDefaultPerceptsNoLED() {
        let alarm = try! Alarm(audioPlayer: audioPlayer, delay: 0.0, doesLEDDisplay: false, doesVibrate: true, runLoop: RunLoop.main)
        //this test confirms that the bools doesLEDDIsplya and doesVibrate show up as percepts
        XCTAssertEqual(alarm.percepts.count, 1)
        XCTAssertNil(alarm.ledPercept)
        XCTAssertNotNil(alarm.vibratePercept)
    }

    func testAlarmInitDefaultPerceptsNoVibrate() {
        let alarm = try! Alarm(audioPlayer: audioPlayer, delay: 0.0, doesLEDDisplay: true, doesVibrate: false, runLoop: RunLoop.main)
        //this test confirms that the bools doesLEDDIsplya and doesVibrate show up as percepts
        XCTAssertEqual(alarm.percepts.count, 1)
        XCTAssertNotNil(alarm.ledPercept)
        XCTAssertNil(alarm.vibratePercept)
    }

    func testPlayOnDelay() {
        let finishedPlayingExpectation = expectation(description: "Play Alarm on Delay")
        XCTAssertNotNil(alarm.soundPlayer)
        XCTAssertNotNil(audioPlayer.audioPlayer)
        //playOnDelay is called with delay specified by variable
        let timeInitiated = Date()
        alarm.startOnDelay()
        //wait twice that amount of time for delay before failing test
        mockAlarmPlayOnDelayCallback.finishedPlayingExpectation = finishedPlayingExpectation
        waitForExpectations(timeout: 2.0 * specifiedDelay) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
        let actualDelay = Date().timeIntervalSince(timeInitiated)
        //Assert that callback was called
        XCTAssertTrue(mockAlarmPlayOnDelayCallback.didFinishPlaying)
        //asssert that callback called after specified amount for delay
        XCTAssertTrue(actualDelay > specifiedDelay)

    }
}
class RepeaterTests: XCTestCase {
    func testRepeat(){

        let finishedPlayingExpectation = expectation(description: "Repeat a number of times")
        let timesToCall = 5
        let delayBetweenCalls = 0.10
        let calculatedTotalDelay = Double(timesToCall) * delayBetweenCalls
        let calculatedMaxTotalDelay = Double(timesToCall + 1) * delayBetweenCalls

        var count = 0
        let mock = MockRepeaterCB()
        let timeInitiated = Date()
        mock.finishedPlayingExpectation = finishedPlayingExpectation
        let testRepeat = Repeater(timesToCall : 5,
                                  delayBetweenCalls: delayBetweenCalls,
                                  initialDelay: delayBetweenCalls,
                                  completedCallback : mock.completedPlaying,
                                  functionToCall : {number_call, total_call in
                                    print("number_call = \(number_call)")
                                    print("total_call = \(total_call)")
                                    print("time since interval = \(Date().timeIntervalSince(timeInitiated))")
                                    count += 1
            })

        
        testRepeat.start()
        waitForExpectations(timeout: 10.0) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
        let actualDelay = Date().timeIntervalSince(timeInitiated)
        XCTAssertEqual(count, 5)
        XCTAssertGreaterThanOrEqual(actualDelay, calculatedTotalDelay)
        XCTAssertLessThan(actualDelay, calculatedMaxTotalDelay)
    }

    func testRepeatConvenienceInit(){
        /*
         Imagine totalDuration = 2.0
         and updatesPerSecond = 2.0
         Then the following is the call sequence if there
         is no initial Delay in call

         0 --- 0.5 --- 1.0 --- 1.5 --- 2.0
         X ---- X ----- X ----- X ----- X

         Therefore there should be 5 calls with a 0.5 
         delay between Calls
        */
        let totalDuration = 10.0
        let updatePerSecond = 2.0
        let timesToCall = Int(totalDuration * updatePerSecond) + 1
        let delayBetweenCalls = 1.0 / updatePerSecond
        let testRepeat = Repeater(totalDuration: totalDuration, updatePerSecond: updatePerSecond, functionToCall: nil)
        XCTAssertEqual(testRepeat.delayBetweenCalls, delayBetweenCalls)
        XCTAssertEqual(testRepeat.timesToCall, timesToCall)
    }

    func testRepeatConvenienceInitInitialDelay(){
        /*
         Imagine totalDuration = 2.0
         and updatesPerSecond = 2.0
         Then the following is the call sequence if there
         is no initial Delay in call
         
         0 --- 0.5 --- 1.0 --- 1.5 --- 2.0
          ----- X ----- X ----- X ----- X
         
         Therefore there should be 4 calls with a 0.5
         delay between Calls
         */
        let totalDuration = 10.0
        let updatePerSecond = 2.0
        let timesToCall = Int(totalDuration * updatePerSecond) + 1
        let delayBetweenCalls = 1.0 / updatePerSecond
        let testRepeat = Repeater(totalDuration: totalDuration,
                                  updatePerSecond: updatePerSecond,
                                  initialDelay: 0.0,
                                  functionToCall: nil)
        XCTAssertEqual(testRepeat.delayBetweenCalls, delayBetweenCalls)
        XCTAssertEqual(testRepeat.timesToCall, timesToCall)
    }
    

    func testRepeatNoInitialDelay(){
        let finishedPlayingExpectation = expectation(description: "Repeat a number of times")
        let timesToCall = 5
        let delayBetweenCalls = 0.1
        //When Repeat is called with no initial delayy boolean then the first
        //call is made immediately. CalculatedTotalDelay must be incremented to account for this
        let calculatedTotalDelay = Double(timesToCall - 1) * delayBetweenCalls
        let calculatedMaxTotalDelay = Double(timesToCall) * delayBetweenCalls
        let timeInitiated = Date()
        
        var count = 0
        let mock = MockRepeaterCB()
        mock.finishedPlayingExpectation = finishedPlayingExpectation
        let testRepeat = Repeater(timesToCall : 5,
                                delayBetweenCalls: delayBetweenCalls,
                                initialDelay : 0.0,
                                completedCallback : mock.completedPlaying,
                                
                                functionToCall : {_, _ in
                                    print("time since interval = \(Date().timeIntervalSince(timeInitiated))")
                                    count += 1
        })
        
        testRepeat.start()
        waitForExpectations(timeout: 100.0) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
        let actualDelay = Date().timeIntervalSince(timeInitiated)
        XCTAssertEqual(count, 5)
        XCTAssertGreaterThanOrEqual(actualDelay, calculatedTotalDelay)
        XCTAssertLessThan(actualDelay, calculatedMaxTotalDelay)
    }

    func testRepeatWithInitialDelay(){
        let finishedPlayingExpectation = expectation(description: "Repeat a number of times")
        let timesToCall = 5
        let delayBetweenCalls = 0.1
        let initialDelay = 0.5
        //When Repeat is called with no initial delayy boolean then the first
        //call is made immediately. CalculatedTotalDelay must be incremented to account for this
        let calculatedTotalDelay = Double(timesToCall - 1) * delayBetweenCalls + initialDelay
        let calculatedMaxTotalDelay = Double(timesToCall) * delayBetweenCalls + initialDelay
        let timeInitiated = Date()
        
        var count = 0
        let mock = MockRepeaterCB()
        mock.finishedPlayingExpectation = finishedPlayingExpectation
        let testRepeat = Repeater(timesToCall : 5,
                                  delayBetweenCalls: delayBetweenCalls,
                                  initialDelay : initialDelay,
                                  completedCallback : mock.completedPlaying,
                                  
                                  functionToCall : {_, _ in
                                    print("time since interval = \(Date().timeIntervalSince(timeInitiated))")
                                    count += 1
        })
        
        testRepeat.start()
        waitForExpectations(timeout: 100.0) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
        let actualDelay = Date().timeIntervalSince(timeInitiated)
        XCTAssertEqual(count, 5)
        XCTAssertGreaterThanOrEqual(actualDelay, calculatedTotalDelay)
        XCTAssertLessThan(actualDelay, calculatedMaxTotalDelay)
    }
}



/*

class QueueTest: XCTestCase {

    var queue : Queue
    override func setUp() {
        integerQueue = Queue<Int>()
    }
    func testInitialLength() {
        XCtAssertEqual(integerQueue.length, 0)
    }

    func testPop() {
        integerQueue.pop()
    }

    func testPush() {
        integerQueue.push(2)
        XCTAssertEqual(integerQueue.length, 1)
        XCTAssertEqual(integerQueue.currentQueue, [1])
    }
}
*/

class AlarmTriggerTest: XCTestCase {
    

    var x : Double = 1.0
    var y : Double = 2.0
    var z : Double = 3.0
    var gs1 : GeneralState?
    var gs2 : GeneralState?
    var gpsState : GPSState?

    override func setUp() {
        super.setUp()
        x = 1.0
        y = 2.0
        z = 3.0
        gs1 = GeneralState(x: x, y: y, z: z)
        gs2 = GeneralState(x: x + 1.0, y: y + 1.0, z: z + 1.0)
        gpsState = GPSState(latitude : 0.0, longitude : 0.0)
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testGeneralStateInit(){

        XCTAssertEqual(gs1!.x, x)
        XCTAssertEqual(gs1!.y, y)
        XCTAssertEqual(gs1!.z, z)
    }

    func testGPSStateInit(){
        let gpsState = GPSState(latitude : 0.0, longitude : 0.0)
        XCTAssertEqual(gpsState.longitude, 0.0)
        XCTAssertEqual(gpsState.latitude, 0.0)
    }

    func testGPSStateSetters(){
        let latitude = 1.0
        let longitude = 2.0
        
        gpsState!.latitude = latitude
        gpsState!.longitude = longitude
        XCTAssertEqual(gpsState!.latitude, latitude)
        XCTAssertEqual(gpsState!.longitude, longitude)
        
    }
    
    func testGeneralStateEquality(){

        XCTAssertEqual(gs1!, gs1!)
        XCTAssertEqual(gs2!, gs2!)
    }

    func testGeneralStateIneqaulity(){
        XCTAssertNotEqual(gs1!, gs2!)
    }

    func testGeneralStateAdd(){
        let gs = GeneralState(x: 1.0, y: 1.0, z: 1.0)
        XCTAssertEqual(gs1! + gs, gs2!)
        let gs3 = GeneralState(x: x, y: y, z: z)
        XCTAssertEqual(gs3 + gs3, GeneralState(x: x + x, y: y + y, z: z + z))
    }

    func testGeneralStateDivision(){
        XCTAssertEqual(gs1! / gs1!, GeneralState(x: 1.0, y: 1.0, z: 1.0))
    }
    func testGeneralStateAddScalar(){
        XCTAssertEqual(gs1! + 1.0, GeneralState(x: x  + 1.0, y: y + 1.0, z: z + 1.0))
    }

    func testGeneralStateMinusScalar(){
        XCTAssertEqual(gs1! - 1.0, GeneralState(x: x - 1.0, y: y - 1.0, z: z - 1.0))
    }

    func testGeneralStateDivideScalar(){
        XCTAssertEqual(gs1! / 2.0, GeneralState(x: x / 2.0, y: y / 2.0, z: z / 2.0))
    }

    func testGeneralStateSubtract(){
        let gs3 = GeneralState(x: 1.0, y: 1.0, z: 1.0)
        let gs4 = GeneralState(x: 0.0, y: 0.0, z: 0.0)
        XCTAssertEqual(gs3 - gs3, gs4)
        let gs5 = GeneralState(x: 1.0, y: -1.0, z: 0.0)
        let gs6 = GeneralState(x: 0.0, y: 2.0, z: 1.0)
        XCTAssertEqual(gs3 - gs5, gs6)
    }

    func testNearlyEqual(){
        let diff = 0.1
        let gsx = gs1! + diff
        XCTAssertTrue(gs1!.nearlyEqual(gsx, threshold: GeneralState(x: diff + 0.1, y: diff + 0.1, z: diff + 0.1)))
    }

    func testNearlyEqualFails(){
        let gs1 = GeneralState(x: 1, y: 1, z: 1)
        let gs2 = GeneralState(x: 2, y: 3, z: 4)
        let gsXFail = GeneralState(x: 0, y: 1000, z: 1000)
        let gsYFail = GeneralState(x: 1000, y: 0, z: 1000)
        let gsZFail = GeneralState(x: 1000, y: 1000, z: 0)
        
        XCTAssertFalse(gs1.nearlyEqual(gs2, threshold: gsXFail))
        XCTAssertFalse(gs1.nearlyEqual(gs2, threshold: gsYFail))
        XCTAssertFalse(gs1.nearlyEqual(gs2, threshold: gsZFail))
    }

    func testNearlyEqualDouble(){
        let diff = 0.1
        let gsx = gs1! - diff
        XCTAssertTrue(gs1!.nearlyEqual(gsx, threshold: diff))
    }

    func testNearlyEqualFailDouble(){
        let diff = 0.1
        let gsx = gs1! + diff
        XCTAssertFalse(gs1!.nearlyEqual(gsx, threshold: diff - 0.01))
    }
}


struct TestFile {
    var fpath : String
    var url : URL

    init(bundle : Bundle, basename : String, ex : String){
        fpath = bundle.path(forResource: basename, ofType: ex)!
        url = URL.init(fileURLWithPath: fpath)
    }
}


class AlarmTriggerTestCollectedData: XCTestCase {
    
    
    var x : Double = 1.0
    var y : Double = 2.0
    var z : Double = 3.0
    var gs1 : GeneralState?
    var gs2 : GeneralState?
    var gpsState : GPSState?
    var motionManager : MockMotionManager?
    var testBundle : Bundle!
    var pickup_phone : TestFile!
    var button_press : TestFile!
    var h_shakes : TestFile!
    var v_shakes : TestFile!
    var stationary_in_hand : TestFile!
    var stationary : TestFile!

    override func setUp() {
        super.setUp()
        motionManager = MockMotionManager()
        testBundle = Bundle(for: type(of: self))
        pickup_phone = TestFile(bundle: testBundle, basename: "iphone5s_pickup-phone", ex: "csv")
        button_press = TestFile(bundle: testBundle, basename: "iphone5s_button-press", ex: "csv")
        h_shakes = TestFile(bundle: testBundle, basename: "iphone5s_3h-shakes", ex: "csv")
        v_shakes = TestFile(bundle: testBundle, basename: "iphone5s_3v-shakes", ex: "csv")
        stationary_in_hand = TestFile(bundle: testBundle, basename: "iphone5s_stationary-in-hand", ex: "csv")
        stationary = TestFile(bundle: testBundle, basename: "iphone5s_stationary", ex: "csv")

    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testSetInitialState(){
        //calculate initial state of first 10 rows
        // llop through all rows of csv]
        //
    }

    func testCSVRead(){
        //NSBundle *bundle = [NSBundle bundleForClass:[self class]];
        //NSString *path = [bundle pathForResource:@"foo" ofType:@"txt"];
        //let testBundle = NSBundle(forClass: self.dynamicType)
        motionManager!.csvContents(pickup_phone.url)
        XCTAssertEqual(motionManager!.currentCount, 0)
        let row = motionManager!.csvContentsConverted[0]
        XCTAssertEqual(row.count, 4)
        XCTAssertEqual(row[1], motionManager!.x)
        XCTAssertEqual(row[2], motionManager!.y)
        XCTAssertEqual(row[3], motionManager!.z)
    }

    func testMockAccelerometerData(){
        let acceleration = CMAcceleration(x: 1.0, y: 2.0, z: 3.0)
        let mockAccelerometerData = MockAccelerometerData(acceleration: acceleration)
        XCTAssertEqual(mockAccelerometerData!.acceleration.x, 1.0)
        XCTAssertEqual(mockAccelerometerData!.acceleration.y, 2.0)
        XCTAssertEqual(mockAccelerometerData!.acceleration.z, 3.0)
    }

    func testAlarmTriggerPickupPhoneNormalThreshold(){
        //Stewart sets an expectation that the alarm will be triggered
        //WHne triggered the Delegate is called
        let alarmTriggeredExpectation = expectation(description: "Alarm was Triggered")

        //Stewart sets the threshold to something small enough to trigger
        let threshold = AccelerometerState(x: 0.5, y: 0.5, z: 0.5)
        //the update interval is meaningless currently with how csvdata is set up
        let alarmTrigger = AccelerometerAlarmTrigger(updateInterval: 0.01, threshold: threshold)
        let mockDelegate = MockAlarmTriggerExpectationDelegate()
        //Stewart points to a filepath which simulates picking up a phone (based on data from iphone 5s)

        motionManager!.csvContents(pickup_phone.url)
        mockDelegate.alarmTriggeredExpectation = alarmTriggeredExpectation
        alarmTrigger.delegate = mockDelegate
        alarmTrigger.motionManager = motionManager!
        alarmTrigger.start()
        //because data is read from csv much faster then how
        //it occured real time. 1 second should be more than enough time for
        //trigger to go offs
        waitForExpectations(timeout: 1) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }

    func testAlarmTrigger3HShakesNormalThreshold(){
        //Stewart sets an expectation that the alarm will be triggered
        //WHne triggered the Delegate is called
        let alarmTriggeredExpectation = expectation(description: "Alarm was Triggered")

        
        //Stewart sets the threshold to something small enough to trigger
        let threshold = AccelerometerState(x: 0.5, y: 0.5, z: 0.5)
        //the update interval is meaningless currently with how csvdata is set up
        let alarmTrigger = AccelerometerAlarmTrigger(updateInterval: 0.01, threshold: threshold)
        let mockDelegate = MockAlarmTriggerExpectationDelegate()
        //Stewart points to a filepath which simulates picking up a phone (based on data from iphone 5s)
        
        motionManager!.csvContents(h_shakes.url)
        mockDelegate.alarmTriggeredExpectation = alarmTriggeredExpectation
        alarmTrigger.delegate = mockDelegate
        alarmTrigger.motionManager = motionManager!
        alarmTrigger.start()
        //because data is read from csv much faster then how
        //it occured real time. 1 second should be more than enough time for
        //trigger to go offs
        waitForExpectations(timeout: 1) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }

    func testAlarmTrigger3VShakesNormalThreshold(){
        //Stewart sets an expectation that the alarm will be triggered
        //WHne triggered the Delegate is called
        let alarmTriggeredExpectation = expectation(description: "Alarm was Triggered")
        
        
        //Stewart sets the threshold to something small enough to trigger
        let threshold = AccelerometerState(x: 0.5, y: 0.5, z: 0.5)
        //the update interval is meaningless currently with how csvdata is set up
        let alarmTrigger = AccelerometerAlarmTrigger(updateInterval: 0.01, threshold: threshold)
        let mockDelegate = MockAlarmTriggerExpectationDelegate()
        //Stewart points to a filepath which simulates picking up a phone (based on data from iphone 5s)
        
        motionManager!.csvContents(v_shakes.url)
        mockDelegate.alarmTriggeredExpectation = alarmTriggeredExpectation
        alarmTrigger.delegate = mockDelegate
        alarmTrigger.motionManager = motionManager!
        alarmTrigger.start()
        //because data is read from csv much faster then how
        //it occured real time. 1 second should be more than enough time for
        //trigger to go offs
        waitForExpectations(timeout: 1) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }

    func testAlarmTriggerStationaryInHand(){
        //This test should not trigger the alarm. At least
        //not for the level of threhsold that will be specified
        //Stewart sets the threshold to something small enough to trigger
        let threshold = AccelerometerState(x: 0.5, y: 0.5, z: 0.5)
        let alarmTrigger: AccelerometerAlarmTrigger? = AccelerometerAlarmTrigger(updateInterval: 0.01, threshold: threshold)
        let delegate = MockAlarmTriggerExpectationDelegate()
        motionManager!.csvContents(stationary_in_hand.url)
        alarmTrigger!.motionManager = motionManager!
        alarmTrigger!.delegate = delegate
        alarmTrigger!.start()
        //all data should be looped through and no trigger set
        while true {
            if delegate.isTriggerCalled {
                XCTFail()
                break
            }
            else{
                //This is a passing test. count > number of rows means
                //alll data in csv has been passed to MockAccelerometrData class
                if motionManager!.currentCount >= motionManager!.dataCount{
                    // test pass
                    break
                }
            }
        }
    }

    func testAlarmTriggerStationary(){
        //This test should not trigger the alarm. At least
        //not for the level of threhsold that will be specified
        //Stewart sets the threshold to something small enough to trigger
        let threshold = AccelerometerState(x: 0.5, y: 0.5, z: 0.5)
        let alarmTrigger = AccelerometerAlarmTrigger(updateInterval: 0.01, threshold: threshold)
        let delegate = MockAlarmTriggerExpectationDelegate()
        motionManager!.csvContents(stationary.url)
        alarmTrigger.motionManager = motionManager!
        alarmTrigger.delegate = delegate
        alarmTrigger.start()
        //all data should be looped through and no trigger set
        while true {
            if delegate.isTriggerCalled {
                XCTFail()
                break
            }
            else{
                //This is a passing test. count > number of rows means
                //alll data in csv has been passed to MockAccelerometrData class
                if motionManager!.currentCount >= motionManager!.dataCount{
                    // test pass
                    break
                }
            }
        }
    }

    func testAlarmTriggerStationaryTooSensitiveThreshold(){
        let alarmTriggeredExpectation = expectation(description: "testAlarmTriggerStationaryTooSensitiveThreshold")
        //This test should trigger the alarm because of the really low threshold
        //Stewart sets the threshold to something small enough to trigger
        let threshold = AccelerometerState(x: 0.00001, y: 0.00001, z: 0.00001)
        let alarmTrigger = AccelerometerAlarmTrigger(updateInterval: 0.01, threshold: threshold)
        let delegate = MockAlarmTriggerExpectationDelegate()
        delegate.alarmTriggeredExpectation = alarmTriggeredExpectation
        motionManager!.csvContents(stationary.url)
        alarmTrigger.motionManager = motionManager!
        alarmTrigger.delegate = delegate
        alarmTrigger.start()
        //all data should be looped through and no trigger set
        //because data is read from csv much faster then how
        //it occured real time. 1 second should be more than enough time for
        //trigger to go offs
        waitForExpectations(timeout: 1) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }

    func testAlarmTriggerPickupPhoneTooBigThreshold(){

        //Stewart sets the threshold to something much too big trigger
        let threshold = AccelerometerState(x: 200.0, y: 200.0, z: 200.0)
        let alarmTrigger = AccelerometerAlarmTrigger(updateInterval: 0.01, threshold: threshold)
        let delegate = MockAlarmTriggerExpectationDelegate()
        motionManager!.csvContents(pickup_phone.url)
        alarmTrigger.motionManager = motionManager!
        alarmTrigger.delegate = delegate
        alarmTrigger.start()
        //all data should be looped through and no trigger set
        while true {
            if delegate.isTriggerCalled {
                XCTFail()
                break
            }
            else{
                //This is a passing test. count > number of rows means
                //alll data in csv has been passed to MockAccelerometrData class
                if motionManager!.currentCount >= motionManager!.dataCount{
                    // test pass
                    break
                }
            }
        }
    }

    func testAlarmCancel(){
        //Stewart sets the threshold to something much too big trigger
        let threshold = AccelerometerState(x: 200.0, y: 200.0, z: 200.0)
        let alarmTrigger = AccelerometerAlarmTrigger(updateInterval: 0.01, threshold: threshold)
        let delegate = MockAlarmTriggerExpectationDelegate()
        motionManager!.csvContents(pickup_phone.url)
        alarmTrigger.motionManager = motionManager!
        alarmTrigger.delegate = delegate
        //Steawrt starts the alarmTrigger
        alarmTrigger.start()
        print("current count = \(motionManager!.currentCount)")
        //Stewart then arbitrarily says that this tell will loop through 1/10
        //of csv file.
        let numDataToRead = motionManager!.dataCount / 10
        //He confirms that that is more than what is required to set initialState
        XCTAssertGreaterThanOrEqual(numDataToRead, alarmTrigger.numberOfInitialStateSamples)
        
        while motionManager!.currentCount < motionManager!.dataCount / 10 {
            //do nothing. Only letting some of the data read
        }
        print("current count = \(motionManager!.currentCount)")
        //Steart confirms initlaState was set
        XCTAssertNotNil(alarmTrigger.initialState)
        //He then tests stopping the alarmTrigger. This shouldnt raise an error and should stop the reads of acceloremetr data
        let currentCount = motionManager!.currentCount
        alarmTrigger.stop()
        //Stewart waits one second. This should be more than enough time to get through the rest of the file
        sleep(1)
        XCTAssertLessThan(motionManager!.currentCount, motionManager!.dataCount)
        //Stewart also confirms that less than 5 additional line were read after
        //calling stop and waiting a seoncd. Ideally stop would kill everything immediately
        //but many of the methods have to wait for the cancelled property to be set before checking
        //addiotnally the accelerometerQueue finishes the OperationBlock its on
        XCTAssertLessThanOrEqual(motionManager!.currentCount - currentCount, 5)
        XCTAssertNil(alarmTrigger.initialState)
        
        print("current count = \(motionManager!.currentCount)")

    }
}


class SettingsTests : XCTestCase {
    var settings: SettingsInfo!

    override func setUp() {
        super.setUp()
        settings = SettingsInfo.singleton
        clearSettingsDictionary()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func clearSettingsDictionary() {
        settings.defaultsDictionaryRepresentation = [String: AnyObject]()
    }

    func testSettingsDefaults() {
        //The way settings are currently configured it is possible to have efault value types
        //that dont match the return type of the getter. This would result in a runtime error.
        //This test verifies no runtime errors are raised

        
        let _ = settings.timeUntilAlarmSounds
        let _ = settings.timeUntilAlarmArms
        let _ = settings.sensitivity
        let _ = settings.ledOnAlarm
        let _ = settings.vibrateOnAlarm
        let _ = settings.useTouchID
        //no assertion needed. Simply finishing the tests wtihout error is a pass
    }

    func testSetTimeUntilAlarm() {
        let timeUntilAlarmArms = 3.0
        settings.timeUntilAlarmArms = timeUntilAlarmArms
        XCTAssertEqual(settings.timeUntilAlarmArms, timeUntilAlarmArms)
    }

    func testSetUnitlAlarmSounds() {
        let timeUntilAlarmSounds = 4.0
        settings.timeUntilAlarmSounds = timeUntilAlarmSounds
        XCTAssertEqual(settings.timeUntilAlarmSounds, timeUntilAlarmSounds)
        
    }

    func testSetSensitivity() {
        let sensitivity = 0.5
        settings.sensitivity = sensitivity
        XCTAssertEqual(settings.sensitivity, sensitivity)
        
    }

    func testSetLedOnAlarm() {
        let ledOnAlarm = false
        settings.ledOnAlarm = ledOnAlarm
        XCTAssertFalse(settings.ledOnAlarm)
        settings.ledOnAlarm = !ledOnAlarm
        XCTAssertTrue(settings.ledOnAlarm)
        
    }
    func testSetVibrateOnAlarm() {
        
        let vibrateOnAlarm = true
        settings.vibrateOnAlarm = vibrateOnAlarm
        XCTAssertTrue(settings.vibrateOnAlarm)
        settings.vibrateOnAlarm = !vibrateOnAlarm
        XCTAssertFalse(settings.vibrateOnAlarm)
    }

    func testSetUseTouchIDDefault() {
        XCTAssertFalse(settings.useTouchID)
    }

    func testSetUseTouchID() {
        let useTouchID = true
        settings.useTouchID = useTouchID
        XCTAssertTrue(settings.useTouchID)
        XCTAssertTrue(UserDefaults.standard.bool(forKey: SettingsInfo.useTouchIDSettings.key))
        settings.useTouchID = !useTouchID
        XCTAssertFalse(settings.useTouchID)
        XCTAssertFalse(UserDefaults.standard.bool(forKey: SettingsInfo.useTouchIDSettings.key))
    }

    func testSetUsePhotoCaptureDefault() {
        XCTAssertFalse(settings.usePhotoCapture)
    }

    func testSetUsePhotoCapture() {
        let usePhotoCapture = true
        settings.usePhotoCapture = usePhotoCapture
        XCTAssertTrue(settings.usePhotoCapture)
        XCTAssertTrue(UserDefaults.standard.bool(forKey: SettingsInfo.usePhotoCaptureSettings.key))
        settings.usePhotoCapture = !usePhotoCapture
        XCTAssertFalse(settings.usePhotoCapture)
        XCTAssertFalse(UserDefaults.standard.bool(forKey: SettingsInfo.usePhotoCaptureSettings.key))
    }

    func testSetNumberOfPhotosDefault() {
        XCTAssertEqual(settings.numberOfPhotos, 4)
    }

    func testSetNumberOfPhotos() {
        let numberOfPhotos = 2.0
        settings.numberOfPhotos = Int(numberOfPhotos)
        XCTAssertEqual(settings.numberOfPhotos, Int(numberOfPhotos))
        XCTAssertEqual(UserDefaults.standard.double(forKey: SettingsInfo.numberOfPhotosSettings.key), numberOfPhotos)
    }

    func testTimeBetweenPhotosDefault() {
        XCTAssertEqual(settings.timeBetweenPhotos, 1.0)
    }

    func testTimeBetweenPhotos() {
        let timeBetweenPhotos = 2.0
        settings.timeBetweenPhotos = timeBetweenPhotos
        XCTAssertEqual(settings.timeBetweenPhotos, timeBetweenPhotos)
        XCTAssertEqual(UserDefaults.standard.double(forKey: SettingsInfo.timeBetweenPhotosSettings.key), timeBetweenPhotos)
    }

}


class SensitivityConverterTest: XCTestCase {
    var sensitivityConverter: SensitivityConverter!
    let low = 0.0
    let lowIndex = 0
    let mid = 0.5
    let midIndex = 1
    let high = 1.0
    let highIndex = 2
    let adjustValue = 0.05
    override func setUp() {
        
        sensitivityConverter = SensitivityConverter(max: 1.0, min: 0.0)
        XCTAssertEqual(sensitivityConverter.sensitivities.count, 3)
        //make sure adjustemntValue is less than the band (divided by two). This is because that band
        //dividied by two approximates the length to either side of speicifed value which will be set
        //to the value boundary
        XCTAssertLessThan(adjustValue, 1.0 / Double(sensitivityConverter.sensitivities.count) / 2.0)
    }


    func testValueSetToExactIndexBoundary() {
       
        //this test depends on sensitivties having 3 values
        //when setting the value at the boundary the output
        //value should be exactly what it was input as (no adjustemtn)
        sensitivityConverter.value = mid
        XCTAssertEqual(sensitivityConverter.value, mid)
        XCTAssertEqual(sensitivityConverter.index, midIndex)
        sensitivityConverter.value = low
        XCTAssertEqual(sensitivityConverter.value, low)
        XCTAssertEqual(sensitivityConverter.index, lowIndex)
        sensitivityConverter.value = high
        XCTAssertEqual(sensitivityConverter.value, high)
        XCTAssertEqual(sensitivityConverter.index, highIndex)
    }

    func testIndexSet() {
        sensitivityConverter.index = midIndex
        XCTAssertEqual(sensitivityConverter.value, mid)
        XCTAssertEqual(sensitivityConverter.index, midIndex)
        sensitivityConverter.index = lowIndex
        XCTAssertEqual(sensitivityConverter.value, low)
        XCTAssertEqual(sensitivityConverter.index, lowIndex)
        sensitivityConverter.index = highIndex
        XCTAssertEqual(sensitivityConverter.value, high)
        XCTAssertEqual(sensitivityConverter.index, highIndex)
    }

    func testValueSetJustUnderBoundary() {
        //this test depends on sensitivties having 3 values
        //when setting the value  near the boundary the setter
        //method actually adjust value. It has to do with
        //sensitivity bar needing to have discrete values
        sensitivityConverter.value = mid - adjustValue
        XCTAssertEqual(sensitivityConverter.value, mid)
        XCTAssertEqual(sensitivityConverter.index, midIndex)
        sensitivityConverter.value = low - adjustValue
        XCTAssertEqual(sensitivityConverter.value, low)
        XCTAssertEqual(sensitivityConverter.index, lowIndex)
        sensitivityConverter.value = high - adjustValue
        XCTAssertEqual(sensitivityConverter.value, high)
        XCTAssertEqual(sensitivityConverter.index, highIndex)
    }

    func testValueSetJustAboveBoundary() {
        //this test depends on sensitivties having 3 values
        //when setting the value  near the boundary the setter
        //method actually adjust value. It has to do with
        //sensitivity bar needing to have discrete values
        sensitivityConverter.value = mid + adjustValue
        XCTAssertEqual(sensitivityConverter.value, mid)
        XCTAssertEqual(sensitivityConverter.index, midIndex)
        sensitivityConverter.value = low + adjustValue
        XCTAssertEqual(sensitivityConverter.value, low)
        XCTAssertEqual(sensitivityConverter.index, lowIndex)
        sensitivityConverter.value = high + adjustValue
        XCTAssertEqual(sensitivityConverter.value, high)
        XCTAssertEqual(sensitivityConverter.index, highIndex)
    }

    func testIndexFromValMaximum() {
        //The index returned from settings the val equal to the max allowed
        //should return an index corresponding to the highest sensitivity
        let newIndex = sensitivityConverter.indexFromOldVal(sensitivityConverter.max)
        XCTAssertEqual(newIndex, sensitivityConverter.sensitivities.count - 1)
    }

    func testIndexFromValMinimum() {
        //The index returned from setting the val equal to the min allowed
        //should return an index corresponding to the lowest sensitivtiy
        let newIndex = sensitivityConverter.indexFromOldVal(sensitivityConverter.min)
        XCTAssertEqual(newIndex, 0)
    }

    func testPutIndexFromRange() {
        let newIndex = sensitivityConverter.putIndexInRange(100)
        XCTAssertEqual(newIndex, sensitivityConverter.sensitivities.count - 1)
    }
}

extension String {
    func strip() -> String{
        return self.trimmingCharacters(
            in: CharacterSet.whitespacesAndNewlines
        )
    }
}


class MockNSCoder : NSCoder {
    override func encodeValue(ofObjCType type: UnsafePointer<Int8>, at addr: UnsafeRawPointer){
        
    }
    override func decodeValue(ofObjCType type: UnsafePointer<Int8>, at data: UnsafeMutableRawPointer){
        
    }
    override func encode(_ data: Data){
        
    }
    override func decodeData() -> Data?{
        return Data()
    }
    override func decodeDouble(forKey key: String) -> Double{
        return 0.0
    }
    override func version(forClassName className: String) -> Int {
        return 0
    }
}


