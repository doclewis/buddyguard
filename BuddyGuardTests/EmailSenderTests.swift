//
//  EmailSenderTests.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 6/13/17.
//  Copyright © 2017 Stewart Boyd. All rights reserved.
//

import Foundation

import XCTest
import AVFoundation
import UIKit
import CoreMotion
import Foundation
import AFHTTPRequestOperationLogger
@testable import BuddyGuard



class EmailSenderTests: XCTestCase {

    var emailSender: EmailSender = EmailSender()
    var emailSenderDelegate: MockEmailSenderDelegate?
    var isInitialized = false
    
    override func setUp() {
        super.setUp()
        emailSenderDelegate = MockEmailSenderDelegate()
        emailSender.delegate = emailSenderDelegate
        if !isInitialized {
            isInitialized = true
            AFHTTPRequestOperationLogger.shared().startLogging()
        }
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testEmailSendSuccessfully() {
        let emailSentException = expectation(description: "Email was sucessfully sent")
        emailSenderDelegate?.emailSentExpectation = emailSentException

        //emailSender.send will fuffill expecation
        emailSender.send(to: "stewartboyd1988@gmail.com", subject: "test", body: "body")
        waitForExpectations(timeout: 2) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
        //confirm that didFinishPlaying is still false and thus was never called
        XCTAssertEqual(emailSenderDelegate!.emailResult, MailResult.success)
        //XCTAssertEqual(emailSenderDelegate!.emailResult?.message, "Queued. Thank you.")
    }

    func testEmailSendInvalidEmailAddress() {
        let emailSentException = expectation(description: "Email was sucessfully sent")
        emailSenderDelegate?.emailSentExpectation = emailSentException
        
        //emailSender.send will fuffill expecation
        emailSender.send(to: "stewartboyd1988", subject: "test", body: "body")
        waitForExpectations(timeout: 2) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
        //confirm that there is a message indicating the error
        XCTAssertEqual(emailSenderDelegate!.emailResult, MailResult.fail)
        //XCTAssertEqual(emailSenderDelegate!.emailResult?.message, "'to' parameter is not a valid address. please check documentation")
    }
    
    func testEmailSendWithImageSuccessfully() {
        let emailSentException = expectation(description: "Email was sucessfully sent")
        //let fpath =
        let image = UIImage.init(named: "lock.png")
        XCTAssertNotNil(image)
        emailSenderDelegate?.emailSentExpectation = emailSentException
        
        
        //emailSender.send will fuffill expecation
        emailSender.send(to: "stewartboyd1988@gmail.com", subject: "test", body: "body", image: image!, imageName: "test.png")
        waitForExpectations(timeout: 2) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
        //confirm that didFinishPlaying is still false and thus was never called
        XCTAssertEqual(emailSenderDelegate!.emailResult, MailResult.success)
        //XCTAssertEqual(emailSenderDelegate!.emailResult?.message, "Queued. Thank you.")
    }
}
