//
//  MockAVCaptureDevice.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 1/2/17.
//  Copyright © 2017 Stewart Boyd. All rights reserved.
//

import Foundation
import Photos

class MockAVCaptureDevice: AVCaptureDevice {

    var _torchMode: AVCaptureTorchMode = AVCaptureTorchMode.off
    var _torchLevel: Float = 0
    override var torchLevel: Float {
        get { return _torchLevel }
        set { _torchLevel = newValue }
    }

    override var torchMode: AVCaptureTorchMode {
        get { return _torchMode }
        set { _torchMode = newValue }
    }

    override var hasTorch: Bool {
        return true
    }

    override func lockForConfiguration() throws {
        //do nothing
    }

    override func unlockForConfiguration() {
        //do nothing
    }

    override func setTorchModeOnWithLevel(_ torchLevel: Float) throws {
        _torchMode = AVCaptureTorchMode.on
        _torchLevel = torchLevel
    }
}

/*
try d.lockForConfiguration()
if turnOn {
    do {
        try d.setTorchModeOnWithLevel(1.0)
    } catch {
        print(error)
    }
} else {
    d.torchMode = AVCaptureTorchMode.off
}
d.unlockForConfiguration()
 */
