//
//  MockEmailSenderDelegate.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 6/13/17.
//  Copyright © 2017 Stewart Boyd. All rights reserved.
//

import Foundation
import XCTest
import SwiftMailgun
@testable import BuddyGuard

enum MailResult {
    case fail
    case success
}


class MockEmailSenderDelegate: EmailSenderDelegate  {

    var emailResult: MailResult?
    var emailSentExpectation : XCTestExpectation!;

    func successCallback(result: String?) {
        emailResult = MailResult.success
        emailSentExpectation.fulfill()
    }
    func failureCallback(error: Error?) {
        emailResult = MailResult.fail
        emailSentExpectation.fulfill()
    }

}
