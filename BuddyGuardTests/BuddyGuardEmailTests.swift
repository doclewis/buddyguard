import XCTest
import AVFoundation
import UIKit
import CoreMotion
import Foundation
@testable import BuddyGuard


class BuddyGuardEmailTests: XCTestCase {
    
    var viewController : MainViewController!
    var filePath: String = Bundle.main.path(forResource: "test", ofType: "wav")!
    var audioPlayer: AudioPlayer!
    override func setUp() {
        super.setUp()
        self.continueAfterFailure = false
        // Put setup code here. This method is called before the invocation of each test method in the class.
        viewController = MainViewController()
        filePath = Bundle.main.path(forResource: "test", ofType: "wav")!
        audioPlayer = try! AudioPlayer(filepath: filePath)
        //need to access view to trigger viewDidLoad
        let _ = viewController.view
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testEmailSend() {
        // This tests email sending
        let to = "web-0o265@mail-tester.com"
        let emailSender = EmailSender()
        let fpath = Bundle.main.path(forResource: "goto_find_my_iphone", ofType: "html")
        let body = Utility.Read(fpath: fpath)
        XCTAssertNotNil(body)
        
        emailSender.send(to: to, subject: "Buddy Guard Alarm Triggered", body: body!)
    }
}
