//
//  AudioPlayerTests.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 12/11/16.
//  Copyright © 2016 Stewart Boyd. All rights reserved.
//

import XCTest
import AVFoundation
import UIKit
import CoreMotion
import Foundation
@testable import BuddyGuard


class AudioPlayerTests: XCTestCase {

    var fpath1: String!
    var fpath2: String!
    var audioPlayer1: AudioPlayer!
    var audioPlayer2: AudioPlayer!

    override func setUp() {
        fpath1 = Bundle.main.path(forResource: "test", ofType: "wav")!
        fpath2 = Bundle.main.path(forResource: "alarm_lydia", ofType: "m4a")!

        audioPlayer1 = try! AudioPlayer(filepath: fpath1)
        audioPlayer2 = try! AudioPlayer(filepath: fpath2)
    }
    func testAudioPlayerConstruction() {

        XCTAssertTrue(audioPlayer2.filepath == fpath2)
    }
    func testAudioPlayerUpdateVolume() {
        
        let newVolume: Float = 0.5
        let audioPlayer = try! AudioPlayer(filepath : fpath2,
                                      volume : VolumeLimit.minVolume,
                                      numberOfLoops : 0,
                                      fader : nil)
        XCTAssertTrue(audioPlayer.volume == VolumeLimit.minVolume)
        XCTAssertTrue(audioPlayer.audioPlayer.volume == VolumeLimit.minVolume)
        audioPlayer.volume = newVolume
        XCTAssertTrue(audioPlayer.volume == newVolume)
        XCTAssertTrue(audioPlayer.audioPlayer.volume == newVolume)
    }



    
    func testAudioPlayerInitConvenience() {
        //When constructiting using convenience class a fader
        //attribute is automatically made
        let fromVolume : Float = 0.2;
        let toVolume : Float = 0.8;
        let alarm = try! AudioPlayer(filepath: fpath1, fromVolume: fromVolume, toVolume: toVolume)
        XCTAssertNotNil(alarm.fader);
        XCTAssertEqual(alarm.fader?.fromVolume, fromVolume)
        XCTAssertEqual(alarm.fader?.toVolume, toVolume);
    }
    
    func testAudioPlayerRepeatCalculatedProperty() {
        // -1 means repat indefintely for the AVAudioPlayer class
        let audioPlayer = try! AudioPlayer(filepath: fpath1, volume: 1.0, numberOfLoops: -1)
        XCTAssertTrue(audioPlayer.doesRepeatIndefinitely)
        //COnfirm that the default doesn't loop indefinitely
        let audioPlayer1 = try! AudioPlayer(filepath: fpath1, volume : 0.9)
        XCTAssertFalse(audioPlayer1.doesRepeatIndefinitely)
        //verify taht afintie number doesn't return that  aloop happens indefintely
        let audioPlayer2 = try! AudioPlayer(filepath: fpath1, volume : 1.0, numberOfLoops: 4);
        XCTAssertFalse(audioPlayer2.doesRepeatIndefinitely)
    }
    
    func testAudioPlayerWithBrokenFilePath(){
        do{
            //this should raise an exception
            _ = try AudioPlayer(filepath: "fake_file_path", volume: 1.0)
            XCTFail();
        }
        catch let error as NSError {
            XCTAssertTrue(true);
        }
        
        //XCTAssertFalse(alarm.play()!, "play should fail because not an extant resource")
    }
    
    func testAudioPlayerWithAbove1Vol(){
        let audioPlayer = try! AudioPlayer(filepath: fpath1, volume : 3.0);
        XCTAssertEqual(audioPlayer.volume, VolumeLimit.maxVolume);
    }
    
    func testAudioPlayerBelow0Vol(){
        let audioPlayer = try! AudioPlayer(filepath: fpath1, volume : -1.0);
        XCTAssertEqual(audioPlayer.volume, VolumeLimit.minVolume);
    }
    
    func testAudioPlayerPlay(){
        let finishedPlayingExpectation = expectation(description: "The play should finish")
        let delegate = MockAVAudioPlayerDelegate(nibName: nil, bundle: nil)
        
        let audioPlayer = try! AudioPlayer(filepath: fpath1, volume: 1.0)
        delegate.finishedPlayingExpectation = finishedPlayingExpectation;
        XCTAssertGreaterThan(audioPlayer.duration, 0.9)
        //Stewart Plays the Alarm
        audioPlayer.audioPlayer.delegate = delegate
        audioPlayer.play()
        //The alarm play is asynchronous. This method will block until the
        //expecation is fufilled (done by delegate)
        waitForExpectations(timeout: audioPlayer.duration + 0.2) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }

    
    func testAudioPlayerStopIfAlreadyPlaying() {
        let audioPlayer = try! AudioPlayer(filepath: fpath1, volume: 1.0)
        let delegate = MockAVAudioPlayerDelegate(nibName: nil, bundle: nil)
        let finishedPlayingExpectation = expectation(description: "The play should finish")
        delegate.finishedPlayingExpectation = finishedPlayingExpectation
        XCTAssertGreaterThan(audioPlayer.duration, 0.9)
        audioPlayer.audioPlayer.delegate = delegate
        //stewart plays the alarm
        audioPlayer.play()
        //stewart then stops (never should play)
        audioPlayer.stop()
        //wait the amount of duration of the file and then fufill exepctation
        let when = DispatchTime.now() + audioPlayer.duration + 0.2 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            finishedPlayingExpectation.fulfill()
        }
        //exepcation should have been fufiled by DispatchQueue
        waitForExpectations(timeout: audioPlayer.duration + 0.2) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
        //confirm that didFinishPlaying is still false and thus was never called
        XCTAssertFalse(delegate.didFinishPlaying)
    }
    
    func testAudioPlayerStopNotPlaying() {
        //This is just to show that you can click stop
        //if nothing is playing and be fine
        let audioPlayer = try! AudioPlayer(filepath: fpath1, volume: 1.0)
        audioPlayer.stop()
        XCTAssertTrue(true)
    }
}
