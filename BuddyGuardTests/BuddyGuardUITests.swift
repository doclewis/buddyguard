import Quick
import Nimble
import UIKit
@testable import BuddyGuard

open class BuddyGuardUITests: QuickSpec {
    override open func spec() {
        var settingsTableViewController: SettingsTableViewController!
        var mainViewController: MainViewController!
        var armedViewController: ArmedViewController!
        var mainBundle: Bundle!
        var storyboard: UIStoryboard!
        var slider: UISlider!
        beforeEach {
            mainBundle = Bundle.main
            storyboard = UIStoryboard(name: "Main", bundle: mainBundle)
        }

        describe("segue around view controllers") {
            beforeEach {
                settingsTableViewController = storyboard.instantiateViewController(withIdentifier: "SettingsTableViewController") as! SettingsTableViewController
                mainViewController = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
                armedViewController = storyboard.instantiateViewController(withIdentifier: "ArmedViewControllerID") as! ArmedViewController
                
                let _ = mainViewController.view
            }
            it("fake test") {
                /*
                expect(settingsTableViewController.ledOnAlarmSwitch).to(beNil())
                expect(mainViewController.settingsButton).to(beNil())
                let _ = mainViewController.view
                expect(settingsTableViewController.ledOnAlarmSwitch).to(beNil())
                expect(mainViewController.settingsButton).toNot(beNil())
                let _ = settingsTableViewController.view
 */
                //expect(settingsTableViewController.ledOnAlarmSwitch).toNot(beNil())
                expect(mainViewController.settingsButton).toNot(beNil())
                
                mainViewController.present(armedViewController, animated: true, completion: nil)
                expect(armedViewController.bottomText).toNot(beNil())
                let timeUntilAlarmArms = armedViewController.settings.timeUntilAlarmArms
                print("timeUntilAlarmArms = \(timeUntilAlarmArms)")
                expect(armedViewController.bottomText.text).to(equal(String(timeUntilAlarmArms)))
            }
        }
        describe("settingsAreUpdated") {

            beforeEach {
                
                settingsTableViewController = storyboard.instantiateViewController(withIdentifier: "SettingsTableViewController") as! SettingsTableViewController
                //by accessing the view the neesary methods are called
                let _ = settingsTableViewController.view
                slider = settingsTableViewController.sensitivitySlider
                expect(settingsTableViewController.view).notTo(beNil())
                expect(settingsTableViewController.vibrateOnAlarmSwitch).notTo(beNil())
                expect(settingsTableViewController.ledOnAlarmSwitch).notTo(beNil())
                expect(settingsTableViewController.timeUntilArmedStep).notTo(beNil())
                expect(settingsTableViewController.timeUntilSoundStep).notTo(beNil())
                expect(settingsTableViewController.timeUntilArmedLabel).notTo(beNil())
                expect(settingsTableViewController.timeUntilSoundLabel).notTo(beNil())
                expect(settingsTableViewController.sensitivitySlider).notTo(beNil())
            }
            it("toggles the vibrateOn Alarm switch") {
                //Stewart gets current status of switch
                let vibrateOnAlarm = settingsTableViewController.vibrateOnAlarmSwitch.isOn
                //Stewart then verifies that the settings singleton deomnstrates this value
                expect(settingsTableViewController.settings.vibrateOnAlarm).to(equal(vibrateOnAlarm))
                //stewart then sets the switch to the opposite value
                settingsTableViewController.vibrateOnAlarmSwitch.setOn(!vibrateOnAlarm, animated: true)
                settingsTableViewController.vibrateOnAlarmValueChanged(settingsTableViewController.vibrateOnAlarmSwitch)
                //stewart then confirms that the swithc value and settings singleton are updated
                expect(settingsTableViewController.vibrateOnAlarmSwitch.isOn).to(equal(!vibrateOnAlarm))
                expect(settingsTableViewController.settings.vibrateOnAlarm).to(equal(!vibrateOnAlarm))
            }
            it("toggles the ledOn Alarm switch") {
                //Stewart gets current status of ledOnAlarm switch
                let  ledOnAlarm = settingsTableViewController.ledOnAlarmSwitch.isOn
                //Stewart then verifies that the settings singleton deomnstrates this value
                expect(settingsTableViewController.settings.ledOnAlarm).to(equal(ledOnAlarm))
                //stewart then sets the switch to the opposite value
                settingsTableViewController.ledOnAlarmSwitch.setOn(!ledOnAlarm, animated: true)
                settingsTableViewController.ledOnAlarmValueChanged(settingsTableViewController.ledOnAlarmSwitch)
                //stewart then confirms that the swithc value and settings singleton are updated
                expect(settingsTableViewController.ledOnAlarmSwitch.isOn).to(equal(!ledOnAlarm))
                expect(settingsTableViewController.settings.ledOnAlarm).to(equal(!ledOnAlarm))
            }
            it("update the time until alarm stepper and check label") {
                //Stewart gets current status of timeUntilAlarm stpeer
                let  timeUntilArmed = settingsTableViewController.timeUntilArmedStep.value
                //Stewart then verifies that the settings singleton deomnstrates this value
                expect(settingsTableViewController.settings.timeUntilAlarmArms).to(equal(timeUntilArmed))
                //stewart then sets the switch to the opposite value
                settingsTableViewController.timeUntilArmedStep.value = timeUntilArmed + 1
                settingsTableViewController.timeUntilArmedStepValueChanged(settingsTableViewController.timeUntilArmedStep)
                //stewart then confirms that the swithc value and settings singleton are updated
                expect(settingsTableViewController.timeUntilArmedStep.value).to(equal(timeUntilArmed + 1.0))
                expect(settingsTableViewController.settings.timeUntilAlarmArms).to(equal(timeUntilArmed + 1))
                //Stewart confirms that the value of timeUntilArmeedStep is contained in the label
                expect(settingsTableViewController.timeUntilArmedLabel.text?.range(of: String(timeUntilArmed + 1))).toNot(beNil())
            }
            it("update the time until alarm sound stepper and check label") {
                //Stewart gets current status of timeUntilAlarm stpeer
                let  timeUntilSound = settingsTableViewController.timeUntilSoundStep.value
                //Stewart then verifies that the settings singleton deomnstrates this value
                expect(settingsTableViewController.settings.timeUntilAlarmSounds).to(equal(timeUntilSound))
                //stewart then sets the switch to the opposite value
                settingsTableViewController.timeUntilSoundStep.value = timeUntilSound + 1
                settingsTableViewController.timeUntilSoundStepValueChanged(settingsTableViewController.timeUntilSoundStep)
                //stewart then confirms that the swithc value and settings singleton are updated
                expect(settingsTableViewController.timeUntilSoundStep.value).to(equal(timeUntilSound + 1.0))
                expect(settingsTableViewController.settings.timeUntilAlarmSounds).to(equal(timeUntilSound + 1))
                //Stewart confirms that the value of timeUntilAlarmSounds is contained in the label
                expect(settingsTableViewController.timeUntilSoundLabel.text?.range(of: String(timeUntilSound + 1))).toNot(beNil())
            }
    
            it("check basic sensitivty stufffs") {
                let numberOfSensitivities = 3
                
                expect(settingsTableViewController.sensitivitySlider.maximumValue).to(equal(1))
                expect(settingsTableViewController.sensitivitySlider.minimumValue).to(equal(0))
                
                expect(settingsTableViewController.settings.sensitivityConverter.sensitivities.count).to(equal(numberOfSensitivities))
            }

            it("update sensitivity to high sensitivity") {
                //Stewart sets the value which will yield the lowest sensitivity
                settingsTableViewController.sensitivitySlider.value = 0.2
                settingsTableViewController.sensitivitySliderTouchUpInside(slider)
                expect(settingsTableViewController.sensitivitySlider.value).to(equal(0.0))
                expect(settingsTableViewController.sensitivitySlider.value).to(equal(0.0))
                let index = settingsTableViewController.settings.sensitivityConverter.index
                let threshold = settingsTableViewController.settings.sensitivityConverter.sensitivities[index].threshold
                expect(threshold).to(equal(AccelerometerAlarmTrigger.highSensitivityThreshold))
            }

            it("update sensitivity to high sensitivity by setting exactly to 0") {
                //Stewart sets the value which will yield the lowest sensitivity
                settingsTableViewController.sensitivitySlider.value = 0.0
                settingsTableViewController.sensitivitySliderTouchUpInside(slider)
                expect(settingsTableViewController.sensitivitySlider.value).to(equal(0.0))
                expect(settingsTableViewController.sensitivitySlider.value).to(equal(0.0))
                let index = settingsTableViewController.settings.sensitivityConverter.index
                let threshold = settingsTableViewController.settings.sensitivityConverter.sensitivities[index].threshold
                expect(threshold).to(equal(AccelerometerAlarmTrigger.highSensitivityThreshold))
            }

            it("update sensitivity to medium sensitivity") {
                //expect(settingsTableViewController.settings.sensitivity).to(equal(settingsTableViewController.sensitivities[0]))
                //Stewart sets a value which will yield the middle sensitvity
                settingsTableViewController.sensitivitySlider.value = 0.4
                settingsTableViewController.sensitivitySliderTouchUpInside(slider)
                expect(settingsTableViewController.sensitivitySlider.value).to(equal(0.5))
                expect(settingsTableViewController.sensitivitySlider.value).to(equal(0.5))
                let index = settingsTableViewController.settings.sensitivityConverter.index
                let threshold = settingsTableViewController.settings.sensitivityConverter.sensitivities[index].threshold
                expect(threshold).to(equal(AccelerometerAlarmTrigger.mediumSensitivityThreshold))
            }

            it("update sensitivity to low sensitivity") {
                //Stewart sets a value which will yield the medium sensitvity
                settingsTableViewController.sensitivitySlider.value = 0.7
                settingsTableViewController.sensitivitySliderTouchUpInside(slider)
                expect(settingsTableViewController.sensitivitySlider.value).to(equal(1.0))
                expect(settingsTableViewController.sensitivitySlider.value).to(equal(1.0))
                let index = settingsTableViewController.settings.sensitivityConverter.index
                let threshold = settingsTableViewController.settings.sensitivityConverter.sensitivities[index].threshold
                expect(threshold).to(equal(AccelerometerAlarmTrigger.lowSensitivityThreshold))
           }
            it("update sensitivity to low sensitivity by settings exactly to 1") {
                //Stewart sets a value which will yield the lowest sensitvity. This
                //is being tested because in the past there were iisuses when settings to max or min
                settingsTableViewController.sensitivitySlider.value = 1.0
                settingsTableViewController.sensitivitySliderTouchUpInside(slider)
                expect(settingsTableViewController.sensitivitySlider.value).to(equal(1.0))
                expect(settingsTableViewController.sensitivitySlider.value).to(equal(1.0))
                let index = settingsTableViewController.settings.sensitivityConverter.index
                let threshold = settingsTableViewController.settings.sensitivityConverter.sensitivities[index].threshold
                expect(threshold).to(equal(AccelerometerAlarmTrigger.lowSensitivityThreshold))
                
            }
        }
    }
}
