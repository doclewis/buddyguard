//
//  MockRepeaterCallback.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 12/12/16.
//  Copyright © 2016 Stewart Boyd. All rights reserved.
//
import Foundation
import XCTest

class MockRepeaterCB{
    var finishedPlayingExpectation : XCTestExpectation!;
    func completedPlaying(_ flag: Bool) {
        if (flag == true){
            self.finishedPlayingExpectation.fulfill()
        }
    }
}
