//
//  LedPerceptTests.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 1/2/17.
//  Copyright © 2017 Stewart Boyd. All rights reserved.
//

import XCTest
import AVFoundation
import UIKit
import CoreMotion
import Foundation
@testable import BuddyGuard



class LedPerceptTests: XCTestCase {

    var d: MockAVCaptureDevice!
    var ledPercept: LedPercept!

    override func setUp() {
        super.setUp()
        d = MockAVCaptureDevice()
        ledPercept = LedPercept(timesToCall: 4, initialDelay: 0, delayBetweenCalls: 0.1, runLoop: RunLoop.main)
        ledPercept.device = d
    }

    func testLedPerceptCreation() {
        XCTAssertNotNil(ledPercept.device)
        XCTAssertTrue(ledPercept.device!.hasTorch)
        XCTAssert(ledPercept.device!.torchMode == AVCaptureTorchMode.off)
    }

    func testTurnOnTurnOffofMockLEDDevice() {
        XCTAssert(d.torchMode == AVCaptureTorchMode.off)
        try! d.setTorchModeOnWithLevel(0.2)
        XCTAssert(d.torchLevel == 0.2)
        XCTAssert(d.torchMode == AVCaptureTorchMode.on)
        d.torchMode = AVCaptureTorchMode.off
        XCTAssert(d.torchMode == AVCaptureTorchMode.off)
    }

    func testLedPerceptEndsWithLightOff() {
        XCTAssert(ledPercept.device!.torchMode == AVCaptureTorchMode.off)
        ledPercept.activateLED(turnOn: true)
        XCTAssert(ledPercept.device!.torchMode == AVCaptureTorchMode.on)
        ledPercept.activateLED(turnOn: false)
        XCTAssert(ledPercept.device!.torchMode == AVCaptureTorchMode.off)
    }

    func testLedPerceptEndsWithLedOffWithOddNumberOfRepeats() {
        let finishedPlayingExpectation = expectation(description: "Repeat a number of times")
        let timesToCall = 1         
        let delayBetweenCalls = 0.05
        
        
        let mock = MockRepeaterCB()
        mock.finishedPlayingExpectation = finishedPlayingExpectation
        let ledPercept = LedPercept(timesToCall: timesToCall,
                                    initialDelay: 0,
                                    delayBetweenCalls: delayBetweenCalls,
                                    runLoop: RunLoop.main,
                                    completedCallback: mock.completedPlaying)
        ledPercept.device = d
        ledPercept.start()
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
        XCTAssert(ledPercept.device!.torchMode == AVCaptureTorchMode.off)
    }

    func testLedPerceptEndsWithLedOffWithEvenNumberOfRepeats() {
        let finishedPlayingExpectation = expectation(description: "Repeat a number of times")
        let timesToCall = 2
        let delayBetweenCalls = 0.05

        
        let mock = MockRepeaterCB()
        mock.finishedPlayingExpectation = finishedPlayingExpectation
        let ledPercept = LedPercept(timesToCall: timesToCall,
                                    initialDelay: 0,
                                    delayBetweenCalls: delayBetweenCalls,
                                    runLoop: RunLoop.main,
                                    completedCallback: mock.completedPlaying)
        ledPercept.device = d
        ledPercept.start()
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
        XCTAssert(ledPercept.device!.torchMode == AVCaptureTorchMode.off)
    }

}
