//
//  AlarmAudioDataSources.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 6/25/17.
//  Copyright © 2017 Stewart Boyd. All rights reserved.
//

import UIKit


struct Filename {
    var name: String
    var ex: String
    var altName: String
    
    init(name: String, ex: String, altName: String) {
        self.name = name
        self.ex = ex
        self.altName = altName
    }
}


struct AlarmAudioDataSource {
    static let data = [Filename(name: GlobalString.alarmLydia, ex: "m4a", altName: GlobalString.altAlarmLydia),
                       Filename(name: GlobalString.alarmLydia2, ex: "m4a", altName: GlobalString.altAlarmLydia2),
                       Filename(name: GlobalString.alarmLydia3, ex: "m4a", altName: GlobalString.altAlarmLydia3),
                       Filename(name: GlobalString.alarmLydiaSpanish, ex: "m4a", altName: GlobalString.altAlarmLydiaSpanish),
                       Filename(name: GlobalString.alarmLydiaRussian, ex: "m4a", altName: GlobalString.altAlarmLydiaRussian),
                       Filename(name: GlobalString.alarmLydiaItalian, ex: "m4a", altName: GlobalString.altAlarmLydiaItalian),
                       Filename(name: GlobalString.alarmLydiaJapanese, ex: "m4a", altName: GlobalString.altAlarmLydiaJapanese),
                       Filename(name: GlobalString.alarmLydiaSinging, ex: "m4a", altName: GlobalString.altAlarmLydiaSinging),
                       
                       ]
    
    static func getData(by name: String) -> Filename?{
        for d in AlarmAudioDataSource.data {
            if name == d.name {
                return d
            }
        }
        return nil
    }
}
