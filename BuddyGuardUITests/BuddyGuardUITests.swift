//
//  BuddyGuardUITests.swift
//  BuddyGuardUITests
//
//  Created by Stewart Boyd on 5/23/16.
//  Copyright © 2016 Stewart Boyd. All rights reserved.
//

import XCTest

class BuddyGuardUITests: XCTestCase {

    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // Use recording to get started writing UI tests.
        
        let app = XCUIApplication()
        app.buttons["Settings"].tap()
        app.navigationBars["Settings"].children(matching: .button).matching(identifier: "Back").element(boundBy: 0).tap()
        
        let lockButton = app.buttons["Lock"]
        lockButton.tap()
        app.navigationBars["ArmedScene"].children(matching: .button).matching(identifier: "Back").element(boundBy: 0).tap()
        lockButton.tap()
        
        let secureTextField = XCUIApplication().children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element(boundBy: 1).children(matching: .secureTextField).element
        secureTextField.tap()
        secureTextField.tap()
        secureTextField.tap()
        secureTextField.tap()
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        
        
    }

    //After hitting back the alarm shouldn't play
    
    func testAfterLockingAndSelectingBackAlarmDoesntPlay(){
        
        let app = XCUIApplication()
        app.buttons["Settings"].tap()
        
        let tablesQuery2 = app.tables
        let tablesQuery = tablesQuery2
        tablesQuery.switches["LED"].tap()
        tablesQuery.switches["Vibrate"].tap()
        tablesQuery2.cells.containing(.staticText, identifier:"Until Armed").buttons["Decrement"].tap()
        tablesQuery2.cells.containing(.staticText, identifier:"Sound").buttons["Decrement"].tap()
        
        let slider = tablesQuery.sliders["100%"]
        slider.swipeLeft()
        slider.swipeLeft()
        slider.press(forDuration: 0.7);
        slider.tap()
        slider.tap()
        slider.tap()
        slider.tap()
        app.navigationBars["Settings"].children(matching: .button).matching(identifier: "Back").element(boundBy: 0).tap()
        
        let lockButton = app.buttons["Lock"]
        lockButton.tap()
        app.alerts["Error"].buttons["OK"].tap()
        app.buttons["Create Password"].tap()
        
        let element = app.otherElements.containing(.navigationBar, identifier:"CreatePw").children(matching: .other).element.children(matching: .other).element
        let secureTextField = element.children(matching: .other).element.children(matching: .other).element.children(matching: .secureTextField).element
        secureTextField.tap()
        secureTextField.typeText("1111")
        element.children(matching: .other).element(boundBy: 0).children(matching: .other).element.tap()
        lockButton.tap()
        app.navigationBars["ArmedScene"].children(matching: .button).matching(identifier: "Back").element(boundBy: 0).tap()
        
    }
    
}
