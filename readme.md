Buddy Guard
===================

Buddy guard aims to arm your phone an alert those in the surrounding
vacinity, via audible and visual perceptives, when motion is detected. 

Current precepts include:

1. SoundPlayer (mp4)
2. LED
3. Vibration
4. Email
5. IWatch Notifications


Support
====================

For support issues contact Stewart Boyd (stewartboyd1988@gmail.com) or
Justin Salazar (jrsalazar89@gmail.com)