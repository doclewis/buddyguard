//
//  EmailMessage.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 6/15/17.
//  Copyright © 2017 Stewart Boyd. All rights reserved.
//

import Foundation
import mailgun


struct EmailMeta {
    //var emailSender: EmailSender
    //var message: MGMessage?

    var to: String
    var body: String
    var subject: String

    init(to:String, subject: String, body: String) {
        self.subject = subject
        self.to = to
        self.body = body
        //emailSender = EmailSender()
        
    }
    
}
