    //
//  File.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 5/29/17.
//  Copyright © 2017 Stewart Boyd. All rights reserved.
//

import Foundation

class GlobalString {

    //MARK: Storyboards
    static let MainStoryboard = "Main"
    static let WelcomeStoryboard = "Welcome"
    // MARK: Storyboard ViewControllers
    static let MainNavigationController = "MainNavigationController"
    static let CameraAccessViewController = "CameraAccessViewController"
    static let LocationAccessViewController = "LocationAccessViewController"
    static let PushNotificationsAccessViewController = "PushNotificationsAccessViewController"
    static let SetEmailViewController = "SetEmailViewController"
    // Mark: Storyboard Segues Literals
    static let SegueCreatePasswordToMain = "unwindToMain"
    static let SegueArmedToMain = "unwindToMainDisarm"
    static let segueToCreatePassword = "segueToCreatePassword";
    static let segueToArmLock = "SegueToArmLock"
    static let segueFromSettingsToConfirmPW = "SegueSettings2ConfirmPW"
    static let SegueSettings2CreatePW = "SegueSettings2CreatePW"
    static let SegueConfirmPW2CreatePW = "SegueConfirmPW2CreatePW"
    static let SegueMainToSettings = "SegueMainToSettings"
    static let SegueCameraCaptureToSettings = "toCameraCaptureSettingsSegue"
    
    //Mark:Storyboard Containers
    static let CreatePasswordNumberpadContainer = "CreatePasswordNumberpadContainer"
    static let ConfirmPasswordNumberpadContainer = "ContainerNumberpad"
    
    //MARK: Storyboard HTML
    static let GotoFindMyIphone = "goto_find_my_iphone"
    static let PhoneThiefEmail = "phone_thief_email"
    
    // Mark: Filepaths
    static let alarmLydia = "alarm_lydia"
    static let altAlarmLydia = "Classic Phone Thief"
    static let alarmLydia2 = "alarm_lydia2"
    static let altAlarmLydia2 = "Cheery Thief"
    static let alarmLydia3 = "alarm_lydia3"
    static let altAlarmLydia3 = "Stop the Thief!"
    static let alarmLydiaItalian = "alarm_lydia_italian"
    static let altAlarmLydiaItalian = "Lydia - Italian"
    static let alarmLydiaJapanese = "alarm_lydia_japanese"
    static let altAlarmLydiaJapanese = "Lydia - Japanese"
    static let alarmLydiaRussian = "alarm_lydia_russian"
    static let altAlarmLydiaRussian = "Lydia - Russian"

    static let alarmLydiaSinging = "alarm_lydia_singing"
    static let altAlarmLydiaSinging = "Lydia - Singing"

    static let alarmLydiaSpanish = "alarm_lydia_spanish"
    static let altAlarmLydiaSpanish = "Lydia - Spanish"
    
}
