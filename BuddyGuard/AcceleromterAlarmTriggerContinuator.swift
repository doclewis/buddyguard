//
//  AcceleromterAlarmTriggerContinuator.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 5/20/17.
//  Copyright © 2017 Stewart Boyd. All rights reserved.
//


import CoreMotion
import AVFoundation


class AccelerometerAlarmTriggerContinuator: NSObject, AVAudioPlayerDelegate {
    
    var audioPlayer: AudioPlayer
    var alarmTrigger: AccelerometerAlarmTrigger
    let audioSessionConfigurator = AudioSessionConfigurator()
    
    init(audioPlayer: AudioPlayer, alarmTrigger: AccelerometerAlarmTrigger) {
        self.audioPlayer = audioPlayer
        self.alarmTrigger = alarmTrigger
        
    }
    
    func runInBackground() {
        audioSessionConfigurator.defaultConfigure()
        alarmTrigger.motionManager.startAccelerometerUpdates()
        audioPlayer.audioPlayer.delegate = self
        let _ = audioPlayer.play()
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print("AcceleromterAlarmTriggerContinuator finished playing. Check acceleromter and repeat if necessary")
        if let _ = alarmTrigger.initialState {
            let accelerometerData = alarmTrigger.motionManager.accelerometerData
            alarmTrigger.getCurrentState(accelerometerData, error: nil)
            if alarmTrigger.isCurrentStateWithinThreshold() == true {
                let _ = audioPlayer.play()
                print("continue acceleromterTrigger")
            } else {
                print("not within state. Dont continue aidoiPlayer")
            }
            
        }
        else {
            print("Initial State not set for acceleromterTrigger. Nothing to do")
        }
        
    }
}
