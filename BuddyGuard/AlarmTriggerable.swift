//
//  AlarmTriggerable.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 12/31/16.
//  Copyright © 2016 Stewart Boyd. All rights reserved.
//

import Foundation

protocol AlarmTriggerable {
    
    
    var initialState : GeneralState? { get set }
    var currentState: GeneralState? {get set}
    var threshold : GeneralState { get set }
    var updateInterval : Double { get set }
    var delegate : AlarmTriggerableDelegate? { get set }
    init(updateInterval : Double, threshold : GeneralState)
    func start()
    func stop()
    func reset()
    func startWithState(initialState: GeneralState)
    func establishInitialState()
    func isCurrentStateWithinThreshold() -> Bool?
    
}
