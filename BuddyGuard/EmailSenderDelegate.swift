//
//  EmailSenderDelegate.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 5/29/17.
//  Copyright © 2017 Stewart Boyd. All rights reserved.
//


protocol EmailSenderDelegate {

    func successCallback(result: String?)
    func failureCallback(error: Error?)

}

