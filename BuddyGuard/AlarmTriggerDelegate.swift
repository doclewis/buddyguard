//
//  AlarmTriggerDelegate.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 9/3/16.
//  Copyright © 2016 Stewart Boyd. All rights reserved.
//

import AudioToolbox


protocol AlarmTriggerableDelegate {
    
    func startedReading()
    
    func stoppedReading()
    
    func initializedInitialState(_ initialState : GeneralState?)
    
    func triggered()
    
}
