//
//  Repeat.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 7/2/16.
//  Copyright © 2016 Stewart Boyd. All rights reserved.
//

import Foundation

open class Repeater {
    static let reapeatIndefinitely = -1
    var functionToCall : ((Int, Int) -> ())?
    var timesToCall : Int
    var delayBetweenCalls : Double
    var timerWorker : Timer? //timer used for calling if no delay is specified
    var initialDelay: Double
    var completedCallback : ((Bool) -> ())?
    var runLoop : RunLoop
    internal var numberOfCalls = 0;

    var doesRepeatIndefinitely: Bool {
        return timesToCall == Repeater.reapeatIndefinitely
    }
    
    var repeatCompleted : Bool {
        if doesRepeatIndefinitely {
            return false
        }
        return numberOfCalls >= timesToCall
    }
    
    var hasInitialDelay: Bool {
        return (initialDelay != 0.0)
    }

    /// Repeater is a class which uses timers to repeatedly call
    /// some worker function.
    ///
    /// - Parameters:
    ///   - timesToCall: Int - number of times to call worker function
    ///   - delayBetweenCalls: Double - seconds of delay between function calls
    ///   - initialDelay: Double - second before first function call
    ///   - runLoop: RunLoop
    ///   - completedCallback: Callback executed after completion
    ///   - functionToCall: worker function
    init(timesToCall: Int,
         delayBetweenCalls: Double,
         initialDelay: Double = 0,
         runLoop: RunLoop = RunLoop.current,
         completedCallback: ((Bool)->())? = nil,
         functionToCall : ((_ numCall : Int, _ totalNumCall : Int) -> ())?){

        self.functionToCall = functionToCall
        self.timesToCall = timesToCall
        self.initialDelay = initialDelay
        self.runLoop = runLoop
        self.delayBetweenCalls = delayBetweenCalls
        self.completedCallback = completedCallback
    }

    /// Convenience constructor is used when easier to specify in terms
    /// of duration as opposed to explicitly in terms of number of calls to function
    ///
    /// - Parameters:
    ///   - totalDuration: Double - indicates number of seconds for repeater to occur
    ///   - updatePerSecond: Double - frequency (per second) of calls to functionToCall
    ///   - initialDelay: Double - indicates number of seconds before first call
    ///   - runLoop: RunLoop
    ///   - completedCallback: Callback executed after completion
    ///   - functionToCall: worker function
    convenience init(totalDuration: Double,
                     updatePerSecond: Double,
                     initialDelay: Double = 0.0,
                     runLoop: RunLoop = RunLoop.current,
                     completedCallback: ((Bool)->())? = nil,
                     functionToCall: ((_ numCall : Int, _ totalNumCall : Int) -> ())?){

        let timesToCall = Int(totalDuration * updatePerSecond) + 1
        let delayBetweenCalls = 1.0 / updatePerSecond
        self.init(timesToCall : timesToCall,
                  delayBetweenCalls: delayBetweenCalls,
                  initialDelay: initialDelay,
                  runLoop: runLoop,
                  completedCallback: completedCallback,
                  functionToCall: functionToCall)
        
    }

    deinit {
        completedCallback?(false)
        stopTimers()
    }

    /// Calls ``functionToCall`` a set number of times. The only variation in behavior
    /// is when hasInitialDelay is true or false (default).
    ///
    /// When true, will pause before first call. When false will call first call
    /// immediately, and then proceed with delays between subsequnet calls
    func start(){

        numberOfCalls = 0
        stop()
        //print("delayBetweenCalls = \(delayBetweenCalls)")
        let fireAt = Date.init(timeIntervalSinceNow: initialDelay)
        timerWorker = Timer(fire: fireAt, interval: delayBetweenCalls, repeats: true, block: self.call)
        if let t = timerWorker {
            runLoop.add(t, forMode: RunLoopMode.defaultRunLoopMode)
        }
   }

    /// Call completedCallback if this Repeater doesRepeatIndefinitely
    /// or if the function has been called as many times as requested.
    /// Then kills timers
    func stop(){
        if doesRepeatIndefinitely {
            self.completedCallback?(true)
        } else {
            self.completedCallback?(numberOfCalls == timesToCall)
        }
        stopTimers()
    }

    /// Kills timer object
    func stopTimers() {
        if let currentTimer = timerWorker {
            currentTimer.invalidate()
            timerWorker = nil
        }
    }

    /// Method used as worker block in created timer objects
    ///
    /// - Parameter timer: Timer
    func call(timer: Timer){
        //print("repeat -> call")
        //print("Repeater numberOfCalls = \(numberOfCalls), timesToCall = \(timesToCall)")
        functionToCall?(numberOfCalls, timesToCall)
        numberOfCalls += 1
        
        if repeatCompleted{
            stop()
            return
        }
    }



}

