//
//  TestEmailViewController.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 5/6/17.
//  Copyright © 2017 Stewart Boyd. All rights reserved.
//

import UIKit



class TestEmailViewController: UITableViewController {

    var emailSender: EmailSender!
    var emailAddress: String?
    let serialQueue = DispatchQueue(label: "Serial Queue")

    @IBOutlet weak var sendTestEmail: UIButton!

    override func viewDidLoad() {
        emailAddress = SettingsInfo.singleton.email
        

    }


    override func viewWillAppear(_ animated: Bool) {
        sendTestEmail.isEnabled = false
    }
    override func viewDidAppear(_ animated: Bool) {
        serialQueue.async {
            self.emailSender = EmailSender()
            if let _ = self.emailAddress {
                DispatchQueue.main.async{
                    self.sendTestEmail.isEnabled = true
                }
            }
            
        }
    }

    @IBAction func buttonClickedSendEmail(_ sender: UIButton) {
        print("hello")
        if let to = emailAddress {

            let fpathPreamble = Bundle.main.path(forResource: "test_preamble", ofType: "html")
            let fpathMain = Bundle.main.path(forResource: "goto_find_my_iphone", ofType: "html")
            let bodyPreamble = Utility.Read(fpath: fpathPreamble)!
            let bodyMain = Utility.Read(fpath: fpathMain)!
            self.serialQueue.async {
                _ = self.emailSender.send(to: to, subject: "Test - Buddy Guard Alarm Triggered", body: bodyPreamble + bodyMain)
            }
            
        }
    }
    
}
