//
//  EmailSender.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 4/23/17.
//  Copyright © 2017 Stewart Boyd. All rights reserved.
//

import Foundation
import mailgun
//import SwiftMailgun


class EmailSender {
    
    let apiKey = "key-bbecf9b478c9f9312a2610dcc481d196"
    let clientDomain = "doclewis.net"
    let from = "Buddy Guard <postmaster@doclewis.net>"
    var delegate: EmailSenderDelegate?
    let mailgun: Mailgun
    
    init() {
        mailgun = Mailgun.client(withDomain: clientDomain, apiKey: apiKey)
    }
    func sendDummy() {
        let image = UIImage(named: "Alarm")
        
        send(to: "Stewart Boyd <stewartboyd1988@gmail.com>", subject: "test attachment", body: "body", image: image!, imageName: "Alarm", type: ImageAttachmentType.PNGFileType)
    }

    func send(to: String, subject: String, body: String, image: UIImage? = nil, imageName: String? = nil,
              type: ImageAttachmentType = ImageAttachmentType.JPEGFileType){
        let message = MGMessage(from: from, to: to, subject: subject, body: "")
        message?.html = body
        if let _ = image {
            message?.add(image, withName: imageName, type: type, inline: false)
        }
        
        send(message: message)
    }

    func send(message: MGMessage?) {
        mailgun.send(message, success: successCallback(result:), failure: failureCallback(error:))
    }

    func successCallback(result: String?) {
        delegate?.successCallback(result: result)
        print("sucessfully sent email")
    }
    
    func failureCallback(error: Error?) {
        delegate?.failureCallback(error: error)
        print("there was a failure sending email")
    }

    static func GetFindMyIphoneEmailContent() -> String?{
        let fpath = Bundle.main.path(forResource: GlobalString.GotoFindMyIphone, ofType: "html")
        return Utility.Read(fpath: fpath)
    }

    static func GetPhoneThiefEmailContent() -> String?{
        let fpath = Bundle.main.path(forResource: GlobalString.PhoneThiefEmail, ofType: "html")
        return Utility.Read(fpath: fpath)
    }



}
