//
//  File.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 6/3/17.
//  Copyright © 2017 Stewart Boyd. All rights reserved.
//

import UIKit


class KeyboardToolbarFactory {

    static func create(title: String, target: Any?, selector: Selector) -> UIToolbar {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: title, style: UIBarButtonItemStyle.done, target: target, action: selector)
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        return doneToolbar
    }

}
