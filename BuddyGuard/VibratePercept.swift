//
//  VibratePercept.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 2/20/17.
//  Copyright © 2017 Stewart Boyd. All rights reserved.
//

import Foundation
import AudioToolbox


class VibratePercept: RepeaterPercept {
    
    
    /// VibratePercept is a subclass of RepeaterPercept.
    /// Its a class that encapsulates the creation of an object
    /// which repeatedly vibrates the device
    ///
    /// - Parameters:
    ///   - timesToCall: Int indicating number of times to vibrate (can be indefinite).
    ///   - initialDelay: Double indicating number of seconds to delay before first vibrate
    ///   - delayBetweenCalls: Double indicating number of seconds between vibrates
    ///   - runLoop: RunLoop
    ///   - completedCallback: callback method called after last vibrate
    override init(timesToCall: Int, initialDelay: Double, delayBetweenCalls: Double, runLoop: RunLoop, completedCallback: ((Bool) -> ())? = nil)  {
        super.init(timesToCall: timesToCall,
                   initialDelay: initialDelay,
                   delayBetweenCalls: delayBetweenCalls,
                   runLoop: runLoop,
                   completedCallback: completedCallback)
        repeater.functionToCall = { _, _ in
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
        }
    }
    
}
