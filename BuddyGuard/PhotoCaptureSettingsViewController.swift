//
//  PhotoCaptureSettings.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 2/25/17.
//  Copyright © 2017 Stewart Boyd. All rights reserved.
//

import Foundation
import UIKit

class PhotoCaptureSettingsViewController: UITableViewController {

    let settings = SettingsInfo.singleton

    //@IBOutlet weak var usesCameraCapture: SwitchTableCell!
    @IBOutlet weak var numberOfPhotos: StepperTableCell!
    @IBOutlet weak var timeBetweenPhotos: StepperTableCell!

    override func viewDidLoad() {
        //usesCameraCapture.uiSwitch.addTarget(self, action: #selector(self.usesCameraCaptureWasSwitched(_:)), for: .valueChanged)
        numberOfPhotos.stepper.addTarget(self, action: #selector(self.numberOfPhotosWasStepped(_:)), for: .valueChanged)
        timeBetweenPhotos.stepper.addTarget(self, action: #selector(self.timeBetweenPhotosWasStepped(_:)), for: .valueChanged)
        loadSettings()
        numberOfPhotos.stepLabel.text = "\(settings.numberOfPhotos)"
        timeBetweenPhotos.stepLabel.text = "\(settings.timeBetweenPhotos)"
    }

    func loadSettings() {
        //usesCameraCapture.uiSwitch.isOn = settings.usePhotoCapture
        numberOfPhotos.stepper.value = Double(settings.numberOfPhotos)
        timeBetweenPhotos.stepper.value = settings.timeBetweenPhotos
    }

    func numberOfPhotosWasStepped(_ sender: UIStepper) {
        print("numberOfPhotosWasStepped -> \(sender.value)")
        settings.numberOfPhotos = Int(sender.value)
        numberOfPhotos.stepLabel.text = "\(settings.numberOfPhotos)"
    }

    func timeBetweenPhotosWasStepped(_ sender: UIStepper) {
        print("timeBetweenPhotosWasStepped -> \(sender.value)")
        settings.timeBetweenPhotos = sender.value
        timeBetweenPhotos.stepLabel.text = "\(settings.timeBetweenPhotos)"
    }
}
