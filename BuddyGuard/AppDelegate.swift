//
//  AppDelegate.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 5/23/16.
//  Copyright © 2016 Stewart Boyd. All rights reserved.
//

import UIKit
import UserNotifications
import AVFoundation
import CoreMotion
import AFHTTPRequestOperationLogger

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?
    var armedViewController: ArmedViewController? = nil
    let settings = SettingsInfo.singleton
    var backgroundTask: UIBackgroundTaskIdentifier? = nil
    let forceWelcome = false

    /// This Method is used to determine if the normal BuddyGuard screen is entered
    /// or the welcome screen.
    ///
    /// - Parameters:
    ///   - application:
    ///   - launchOptions:
    /// - Returns: 
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        //let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        //let vc = mainStoryboard.instantiateViewController(withIdentifier: "TestViewController") as! TestViewController
        //window?.rootViewController = vc

        // Override point for customization after application launch.
        let mainStoryboard = UIStoryboard(name: GlobalString.MainStoryboard, bundle: nil)
        let mainViewController = mainStoryboard.instantiateViewController(withIdentifier: GlobalString.MainNavigationController) as! UINavigationController
        
        if !forceWelcome && settings.wasLaunchedBefore {
            window?.rootViewController = mainViewController
        }
        else {
            let welcomeStoryboard = UIStoryboard(name: GlobalString.WelcomeStoryboard, bundle: nil)
            let cameraQueryRootController = welcomeStoryboard.instantiateViewController(withIdentifier: GlobalString.CameraAccessViewController) as! CameraAccessViewController
            window?.rootViewController = cameraQueryRootController
            settings.wasLaunchedBefore = true
        }
        
        
        let disarmAction = UNNotificationAction(identifier: "DISARM_ACTION", title: "Disarm", options: .authenticationRequired)
        
        let alarmArmed = UNNotificationCategory(identifier: "ARMED", actions: [disarmAction], intentIdentifiers: [], options: .customDismissAction)
        let center = UNUserNotificationCenter.current()
        center.delegate = self
        center.setNotificationCategories([alarmArmed])
        
        
        
        
        

        return true
    }

    private func storyboardViewController(storyboardID: String, viewControllerStoryID: String) -> UIViewController {
        return UIStoryboard(name: storyboardID, bundle: nil) .
            instantiateViewController(withIdentifier: viewControllerStoryID)
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        

        if let armed = armedViewController {
            //
            print("armed view controller was set")
            if armed.isAlarmTriggerActivated {
                print("armed state")
                let alarmTrigger = armed.alarmTrigger
                backgroundTask = application.beginBackgroundTask(withName: "TransferAlarmTrigger") {
                    print("background being killed. Sorry")
                    if let backgroundTask = self.backgroundTask {
                        let audioFilepath = Bundle.main.path(forResource: "test", ofType: "wav")
                        let continuatorAudioPlayer = try! AudioPlayer(filepath: audioFilepath!,
                                                                      fromVolume: 1,
                                                                      toVolume: 1,
                                                                      numberOfLoops: 0)
                        let continuator = AccelerometerAlarmTriggerContinuator(audioPlayer: continuatorAudioPlayer, alarmTrigger: armed.alarmTrigger as! AccelerometerAlarmTrigger)
                        continuator.runInBackground()
                        application.endBackgroundTask(backgroundTask)
                    } else {
                        print("continuator not started")
                    }
                    
                    self.backgroundTask = UIBackgroundTaskInvalid
                    
                }
                DispatchQueue.global(qos: .background).async {

                    if let initialState = alarmTrigger?.initialState {
                        alarmTrigger?.stop()
                        alarmTrigger?.reset()
                        alarmTrigger?.startWithState(initialState: initialState)
                    }

                }

            } else {
                print("not in armed stated")
            }
            
            
        } else {
            print("not set")
        }
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        print("forgeground")
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        if response.notification.request.content.categoryIdentifier == "ARMED"{
            if response.actionIdentifier == "DISARM_ACTION"{
                if let armed = armedViewController{
                    if armed.isAlarmTriggerActivated{
                        armed.verificationAuthorized()
                    }
                }
                
            }
        }
        
        
        
    }


}

