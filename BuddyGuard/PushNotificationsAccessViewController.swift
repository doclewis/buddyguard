//
//  PushNotificationsAccessViewController.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 2/17/17.
//  Copyright © 2017 Stewart Boyd. All rights reserved.
//

import Foundation
import UserNotifications
import EAIntroView


class PushNotificationsAccessViewController: UIViewController {
 

    @IBOutlet weak var introView: EAIntroView!
    
    override func viewDidLoad() {
        introView.delegate = self
        print("MasaterViewController.viewDidLoad")
        let page1: EAIntroPage = EAIntroPage();
        page1.title = "Buddy Guard can do cool things with notifications";
        page1.desc = "Buddy Guard has the ability to send you notificaitons on your Apple Watch if your Guard has been activated"
        page1.bgColor = UIColor.white
        page1.titleIconView = UIImageView(image: UIImage(named: "notification"))
        page1.titleColor = UIColor.black
        page1.descColor = UIColor.black
        let page2: EAIntroPage = EAIntroPage()
        page2.title = "BuddyGuard would like to send notifications"
        page2.titleIconView = UIImageView(image: UIImage(named: "notification"))
        let page3: EAIntroPage = EAIntroPage()
        introView.pages = [page1, page2, page3]
        introView.skipButton.isHidden = true
    }

    func loadNextStoryboard() {
        DispatchQueue.main.async {
            let mainStoryboard = UIStoryboard(name: GlobalString.WelcomeStoryboard, bundle: nil)
            let setEmailViewController = mainStoryboard.instantiateViewController(withIdentifier: GlobalString.SetEmailViewController) as! SetEmailViewController
            self.present(setEmailViewController, animated: true)
        }
    }
}


//# Mark: - EAIntroViewDelegate Methods
extension PushNotificationsAccessViewController: EAIntroDelegate {

    
    func intro(_ introView: EAIntroView!, pageAppeared page: EAIntroPage!, with pageIndex: UInt) {
        //<div>Icons made by <a href="http://www.flaticon.com/authors/madebyoliver" title="Madebyoliver">Madebyoliver</a> from <a href="http://www.flaticon.com" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
        print("pageAppeared")
        let pageCount: UInt = UInt(introView.pages.count)
        //on last page show access dialog
        if pageCount - 1 == pageIndex {
            _ = UIStoryboard(name: "Main", bundle: nil)
            //let mainViewController = mainStoryboard.instantiateViewController(withIdentifier: "MainNavigationController") as! UINavigationController

            print("last page appeared")
            let center = UNUserNotificationCenter.current()
            center.requestAuthorization(options: [.alert, .sound]) { (granted, error) in
                // Enable or disable features based on authorization.
                self.loadNextStoryboard()
            }
        }
    }
    //- (void)introWillFinish:(EAIntroView *)introView wasSkipped:(BOOL)wasSkipped;
    //- (void)introDidFinish:(EAIntroView *)introView wasSkipped:(BOOL)wasSkipped;
    //- (void)intro:(EAIntroView *)introView pageAppeared:(EAIntroPage *)page withIndex:(NSUInteger)pageIndex;
    //- (void)intro:(EAIntroView *)introView pageStartScrolling:(EAIntroPage *)page withIndex:(NSUInteger)pageIndex;
    //- (void)intro:(EAIntroView *)introView pageEndScrolling:(EAIntroPage *)page withIndex:(NSUInteger)pageIndex;
    
    
}
