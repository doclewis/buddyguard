//
//  SettingsInfo.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 8/29/16.
//  Copyright © 2016 Stewart Boyd. All rights reserved.
//

//import Foundation

struct SettingsItem {
    let key: String
    var defaultValue: AnyObject
}


class SettingsInfo {
    static let singleton = SettingsInfo()
    
    static let alarmAudioFilenameSettings = SettingsItem(key: "Alarm Audio Filename",
                                                         defaultValue: GlobalString.alarmLydia as AnyObject)
    static let alarmAudioFilenameExtensionSettings = SettingsItem(key: "Alarm Audio Filename Extension",
                                                         defaultValue: "m4a" as AnyObject)
    static let wasLaunchedBeforeSettings = SettingsItem(key: "Application Has Been Opened", defaultValue: false as AnyObject)
    static let timeUntilAlarmArmsSettings = SettingsItem(key: "Time Until Alarm Arms", defaultValue: 5.0 as AnyObject)
    static let timeUntilAlarmSoundsSettings = SettingsItem(key: "Time Until Alarm Sounds", defaultValue: 2.0 as AnyObject)
    static let sensitivitySettings = SettingsItem(key: "Sensitivity", defaultValue: 1.0 as AnyObject)
    static let ledOnAlarmSettings = SettingsItem(key: "LED on Alarm", defaultValue: false as AnyObject)
    static let vibrateOnAlarmSettings = SettingsItem(key: "Vibrate On Alarm", defaultValue: true as AnyObject)
    static let useTouchIDSettings = SettingsItem(key: "Use Touch ID", defaultValue: true as AnyObject)
    static let usePhotoCaptureSettings = SettingsItem(key: "Use Photo Capture", defaultValue: true as AnyObject)
    static let numberOfPhotosSettings = SettingsItem(key: "Number Of Photos", defaultValue: 1 as AnyObject)
    static let timeBetweenPhotosSettings = SettingsItem(key: "Time Between Photos", defaultValue: 1.0 as AnyObject)
    static let emailString: String? = nil
    static let emailSettings = SettingsItem(key: "Email Address", defaultValue: emailString as AnyObject)
    static let useEmailSettings = SettingsItem(key: "Use Email", defaultValue: true as AnyObject)
    
    let defaults = UserDefaults.standard
    var defaultsDictionaryRepresentation = UserDefaults.standard.dictionaryRepresentation()
    var sensitivityConverter = SensitivityConverter(max: 1.0, min: 0.0)

    var alarmAudioFilename: String {
        get {
            if let _ = defaultsDictionaryRepresentation[SettingsInfo.alarmAudioFilenameSettings.key] {
                return defaults.string(forKey: SettingsInfo.alarmAudioFilenameSettings.key)!
            } else {
                return SettingsInfo.alarmAudioFilenameSettings.defaultValue as! String
            }
        }
        set {
            defaults.set(newValue, forKey: SettingsInfo.alarmAudioFilenameSettings.key)
            defaultsDictionaryRepresentation[SettingsInfo.alarmAudioFilenameSettings.key] = newValue
        }
    }

    
    var alarmAudioFilenameExtension: String {
        get {
            if let _ = defaultsDictionaryRepresentation[SettingsInfo.alarmAudioFilenameExtensionSettings.key] {
                return defaults.string(forKey: SettingsInfo.alarmAudioFilenameExtensionSettings.key)!
            } else {
                return SettingsInfo.alarmAudioFilenameExtensionSettings.defaultValue as! String
            }
        }
        set {
            defaults.set(newValue, forKey: SettingsInfo.alarmAudioFilenameExtensionSettings.key)
            defaultsDictionaryRepresentation[SettingsInfo.alarmAudioFilenameExtensionSettings.key] = newValue
        }
    }

    var wasLaunchedBefore: Bool {
        get {
            if let _ = defaultsDictionaryRepresentation[SettingsInfo.wasLaunchedBeforeSettings.key] {
                return defaults.bool(forKey: SettingsInfo.wasLaunchedBeforeSettings.key)
            } else {
                return SettingsInfo.wasLaunchedBeforeSettings.defaultValue as! Bool
            }
        }
        set {
            defaults.set(newValue, forKey: SettingsInfo.wasLaunchedBeforeSettings.key)
            defaultsDictionaryRepresentation[SettingsInfo.wasLaunchedBeforeSettings.key] = newValue
        }
    }

    var useTouchID: Bool {
        get {
            if let _ = defaultsDictionaryRepresentation[SettingsInfo.useTouchIDSettings.key] {
                return defaults.bool(forKey: SettingsInfo.useTouchIDSettings.key)
            } else {
                return SettingsInfo.useTouchIDSettings.defaultValue as! Bool
            }
        }
        set {
            defaults.set(newValue, forKey: SettingsInfo.useTouchIDSettings.key)
            defaultsDictionaryRepresentation[SettingsInfo.useTouchIDSettings.key] = newValue
        }
    }

    var timeUntilAlarmArms: Double {
        get {
            if let _ = defaultsDictionaryRepresentation[SettingsInfo.timeUntilAlarmArmsSettings.key] {
                return defaults.double(forKey: SettingsInfo.timeUntilAlarmArmsSettings.key)
            } else {
                return SettingsInfo.timeUntilAlarmArmsSettings.defaultValue as! Double
            }
        }
        set {
            defaults.set(newValue, forKey: SettingsInfo.timeUntilAlarmArmsSettings.key)
            defaultsDictionaryRepresentation[SettingsInfo.timeUntilAlarmArmsSettings.key] = newValue
        }
    }
    
    var timeUntilAlarmSounds: Double {
        get {
            if let _ = defaultsDictionaryRepresentation[SettingsInfo.timeUntilAlarmSoundsSettings.key] {
                return defaults.double(forKey: SettingsInfo.timeUntilAlarmSoundsSettings.key)
            } else {
                return SettingsInfo.timeUntilAlarmSoundsSettings.defaultValue as! Double
            }
        }
        set {
            defaults.set(newValue, forKey: SettingsInfo.timeUntilAlarmSoundsSettings.key)
            defaultsDictionaryRepresentation[SettingsInfo.timeUntilAlarmSoundsSettings.key] = newValue
        }
    }
    
    var sensitivity: Double {
        get {
            if let _ = defaultsDictionaryRepresentation[SettingsInfo.sensitivitySettings.key] {
                return defaults.double(forKey: SettingsInfo.sensitivitySettings.key)
            } else {
                return SettingsInfo.sensitivitySettings.defaultValue as! Double
            }
        }
        set {
            defaults.set(newValue, forKey: SettingsInfo.sensitivitySettings.key)
            defaultsDictionaryRepresentation[SettingsInfo.sensitivitySettings.key] = newValue
        }
    }
    
    var ledOnAlarm: Bool {
        get {
            if let _ = defaultsDictionaryRepresentation[SettingsInfo.ledOnAlarmSettings.key] {
                return defaults.bool(forKey: SettingsInfo.ledOnAlarmSettings.key)
            } else {
                return SettingsInfo.ledOnAlarmSettings.defaultValue as! Bool
            }
        }
        set {
            defaults.set(newValue, forKey: SettingsInfo.ledOnAlarmSettings.key)
            defaultsDictionaryRepresentation[SettingsInfo.ledOnAlarmSettings.key] = newValue
        }
    }
    
    var vibrateOnAlarm: Bool {
        get {
            if let _ = defaultsDictionaryRepresentation[SettingsInfo.vibrateOnAlarmSettings.key] {
                return defaults.bool(forKey: SettingsInfo.vibrateOnAlarmSettings.key)
            } else {
                return SettingsInfo.vibrateOnAlarmSettings.defaultValue as! Bool
            }
        }
        set {
            defaults.set(newValue, forKey: SettingsInfo.vibrateOnAlarmSettings.key)
            defaultsDictionaryRepresentation[SettingsInfo.vibrateOnAlarmSettings.key] = newValue
        }
    }

    var usePhotoCapture: Bool {
        get {
            if let _ = defaultsDictionaryRepresentation[SettingsInfo.usePhotoCaptureSettings.key] {
                return defaults.bool(forKey: SettingsInfo.usePhotoCaptureSettings.key)
            } else {
                return SettingsInfo.usePhotoCaptureSettings.defaultValue as! Bool
            }
        }
        set {
            defaults.set(newValue, forKey: SettingsInfo.usePhotoCaptureSettings.key)
            defaultsDictionaryRepresentation[SettingsInfo.usePhotoCaptureSettings.key] = newValue
        }
    }

    var numberOfPhotos: Int {
        get {
            if let _ = defaultsDictionaryRepresentation[SettingsInfo.numberOfPhotosSettings.key] {
                return defaults.integer(forKey: SettingsInfo.numberOfPhotosSettings.key)
            } else {
                return SettingsInfo.numberOfPhotosSettings.defaultValue as! Int
            }
        }
        set {
            defaults.set(newValue, forKey: SettingsInfo.numberOfPhotosSettings.key)
            defaultsDictionaryRepresentation[SettingsInfo.numberOfPhotosSettings.key] = newValue
        }
    }

    var timeBetweenPhotos: Double {
        get {
            if let _ = defaultsDictionaryRepresentation[SettingsInfo.timeBetweenPhotosSettings.key] {
                return defaults.double(forKey: SettingsInfo.timeBetweenPhotosSettings.key)
            } else {
                return SettingsInfo.timeBetweenPhotosSettings.defaultValue as! Double
            }
        }
        set {
            defaults.set(newValue, forKey: SettingsInfo.timeBetweenPhotosSettings.key)
            defaultsDictionaryRepresentation[SettingsInfo.timeBetweenPhotosSettings.key] = newValue
        }
    }
    
    var email: String? {
        get {
            
            if let _ = defaultsDictionaryRepresentation[SettingsInfo.emailSettings.key] {
                return defaults.string(forKey: SettingsInfo.emailSettings.key)
            } else {
                return SettingsInfo.emailSettings.defaultValue as? String
            }
            
        }
        set {
            defaults.set(newValue, forKey: SettingsInfo.emailSettings.key)
            defaultsDictionaryRepresentation[SettingsInfo.emailSettings.key] = newValue
        }
    }
    var useEmail: Bool {
        get {
            if let _ = defaultsDictionaryRepresentation[SettingsInfo.useEmailSettings.key] {
                return defaults.bool(forKey: SettingsInfo.useEmailSettings.key)
            } else {
                return SettingsInfo.useEmailSettings.defaultValue as! Bool
            }
        }
        set {
            defaults.set(newValue, forKey: SettingsInfo.useEmailSettings.key)
            defaultsDictionaryRepresentation[SettingsInfo.useEmailSettings.key] = newValue
        }
    }

    private init() {}
    
}
