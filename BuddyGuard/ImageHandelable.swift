//
//  ImageHandelable.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 1/2/17.
//  Copyright © 2017 Stewart Boyd. All rights reserved.
//

import Foundation
import Photos

protocol ImageHandleable {
    var image: CMSampleBuffer? {get set}
    func process(imageData: CMSampleBuffer)
}
