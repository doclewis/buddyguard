import UIKit
import Validator


final class SwitchTableViewCell2: ValidatorTextViewCell {
    
    var _valueChanged: ((_ sender: UISwitch, _ cell: SwitchTableViewCell2) -> ())? = nil
    @IBOutlet weak var uiSwitch: UISwitch!

    @IBAction func valueChanged(_ sender: UISwitch) {
        _valueChanged?(sender, self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //textField.validateOnInputChange(enabled: true)
        //textField.validationHandler = { result in self.updateValidationState(result: result) }
    }
    
    override func prepareForReuse() {
        //textField.text = ""
    }
    
}
