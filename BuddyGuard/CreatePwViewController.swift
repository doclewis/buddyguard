//
//  CreatePwViewController.swift
//  BuddyGuard
//
//  Created by Justin Salazar on 6/6/16.
//  Copyright © 2016 Stewart Boyd. All rights reserved.
//


import UIKit

class CreatePwViewController: UIViewController {
    
    let MyKeychainWrapper = KeychainWrapper()
    var numberpadViewController: NumberpadViewController!


    override func viewDidLoad() {
        super.viewDidLoad()
        numberpadViewController.touchIdButton.isHidden = true
        //numberpadViewController.touchIdButton.imageView?.isHidden = true
        //numberpadViewController.touchIdButton.imageView?.alpha = 0.0
        numberpadViewController.passwordLength = 4
        //numberpadViewController.touchIdButton.isHidden = true
        numberpadViewController.touchIdButton.isEnabled = false
    }
    
    func savePassword(newPassword: String) -> Bool {
        
        guard (newPassword != "" || newPassword.characters.count == 4) else {
            let alertView = UIAlertController(title: "Invalid Password", message: "Password must be a 4 digit number" as String, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertView.addAction(okAction)
            self.present(alertView, animated: true, completion: nil)
            return false;
        }
        
        
        if UserDefaults.standard.object(forKey: "default") == nil {
            UserDefaults.standard.setValue(newPassword, forKey: "default")
        }
        MyKeychainWrapper.mySetObject(newPassword, forKey: kSecValueData)
        MyKeychainWrapper.writeToKeychain()
        
        performSegue(withIdentifier: GlobalString.SegueCreatePasswordToMain, sender: self)
        return true
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


    // MARK: - Navigation
 
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == GlobalString.CreatePasswordNumberpadContainer){
            numberpadViewController = (segue.destination as! NumberpadViewController)
            //numberpadViewController.verificationAuthorized = verificationAuthorized
            numberpadViewController.attemptPassword = savePassword(newPassword:)
        }
    }
 
}
