//
//  PhotoCapturePercept.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 1/22/17.
//  Copyright © 2017 Stewart Boyd. All rights reserved.
//

import Foundation
import Photos


class PhotoCapturePercept: RepeaterPercept {
    
    var photoCapture: PhotoCapture?
    
    /// PhotoCapturePercept is a subclass of RepeaterPercept.
    /// Used to repeatedly take photos. A photo capture object
    /// will have to be externally created an added as attribute
    ///
    /// - Parameters:
    ///   - timesToCall: Int indicating number of times to take a photo (can be indefinite).
    ///   - initialDelay: Double indicating number of seconds to delay before first take a photo
    ///   - delayBetweenCalls: Double indicating number of seconds between take a photoa
    ///   - runLoop: RunLoop
    ///   - completedCallback: callback method called after last taken photo
    override init(timesToCall: Int, initialDelay: Double, delayBetweenCalls: Double, runLoop: RunLoop, completedCallback: ((Bool) -> ())? = nil)  {
        super.init(timesToCall: timesToCall,
                   initialDelay: initialDelay,
                   delayBetweenCalls: delayBetweenCalls,
                   runLoop: runLoop,
                   completedCallback: completedCallback)
        repeater.functionToCall = { _, _ in
            print("take photo after alarm...\(index)")
            self.photoCapture?.snapStillImage()
        }
    }
    
    /// Convenience init which allows the user to specify a PhotoCapture object
    ///
    /// - Parameters:
    ///   - timesToCall: Int indicating number of times to take a photo (can be indefinite).
    ///   - initialDelay: Double indicating number of seconds to delay before first take a photo
    ///   - delayBetweenCalls: Double indicating number of seconds between take a photoa
    ///   - runLoop: RunLoop
    ///   - completedCallback: callback method called after last taken photo
    convenience init (photoCapture: PhotoCapture, numberOfPhotos: Int, initialDelay: Double, delayBetweenCalls: Double, runLoop: RunLoop, completedCallback: ((Bool) -> ())? = nil) {
        self.init(timesToCall: numberOfPhotos,
                  initialDelay: initialDelay,
                  delayBetweenCalls: delayBetweenCalls,
                  runLoop: runLoop,
                  completedCallback: completedCallback)
        self.photoCapture = photoCapture
        self.photoCapture?.photoCaptureDelegate 
    }
    
    /// Used to startSession of internal photoCapture device.
    /// Note a Session must be started before data from an input (camera)
    /// can take place)
    ///
    /// - Parameter cameraPosition: position of camera to start session for
    func startSession(cameraPosition: AVCaptureDevicePosition) {
        photoCapture?.startSession()
        photoCapture?.addInput(cameraPosition)
    }
    
    /// Stop currently run session. If not session started this method does nothing
    func stopSession() {
        photoCapture?.stopSession()
    }
    
    /// Method to start photoCapture.
    /// Prints errro and does nothing else if photoCapture not properly started
    /// and configured
    internal override func start() {
        if (photoCapture?.isStarted == true) {
            super.start()
        } else {
            print("photoCapture.session has not been started or . No capture will take place")
        }
    }
}
