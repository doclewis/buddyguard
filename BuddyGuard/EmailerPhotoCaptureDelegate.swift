//
//  ImageEmailer.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 1/2/17.
//  Copyright © 2017 Stewart Boyd. All rights reserved.
//

import Foundation
import Photos
import mailgun


class EmailerPhotoCaptureDelegate: NSObject, AVCapturePhotoCaptureDelegate {

    let emailMeta: EmailMeta
    var imageType: ImageAttachmentType
    var imageName: String
    lazy var emailSender = EmailSender()
    
    
    init(emailMeta: EmailMeta, imageName: String, imageType: ImageAttachmentType) {
        self.emailMeta = emailMeta
        self.imageName = imageName
        self.imageType = imageType
    }

    var imageExtension: String {
        switch imageType{
        case ImageAttachmentType.PNGFileType:
            return ".png"
        case ImageAttachmentType.JPEGFileType:
            return ".jpg"
        }
    }

    public func capture(_ captureOutput: AVCapturePhotoOutput,
                        didFinishProcessingPhotoSampleBuffer photoSampleBuffer: CMSampleBuffer?,
                        previewPhotoSampleBuffer: CMSampleBuffer?,
                        resolvedSettings: AVCaptureResolvedPhotoSettings,
                        bracketSettings: AVCaptureBracketedStillImageSettings?,
                        error: Error?) {
        

        guard error == nil else {
            print("Error on capture")
            print(error?.localizedDescription)
            return
        }
        guard let sampleBuffer = photoSampleBuffer else {
            return
        }
        //guard let previewBuffer = previewPhotoSampleBuffer else {
        //    return
        //}
        guard let dataImage = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: sampleBuffer, previewPhotoSampleBuffer: previewPhotoSampleBuffer) else {
            return
        }
        
        let image  = UIImage(data: dataImage)
        let emailSender = EmailSender()
        guard let _ = image else {
            print("Image was not created properly from captured data")
            return
        }
        emailSender.send(to: emailMeta.to, subject: emailMeta.subject, body: emailMeta.body, image: image!, imageName: imageName + imageExtension, type: ImageAttachmentType.JPEGFileType)
        
        
    }


}
