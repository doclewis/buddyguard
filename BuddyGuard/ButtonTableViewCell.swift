//
//  ButtonTableViewCell.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 5/29/17.
//  Copyright © 2017 Stewart Boyd. All rights reserved.
//

import UIKit


class ButtonTableViewCell: UITableViewCell {
    
    @IBOutlet weak var button: UIButton!
    var _buttonClicked: (() -> ())?
    @IBAction func buttonClicked(_ sender: Any) {
        _buttonClicked?()
    }
}
