//
//  ImageSaver.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 1/2/17.
//  Copyright © 2017 Stewart Boyd. All rights reserved.
//

import Foundation
import Photos

class ImageSaver: ImageHandleable {
    
    var image: CMSampleBuffer?
    
    static func savePhotoToLibrary(imageData: CMSampleBuffer) {
        ImageSaver.saveSampleBufferToPhotoLibrary(imageData,
                                                  previewSampleBuffer: nil,
                                                  completionHandler: { success, error in
                                                    if success {
                                                        print("Added JPEG photo to library.")
                                                    } else {
                                                        print("Error adding JPEG photo to library: \(String(describing: error))")
                                                    }
        })
    }
    
    static func saveSampleBufferToPhotoLibrary(_ sampleBuffer: CMSampleBuffer,
                                               previewSampleBuffer: CMSampleBuffer?,
                                               completionHandler: ((_ success: Bool, _ error: Error?) -> Void)?) {
        ImageSaver.checkPhotoLibraryAuthorization(){ authorized in
            guard authorized else {
                print("Permission to access photo library denied.")
                completionHandler?(false, nil)
                return
            }
            
            guard let jpegData = AVCapturePhotoOutput.jpegPhotoDataRepresentation(
                forJPEGSampleBuffer: sampleBuffer,
                previewPhotoSampleBuffer: previewSampleBuffer)
                else {
                    print("Unable to create JPEG data.")
                    completionHandler?(false, nil)
                    return
            }
            
            PHPhotoLibrary.shared().performChanges( {
                let creationRequest = PHAssetCreationRequest.forAsset()
                creationRequest.addResource(with: PHAssetResourceType.photo, data: jpegData, options: nil)
            }, completionHandler: { success, error in
                DispatchQueue.main.async {
                    completionHandler?(success, error)
                }
            })
        }
    }
    
    static func checkPhotoLibraryAuthorization(completionHandler: @escaping ((_ authorized: Bool) -> Void)) {
        switch PHPhotoLibrary.authorizationStatus() {
        case .authorized:
            // The user has previously granted access to the photo library.
            completionHandler(true)
            
        case .notDetermined:
            // The user has not yet been presented with the option to grant photo library access so request access.
            PHPhotoLibrary.requestAuthorization({ status in
                completionHandler((status == .authorized))
            })
            
        case .denied:
            // The user has previously denied access.
            completionHandler(false)
            
        case .restricted:
            // The user doesn't have the authority to request access e.g. parental restriction.
            completionHandler(false)
        }
    }
    
    internal func process(imageData: CMSampleBuffer) {
        ImageSaver.savePhotoToLibrary(imageData: imageData)
    }
}
