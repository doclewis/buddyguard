import UIKit
import Validator

final class StepTableViewCell2: ValidatorTextViewCell {
    
    

    @IBOutlet weak var stepper: UIStepper!
    var _valueChanged: ((_ sender: UIStepper, _ counter: UILabel) -> ())? = nil
    
    @IBAction func valueChanged(_ sender: UIStepper) {
        _valueChanged?(sender, statusLabel)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        //textField.validateOnInputChange(enabled: true)
        //textField.validationHandler = { result in self.updateValidationState(result: result) }
    }
    
    override func prepareForReuse() {
        //textField.text = ""
    }
    
}
