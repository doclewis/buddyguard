//
//  AudioSessionConfigurator.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 5/20/17.
//  Copyright © 2017 Stewart Boyd. All rights reserved.
//

import Foundation
import AVFoundation

class AudioSessionConfigurator {

    let audioSession: AVAudioSession = AVAudioSession.sharedInstance()
    
    init() {
    }

    func defaultConfigure() {
        try? audioSession.setCategory(AVAudioSessionCategoryPlayback)
        try? audioSession.setActive(true)
    }
}
