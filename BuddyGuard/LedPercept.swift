//
//  LedPercept.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 1/22/17.
//  Copyright © 2017 Stewart Boyd. All rights reserved.
//

import Foundation
import Photos


class LedPercept: RepeaterPercept {
    
    
    var device: AVCaptureDevice?
    
    var ledOn: Bool {
        return device?.torchMode == AVCaptureTorchMode.on
    }
    
    /// LedPercept is a subclass of RepeaterPercept.
    /// Its a class that encapsulates the creation of an object
    /// which repeatedly turns on LED of the device
    ///
    /// - Parameters:
    ///   - timesToCall: Int indicating number of times to flash LED (can be indefinite).
    ///   - initialDelay: Double indicating number of seconds to delay before first flash of LED
    ///   - delayBetweenCalls: Double indicating number of seconds between flashes of LED
    ///   - runLoop: RunLoop
    ///   - completedCallback: callback method called after last flash of LED
    override init(timesToCall: Int, initialDelay: Double, delayBetweenCalls: Double, runLoop: RunLoop, completedCallback: ((Bool) -> ())? = nil)  {
        super.init(timesToCall: timesToCall,
                   initialDelay: initialDelay,
                   delayBetweenCalls: delayBetweenCalls,
                   runLoop: runLoop,
                   completedCallback: completedCallback)
        device = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        repeater.functionToCall = { _, _ in
            self.activateLED(turnOn: !self.ledOn)
        }
        repeater.completedCallback = {completed in
            completedCallback?(completed)
            self.activateLED(turnOn: false)
        }
    }
    
    /// Method, which based on arguments turns on or turns off LED
    ///
    /// - Parameter turnOn: Bool, that when true, turns on LED. Otherwise turns off
    func activateLED(turnOn: Bool) {
        
        if let d = device {
            //print("turn on = \(turnOn)")
            //print("d.torchMode = \(d.torchMode)")
            //print("d.isTorchActive = \(d.isTorchActive)")
            //print("d.torchLevel = \(d.torchLevel)")
            if (d.hasTorch) {
                do {
                    try d.lockForConfiguration()
                    if turnOn {
                        do {
                            //print("turn on")
                            try d.setTorchModeOnWithLevel(1.0)
                            //print("no error")
                        } catch {
                            print(error)
                        }
                    } else {
                        d.torchMode = AVCaptureTorchMode.off
                    }
                    d.unlockForConfiguration()
                    
                } catch {
                    print(error)
                }
            }
        }
        else {
            print("device not available")
        }
    }
}
