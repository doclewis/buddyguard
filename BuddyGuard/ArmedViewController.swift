//
//  ArmedViewController.swift
//  BuddyGuard
//
//  Created by Justin Salazar on 6/7/16.
//  Copyright © 2016 Stewart Boyd. All rights reserved.
//

import CoreMotion
import UIKit
import AVFoundation
import Photos
import LocalAuthentication
import mailgun
import UserNotifications

class ArmedViewController: UIViewController, UINavigationControllerDelegate, AlarmTriggerableDelegate, AVCapturePhotoCaptureDelegate, PasswordSubstantiatable {
    
    
    //# MARK: - Attributes
    let MyKeychainWrapper = KeychainWrapper()
    var activateAlarmTimer : Timer?
    var alarm : Alarm!
    var alarmTrigger: AlarmTriggerable!
    var photoCapture: PhotoCapture!
    var photoCapturePercept: PhotoCapturePercept!
    var emailPercept: EmailPercept!
    var previewLayer: AVCaptureVideoPreviewLayer!
    var imageProcessor: ImageHandleable = ImageSaver()
    var imageCaptureBuffer: CMSampleBuffer?
    var previewImageCaptureBuffer: CMSampleBuffer?


    var isAlarmTriggerActivated: Bool = false
    var timeToSound: Double!
    var timeToArmAlarm: Double!
    var doesFlashLEDOnAlarm: Bool!
    var doesVibrateOnAlarm: Bool!
    var doesUsePhotoCapture: Bool!
    var doesUseTouchID: Bool!
    var doesUseEmail: Bool!
    var numberOfPhotos: Int!
    var timeBetweenPhotos: Double!
    var threshold: AccelerometerState!
    var timeRemaining : Int!
    let settings = SettingsInfo.singleton
    var audioPlayer: AVAudioPlayer?
    var audioSession: AVAudioSession = AVAudioSession.sharedInstance()
    let notificationCenter = NotificationCenter.default

    //Repeaters
    var updateTopText: Repeater?
    var updateBottomText: Repeater!
    var updateProgressBar: Repeater!

    //# MARK: - IBOutput Attributes
    @IBOutlet weak var pwField: UITextField!
    @IBOutlet weak var bottomText: UILabel!
    @IBOutlet weak var topText: UILabel!
    @IBOutlet weak var disarmButton: UIButton!
    @IBOutlet weak var soundPlay: UILabel!
    @IBOutlet weak var circularProgress: KDCircularProgress!

    //# MARK: - Resources
    //let default_alarm = "alarm_lydia"
    var emailSender: EmailSender?

    //# MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        addDoneButtonOnKeyboard()

        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.armedViewController = self
        
        photoCapture = try! PhotoCapture(photoCaptureDelegate: self)
        try! photoCapture.defaultConfiguration()
        // Do any additional setup after loading the view.
        navigationController?.delegate = self;
        
        notificationCenter.addObserver(forName: NSNotification.Name.AVAudioSessionRouteChange, object: nil, queue: nil) { notification in
            print("Route Change")
            print("name = \(notification.name) and object \(String(describing: notification.object))")
        }
        notificationCenter.addObserver(forName: NSNotification.Name.AVCaptureSessionWasInterrupted, object: nil, queue: nil) {
            notification in
            print("AVCaptureSessionWasInterrupted")
            print("notifcation = \(String(describing: notification.userInfo))")
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        print("viewWillAppear")
        isAlarmTriggerActivated = false
        let fpath = Bundle.main.path(forResource: settings.alarmAudioFilename,
                                     ofType: settings.alarmAudioFilenameExtension)!
        print("fpath = \(fpath)")
        timeToSound = settings.timeUntilAlarmSounds
        timeToArmAlarm = settings.timeUntilAlarmArms
        doesVibrateOnAlarm = settings.vibrateOnAlarm
        doesFlashLEDOnAlarm = settings.ledOnAlarm
        doesUsePhotoCapture = settings.usePhotoCapture
        doesUseEmail = settings.useEmail
        let emailAddress = settings.email
        doesUseTouchID = settings.useTouchID
        numberOfPhotos = settings.numberOfPhotos
        timeBetweenPhotos = settings.timeBetweenPhotos
        threshold = settings.sensitivityConverter.sensitivities[settings.sensitivityConverter.indexFromVal(settings.sensitivity)] as! AccelerometerState
        //threshold = settings.sensitivityConverter.sensitivities[settings.sensitivityConverter.index].threshold as! AccelerometerState
        print("emailAddress = \(String(describing: emailAddress))")
        let audioPlayer = try! AudioPlayer(filepath: fpath,
                                           fromVolume: 0,
                                           toVolume: 1,
                                           numberOfLoops: AudioPlayer.infiniteLoop)
        alarm = Alarm(audioPlayer: audioPlayer,
                      delay: timeToSound,
                      doesLEDDisplay: doesFlashLEDOnAlarm,
                      doesVibrate: doesVibrateOnAlarm,
                      runLoop: RunLoop.main)
        
        alarmTrigger = AccelerometerAlarmTrigger(updateInterval: 0.2,
                                                     threshold: threshold)
        //does use photo capture is only useful if can send email too
        if doesUsePhotoCapture! && doesUseEmail{

            if let to = emailAddress {
                let phoneThiefFpath = Bundle.main.path(forResource: GlobalString.PhoneThiefEmail, ofType: "html")
                let phoneThiefBody = Utility.Read(fpath: phoneThiefFpath)
                let emailMeta = EmailMeta(to: to, subject: "Phone Thief Capture", body: phoneThiefBody!)
                let photoCaptureDelegate = EmailerPhotoCaptureDelegate(emailMeta: emailMeta, imageName: "image", imageType: ImageAttachmentType.JPEGFileType)
                photoCapture.photoCaptureDelegate = photoCaptureDelegate
                photoCapturePercept = PhotoCapturePercept(photoCapture: photoCapture,
                                                            numberOfPhotos: numberOfPhotos,
                                                            initialDelay: 0,
                                                            delayBetweenCalls: timeBetweenPhotos,
                                                            runLoop: RunLoop.main)
                alarm.addPercept(perceptable: photoCapturePercept)
            }
        }
        if doesUseEmail {
            DispatchQueue.global(qos: .userInitiated).async {
                if let to = emailAddress {
                    let findIphoneFpath = Bundle.main.path(forResource: GlobalString.GotoFindMyIphone, ofType: "html")
                    let findIphoneBody = Utility.Read(fpath: findIphoneFpath)
                    self.emailSender = EmailSender()
                    
                    
                    self.emailPercept = EmailPercept(sender: self.emailSender,
                                                     to: to,
                                                     subject: "Buddy Guard Alarm Triggered",
                                                     body: findIphoneBody!)
                    self.alarm.addPercept(perceptable: self.emailPercept)
                }

            }
        }
        alarmTrigger.delegate = self

        
        updateBottomText = Repeater(timesToCall: Int(timeToArmAlarm),
                                        delayBetweenCalls: 1,
                                        initialDelay: 0.0){ numCall, _ in
                                            print("gooo")
                                            let timeRemaining = Int(self.timeToArmAlarm) - numCall
                                            self.bottomText.text = "\(timeRemaining)"
        }
        let yellowCutoff = 0.60
        let redCutoff = 0.80
        let updatePerSecond = 10.0

        
        updateProgressBar = Repeater(totalDuration: Double(timeToArmAlarm),
                                         updatePerSecond: updatePerSecond,
                                         completedCallback: cleanupProgressbar) {numCall, totalNumCall in
                                            //self.circularProgress.layer.isHidden =  !self.circularProgress.layer.isHidden
                                            let progress = Double(numCall) / Double(totalNumCall)
                                            self.circularProgress.animateToAngle(360.0 * progress, duration: 1.0 / updatePerSecond, completion : nil)

                                            if progress >= redCutoff {
                                                self.circularProgress.glowAmount = 0.2
                                                self.circularProgress.setColors(UIColor.red)
                                            }
        }

        //when using touch id (no need for text field...unless touch id keeps failing)
        
        configureView(doesUseTouchID: doesUseTouchID)
        print("updateBottomText.start")
        updateBottomText.start()
        print("updateProgressBar.start")
        updateProgressBar.start()
        print("started both")
    }

    override func viewDidAppear(_ animated: Bool) {
        
        startLockTimer()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        deactivateAlarm()
        updateBottomText.stop()
        updateProgressBar.stop()
        updateTopText?.stop()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //# MARK - Other Methods

    /// pressDisarm is the response to clicking the disarm button.
    /// This method behaves different if touchID is being used.
    /// If touchID Being used. we launch the touchID dialog. Else
    /// this method stops the alarm and the trigger, if password correct
    /// otherwise shaeks screen
    ///
    /// - Parameter sender: UIButton
    @IBAction func pressDisarm(_ sender: UIButton) {
        if doesUseTouchID {
            authenticateUser()
        } else {
            processTextPasswordEntry()
        }
    }

    
    /// Configure Views based on whether or not using touch ID
    ///
    /// - Parameter doesUseTouchID: Indicates if using touch ID or not
    func configureView(doesUseTouchID: Bool) {
        pwField.isHidden = doesUseTouchID
        disarmButton.isHidden = !doesUseTouchID
        disarmButton.isHidden = true
        soundPlay.isHidden = true
    }

    /// Gets pwField text and verifies entered password
    func processTextPasswordEntry() {
        pwField.resignFirstResponder()
        authenticateTextPassword(text: pwField.text)
    }


    /// Checks a text string password
    ///
    /// - Parameter text: Potential password
    /// - Returns: Bool inidicating if password was valid
    func authenticateTextPassword(text: String?) -> Bool {
        if(text == MyKeychainWrapper.myObject(forKey: "v_Data") as? String){
            verificationAuthorized()
            return true
        }
        else{
            verificationRejected()
            return false
        }
    }

    func authenticateUser() {
        let touchIDManager = TouchIDManager()
        
        touchIDManager.authenticateUser(success: { () -> () in
            //OperationQueue.main.addOperation({ () -> Void in
            //    print("success bitch")
            //    self.verificationAuthorized()
            //})
            DispatchQueue.main.async {
                print("success bitch")
               self.verificationAuthorized()
            }

        }, failure: { (evaluationError: NSError) -> () in
            switch evaluationError.code {
            case LAError.Code.systemCancel.rawValue:
                print("Authentication cancelled by the system")
            //self.statusLabel.text = "Authentication cancelled by the system"
            case LAError.Code.userCancel.rawValue:
                print("Authentication cancelled by the user")
                
            //self.statusLabel.text = "Authentication cancelled by the user"
            case LAError.Code.userFallback.rawValue:
                print("User wants to use a password")
                self.pwField.isHidden = false
            case LAError.Code.touchIDNotEnrolled.rawValue:
                print("TouchID not enrolled")
                self.pwField.isHidden = false
            case LAError.Code.passcodeNotSet.rawValue:
                print("Passcode not set")
            default:
                print("Authentication failed")
            }
        })
    }

    func verificationAuthorized() {
        let _ = alarm.stop()
        alarmTrigger.stop()
        performSegue(withIdentifier: GlobalString.SegueArmedToMain, sender: self)
    }

    func verificationRejected() {
        SimpleAnimation.shake(view: view)
    }

    func startLockTimer(){
        
        activateAlarmTimer = Timer.scheduledTimer(timeInterval: TimeInterval(timeToArmAlarm), target: self, selector: #selector(activateAlarmTrigger), userInfo: nil, repeats: false)
    }



    /// Activate alarm and configure GUI to look appropriate
    func activateAlarmTrigger(){
        isAlarmTriggerActivated = true
        print("activateALarm")
        navigationController?.isNavigationBarHidden = true;
        disarmButton.isHidden = false
        //when using touch id (no need for text field...unless touch id keeps failing)
        pwField.isHidden = doesUseTouchID!;
        topText.numberOfLines = 2
        topText.text = "Alarm armed!\nEnter pin to deactivate"
        alarmTrigger.start()
        isAlarmTriggerActivated = true
        activateAlarmTimer = nil

    }

    /// Cleans up actions and objects associated with Alarm starting.
    func deactivateAlarm(){
        isAlarmTriggerActivated = false
        if let timer = activateAlarmTimer {
            timer.invalidate()
            activateAlarmTimer = nil
        }
        let _ = alarm.stop()
        if(doesUsePhotoCapture){
            photoCapture.stopSession()}
    }

    /// Asynchronous call which changes the view of the 
    /// ArmedViewController to be that of PhotoCapture sesion
    func addPhotoCaptureView() {
        print("addPhotoCaptureView")
        DispatchQueue.main.async {
            self.previewLayer = AVCaptureVideoPreviewLayer(session: self.photoCapture.session)
            if let _ = self.previewLayer {
                print("create previewLayer")
                self.previewLayer.frame = self.view.bounds
                self.previewLayer.opacity = 0.5
                self.view.layer.addSublayer(self.previewLayer)
            }
            else {
                print("no good")
            }
        }
    }

    func removePhotoCaptureView() {
        //remove previewLayer
    }

    func cleanupProgressbar(isCompleted: Bool) -> () {
        if isCompleted {
            //self.circularProgress.trackColor = UIColor.clear
            circularProgress.layer.isHidden = true
            print("This is called")
        }
    }

    //# MARK: - AlarmTriggerableDelegate Methods
    //alarm delegate method
    func startedReading(){
        print("startedReading")
    }
    
    //alarm delegate method
    func stoppedReading(){
        print("stoppedReading")
    }
    
    //alarm delegate method
    func initializedInitialState(_ initialState : GeneralState?){
        print("AlarmTrigger initialState set")
    }
    
    //alarm delegate method
    func triggered(){
        print("triggered")
        circularProgress.isHidden = false
        print("volume = \(String(describing: alarm.volume))")
        if(doesUsePhotoCapture){
            photoCapture.startSession()
            photoCapture.addInput(.front)
            addPhotoCaptureView()}
        
        let content = UNMutableNotificationContent()
        content.title = NSString.localizedUserNotificationString(forKey: "Alarm Triggered!", arguments: nil)
        content.body = NSString.localizedUserNotificationString(forKey: "Would you like to disarm?", arguments: nil)
        content.categoryIdentifier = "ARMED"
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 0.1, repeats: false)
        let request = UNNotificationRequest(identifier: "AlarmTriggered", content: content, trigger: trigger)
        let center = UNUserNotificationCenter.current()
        center.add(request) { (error : Error?) in
            if let theError = error {
                print(theError.localizedDescription)
            }
        }
        
        alarm.startOnDelay(runLoop: RunLoop.main)
        self.topText.text = "Alarm will sound"
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + self.timeToSound) {
            self.topText.text = "Alarm Sounding!"
        }
        
        updateBottomText = Repeater(timesToCall: Int(timeToSound),
                                    delayBetweenCalls: 1,
                                    initialDelay: 0.0,
                                    runLoop: RunLoop.main){ numCall, _ in
                                        print("updateBottomText = \(numCall)")
                                        let timeRemaining = Int(self.timeToSound) - numCall
                                        self.bottomText.text = "\(timeRemaining)"
        }
        updateBottomText?.start()
        updateTopText?.start()
        DispatchQueue.main.async{
            self.circularProgress.glowAmount = 0.0
            self.circularProgress.setColors(UIColor.white)
            self.circularProgress.angle = 0.0
            self.circularProgress.layer.isHidden = false
            self.circularProgress.animateToAngle(360.0, duration: self.timeToSound, completion: self.cleanupProgressbar)
            //self.circularProgress.animateToAngle(<#T##toAngle: Double##Double#>, duration: <#T##TimeInterval#>, completion: <#T##((Bool) -> Void)?##((Bool) -> Void)?##(Bool) -> Void#>)
        }

        /*
        let updateProgressBar = Repeater(totalDuration: Double(timeToArmAlarm),
                                     updatePerSecond: updatePerSecond,
                                     completedCallback: completedCallback) {numCall, totalNumCall in
                                        let progress = Double(numCall) / Double(totalNumCall)
                                        self.circularProgress.animateToAngle(360.0 * progress, duration: 1.0 / updatePerSecond, completion : nil)
                                        
                                        if progress >= redCutoff {
                                            self.circularProgress.glowAmount = 0.2
                                            self.circularProgress.setColors(UIColor.red)
                                        }
        }
 */
    }

    //# MARK: - AVCapturePhotoCaptureDelegate Methods
    func capture(_ captureOutput: AVCapturePhotoOutput,
                 didFinishProcessingPhotoSampleBuffer photoSampleBuffer: CMSampleBuffer?,
                 previewPhotoSampleBuffer: CMSampleBuffer?,
                 resolvedSettings: AVCaptureResolvedPhotoSettings,
                 bracketSettings: AVCaptureBracketedStillImageSettings?,
                 error: Error?) {
        print("this is the delegates capture1")
        guard error == nil, let photoSampleBuffer = photoSampleBuffer else {
            print("Error capturing photo: \(String(describing: error))")
            return
        }
        
        self.imageCaptureBuffer = photoSampleBuffer
        self.previewImageCaptureBuffer = previewPhotoSampleBuffer
    }

    func capture(_ captureOutput: AVCapturePhotoOutput,
                 didFinishCaptureForResolvedSettings resolvedSettings: AVCaptureResolvedPhotoSettings,
                 error: Error?) {
        guard error == nil else {
            print("Error in capture process: \(String(describing: error))")
            return
        }
        
        if let photoSampleBuffer = self.imageCaptureBuffer {
            self.imageProcessor.process(imageData: photoSampleBuffer)
        }
    }

    func addDoneButtonOnKeyboard()
    {
        let doneToolbar = KeyboardToolbarFactory.create(title: "Disarm", target: self, selector: #selector(ArmedViewController.processTextPasswordEntry))
        pwField.inputAccessoryView = doneToolbar

    }
    

     // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("prepareForSegue")
        navigationController?.isNavigationBarHidden = false;
    }
}


