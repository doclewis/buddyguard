//
//  AudioPlayer.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 12/11/16.
//  Copyright © 2016 Stewart Boyd. All rights reserved.
//

import Foundation
import AVFoundation


enum SbError: Error {
    case FileNotFound

}


class AudioPlayer: SoundPlayable {

    static let infiniteLoop = -1
    var audioPlayer: AVAudioPlayer
    var fader: Fader?

    /// Double indicating duration of underlying file
    var duration: Double {
        return self.audioPlayer.duration
    }
    
    /// Indicates nubmer of times to repeat audioPlayer
    var numberOfLoops: Int? {
        get {
            return audioPlayer.numberOfLoops
        }
        set{
            if let _ = newValue {
                audioPlayer.numberOfLoops = newValue!
            }
        }
    }
    
    /// Indicates underlying media file played by audioPlayer.
    /// Upon setting a new one a new audioPlayer is created and set
    var fileURL: URL? {
        
        get {
            return audioPlayer.url
        }
        set {
            let volume = audioPlayer.volume
            if let nv = newValue {
                if let _audioPlayer = try? AVAudioPlayer.init(contentsOf: nv) {
                    audioPlayer = _audioPlayer
                    audioPlayer.volume = volume
                }
            }
        }
    }
    
    /// A string representation of the filepath. It should be noted that the protocol
    /// is included (e.g. file://)
    var filepath : String?{

        get {
            return self.fileURL?.absoluteString
        }
        set{
            self.fileURL = URL.init(fileURLWithPath: newValue!);
        }
    }
    
    /// An AVAudioPlayer repeats indefinitely if
    /// the numberOfLoops is less than 0.
    /// If repeat_indefinitely is set to True then set numberOfLoops
    /// to -1 which is understood by AVAudioPlayer
    var doesRepeatIndefinitely : Bool{

        get{
            return numberOfLoops == nil || numberOfLoops! < 0;
        }
        set{
            if newValue{
                numberOfLoops = AudioPlayer.infiniteLoop;
            }
        }
    }

    /// Sets volume of audioPlayer. Only allows range to
    /// be between 0 and 1 inclusive
    var volume : Float{
        
        get{
            return audioPlayer.volume;
        }
        set{
            audioPlayer.volume = VolumeLimit.valueIsInRange(newValue);
        }
    }

    /// AudioPlayer in a class which encapsulates the creation and
    /// execution of AVAudioPlayers
    ///
    /// - Parameters:
    ///   - filepath: filepath pointing to a resource to be used as alarm
    ///   - volume: val between 0 and 1 corresponding to volume of AVAudioPlayer
    ///   - numberOfLoops: Indicates number of times to play audio (Negative is indefinitely)
    ///   - fader: Fader object determining how alarm play is faded in. If nil no fading occurs
    /// - Throws: An exception is thrown if AVAudioPlayer can't be constructed (generally broken filepath)
    init(filepath : String,
         volume : Float = VolumeLimit.maxVolume,
         numberOfLoops : Int = 0,
         fader : Fader? = nil) throws {
        
        let fileURL = URL.init(string: filepath)
        self.fader = fader
        if let _ = fileURL {
            audioPlayer = try AVAudioPlayer.init(contentsOf: fileURL!)
            audioPlayer.numberOfLoops = numberOfLoops;
            audioPlayer.volume = VolumeLimit.valueIsInRange(volume)
            audioPlayer.prepareToPlay()
        } else {
            throw SbError.FileNotFound
        }
    }

    /// Convenience initializer which, instead of specifying a fader, constructs one
    /// based on simple inputs
    ///
    /// - Parameters:
    ///   - filepath: filepath pointing to a resource to be used as alarm
    ///   - fromVolume: Float specifinyg inital Volume for player and starting point for Fader
    ///   - toVolume: Float specifying final Volume for player after Fade
    ///   - numberOfLoops: Indicates number of times to play audio (Negative is indefinitely)
    ///   - duration: Double indicating period of time, overwhcih, fade will occur
    ///   - velocity: Double
    ///   - faderRunLoop: RunLoop
    ///   - fadeCompletedCallback: Callback method called at end of Fade
    /// - Throws: <#throws value description#>
    convenience init(filepath: String,
                     fromVolume: Float,
                     toVolume: Float,
                     numberOfLoops: Int = 0,
                     duration: Double = Fader.defaultFadeDurationSeconds,
                     velocity: Double = Fader.defaultVelocity,
                     faderRunLoop: RunLoop = RunLoop.current,
                     fadeCompletedCallback: ((Bool) -> ())? = nil) throws {
        
        try self.init(filepath : filepath, volume : Float(fromVolume), numberOfLoops: numberOfLoops, fader: nil)
        self.fader = Fader(player: audioPlayer,
                           fromVolume: fromVolume,
                           toVolume: toVolume,
                           timesToCall: Int(duration * Fader.defaultVolumeAlterationsPerSecond),
                           delayBetweenCalls: 1.0/Fader.defaultVolumeAlterationsPerSecond,
                           velocity: velocity,
                           runLoop: faderRunLoop,
                           fadeCompletedCallback: fadeCompletedCallback)
    }

    /// Method for starting audioPlayer
    ///
    /// - Returns: Bool indicating if played (Note, no sound might be heard for other
    ///            Other reasons such as runLoop execution or ARC deleting underlying
    ///            AVAudioPlayer
    func play() -> Bool {
        let played = audioPlayer.play()
        fader?.fade()
        return played
    }

    /// Method for stopping audioPlayer
    ///
    /// - Returns: Always returns true
    func stop() -> Bool {
        audioPlayer.stop()
        return true;
    }

}
