//
//  AlarmTrigger.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 7/19/16.
//  Copyright © 2016 Stewart Boyd. All rights reserved.
//

import Foundation
import CoreLocation


class GPSAlarmTrigger : NSObject, AlarmTriggerable, CLLocationManagerDelegate {
    
    var initialState: GeneralState?
    var currentState: GeneralState?
    var threshold: GeneralState
    var updateInterval: Double = 999
    var delegate: AlarmTriggerableDelegate?
    var isCancelled : Bool = false
    var motionManager = CLLocationManager()
    var numberOfInitialStateSamples = 10
    
    fileprivate var semaphore = DispatchSemaphore(value: 0)
    var initialStateBlock : BlockOperation?
    var runStateBlock : BlockOperation?
    var locationQueue : [CLLocation] = []
    var locationQueueUpdate : DispatchQueue = DispatchQueue(label: "queue for updating location queue in serial fashion", attributes: [])

    lazy var stateQueue: OperationQueue = {
        var stateQueue = OperationQueue()
        stateQueue.name = "State Queue"
        stateQueue.maxConcurrentOperationCount = 1
        return stateQueue
    }()

    var desiredAccuracy : CLLocationAccuracy {
        get { return motionManager.desiredAccuracy}
        set { motionManager.desiredAccuracy = newValue }
    }

    var distanceFilter : CLLocationDistance {
        get { return motionManager.distanceFilter }
        set { motionManager.distanceFilter = newValue }
    }

    var allowsBackgroundLocationUpdates : Bool {
        get { return motionManager.allowsBackgroundLocationUpdates }
        set { motionManager.allowsBackgroundLocationUpdates = newValue }
    }
    
    /// GPSAlarmTrigger is a class used to Trigger Alarm
    ///
    /// - Parameters:
    ///   - updateInterval: Specifies how often to update GPS
    ///   - threshold: If a change, beyond this, is noted then the Alarm is triggered
    required init(updateInterval : Double, threshold : GeneralState){
        self.updateInterval = updateInterval
        self.threshold = threshold
    }

    //# MARK: - AlarmTriggerable Protocol Methods

    /// Sets the initialState and then compares back to it. This call is asyncnchronous
    func start(){

        if CLLocationManager.locationServicesEnabled(){
            establishInitialState()
            runCompare()
        }
    }
    
    /// Stop/Cancels all operations in queue as well as stalling the
    /// acceleromter updates. It additionally, creates a new semaphore object for the case 
    // where stop killed the blocks execution before the semaphore could be singaled (incremented)
    
    func stop(){

        //stop all in queue
        isCancelled = true
        stateQueue.cancelAllOperations()
        initialState = nil
        stopUpdatingLocation()
        // potentially a thread may be blocked on a semaphore.
        // if the accelerometerQueues are canceled. There is potential one of the
        //threads never is signaled. Signal before resetting semaphore
        self.semaphore.signal()
        clearQueue()
    }

    func reset() {
        //Nee to implement
    }
    func startWithState(initialState: GeneralState) {
        
        if CLLocationManager.locationServicesEnabled(){
            self.initialState = initialState
            runCompare()
        }
    }

    /// Adds to queue a block wwhich uses the startAccelerometerUpdatesToQueue
    /// to calculate the average of the of CMAcceleration struct over a number of
    /// samples data (Specified by numOfInitialStateSamples int). When that many
    /// samples are taken the initialState is set and the sempahore is again incremented
    func establishInitialState() {

        desiredAccuracy = kCLLocationAccuracyBest
        distanceFilter = kCLDistanceFilterNone
        motionManager.startUpdatingLocation()
        let initialStateBlock = BlockOperation{
            var count = 0
            var (latitude, longitude) = (0.0, 0.0)
            while count < self.numberOfInitialStateSamples {
                let coordinate = self.popQueue()
                if self.isCancelled {
                    break
                }
                if let c = coordinate {
                    latitude += c.coordinate.latitude
                    longitude += c.coordinate.longitude
                    count += 1
                    if count == self.numberOfInitialStateSamples {
                        break
                    }
                }
            }
        }
        stateQueue.addOperation(initialStateBlock)
    }
    

    
    /// Method for checking currentState versus initialState
    ///
    /// - Returns: true if initialState and currentState are within threshold. False otherwise
    func isCurrentStateWithinThreshold() -> Bool? {

        return (initialState?.nearlyEqual(currentState!, threshold: threshold))
    }


    //# MARK: - AlarmTriggerable Helper Methods


    /// runCompare adds an Operation to the stateQueue. This operation
    /// adds pulls from the queue a location type, converts it to a
    /// GPSState type and compares that to the initialState
    /// The stateQueue is a serial queue used to synchronize runCompare and establishInitialState.
    /// Additionally the popQueue method has an internal counting semaphore
    /// that will block the runCompare method if the queue is empty.
    func runCompare(){

        motionManager.startUpdatingLocation()
        runStateBlock = BlockOperation{
            
            print("runCompare")
            //decrement semaphore if available. If not wait
            //Only available if initialState has been set and completed.
            //Or this block was arrived at before establishInitialState
            
            if !self.isCancelled{
                if let initialState = self.initialState {
                    while true {
                        if self.isCancelled {
                            break
                        }
                        let location = self.popQueue()
                        if let loc = location {
                            self.currentState = GPSState(latitude: loc.coordinate.latitude, longitude: loc.coordinate.longitude)
                            if let isWithinThreshold = self.isCurrentStateWithinThreshold(){
                                if !isWithinThreshold{
                                    print("initialState = \(initialState.x), \(initialState.y)")
                                    print("currentState = \(String(describing: self.currentState?.x)), \(String(describing: self.currentState?.y))")
                                    self.delegate?.triggered()
                                    self.stopUpdatingLocation()
                                    break
                                }
                            }
                        }
                    }
                }
            }
        }
        stateQueue.addOperation(runStateBlock!)
    }

    /// Stops GPS reading
    func stopUpdatingLocation() {
        motionManager.stopUpdatingLocation()
        delegate?.stoppedReading()
    }

    /// Adds to queue list of CLLocation objects to queue
    /// and synchronously signals semaphore
    ///
    /// - Parameter newValue: [CLLocation] objects which are each added to queue.
    func putQueue(_ newValue : [CLLocation]){
        /*
         Adds to the queue
         */
        locationQueueUpdate.sync{
            for _ in 1...newValue.count {
                self.semaphore.signal()
            }
            self.locationQueue += newValue
        }
    }
    
    /// Pops from queue the first item in the list. This
    /// method is meant to insure thant the queue is updated in a safe thread
    /// manner.
    ///
    /// - Returns: nil if nothing in queue otherwise returns first item in queue as well as
    ///            removing it from queue
    func popQueue() -> CLLocation? {

        var returnValue : CLLocation?
        locationQueueUpdate.sync{
            _ = self.semaphore.wait(timeout: DispatchTime.distantFuture)
            if self.locationQueue.count == 0 {
                returnValue = nil
            }
            else {
                returnValue = self.locationQueue.remove(at: 0)
            }
        }
        return returnValue
    }
    
    /// Emties queue and creates new semaphore
    func clearQueue(){

        locationQueueUpdate.sync{
            self.semaphore = DispatchSemaphore(value: 0)
            self.locationQueue = []
            
        }
    }

    //# MARK: - CLLocationManagerDelegate Methods

    /// A demanded method for CLLocationMangerDelegate
    ///
    /// - Parameters:
    ///   - manager: CLLoationManger
    ///   - locations: [CLLocation] list passing updated CLLocation
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        putQueue(locations)
    }

    /// A demanded method for CLLocationMangerDelegate
    ///
    /// - Parameters:
    ///   - manager: CLLoationManger
    ///   - locations: Error indicating issue
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
    }
}
