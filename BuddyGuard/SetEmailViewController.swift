//
//  SetEmailViewController.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 7/16/17.
//  Copyright © 2017 Stewart Boyd. All rights reserved.
//

import UIKit
//import EAIntroView

class SetEmailViewController: UIViewController, UITextFieldDelegate {
    
    var viewControllers: [UIViewController]!
    var blurEffectView: UIVisualEffectView? = nil
    var blurEffect: UIBlurEffect!
    var completedCallback: (()->())? = nil
    var currentPage = 0
    var pendingViewController: UIViewController!
    
    let settingsInfo = SettingsInfo.singleton
    let notificationCenter = NotificationCenter.default
    
    @IBOutlet weak var emailAddressTextField: UITextField!
    @IBOutlet weak var dummyView: UIView!
    @IBOutlet weak var adjustableConstraint2: NSLayoutConstraint!

    // MARK: - View Controller Overrides
    override func viewDidLoad() {
        emailAddressTextField.delegate = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        notificationCenter.addObserver(self, selector: #selector(keyboardWillShowNotification(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        notificationCenter.addObserver(self, selector: #selector(keyboardWillHideNotification(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        notificationCenter.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        notificationCenter.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    
    @IBAction func completePressed(_ sender: Any) {
        loadNextStoryboard()
    }
    
    func keyboardWillShowNotification(notification: NSNotification) {
        updateBottomLayoutConstraintWithNotification(notification: notification)
    }
    
    func keyboardWillHideNotification(notification: NSNotification) {
        updateBottomLayoutConstraintWithNotification(notification: notification)
    }
    
    func updateBottomLayoutConstraintWithNotification(notification: NSNotification) {
        let userInfo = notification.userInfo!
        
        let animationDuration = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        let keyboardEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let convertedKeyboardEndFrame = view.convert(keyboardEndFrame, from: view.window)
        let rawAnimationCurve = (notification.userInfo![UIKeyboardAnimationCurveUserInfoKey] as! NSNumber).uint32Value << 16
        let animationCurve = UIViewAnimationOptions.init(rawValue: UInt(rawAnimationCurve))
        let options = [UIViewAnimationOptions.beginFromCurrentState, animationCurve]
        adjustableConstraint2.constant = view.bounds.maxY - convertedKeyboardEndFrame.minY
        
        //UIView.animate(withDuration: <#T##TimeInterval#>, delay: <#T##TimeInterval#>, usingSpringWithDamping: <#T##CGFloat#>, initialSpringVelocity: <#T##CGFloat#>, options: <#T##UIViewAnimationOptions#>, animations: <#T##() -> Void#>, completion: <#T##((Bool) -> Void)?##((Bool) -> Void)?##(Bool) -> Void#>)
        UIView.animate(withDuration: animationDuration, delay: 0.0, options: .beginFromCurrentState, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    // MARK: - Text Field Delegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("should Return")
        emailAddressTextField.resignFirstResponder()
        settingsInfo.email = textField.text
        loadNextStoryboard()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        emailAddressTextField.resignFirstResponder()
        settingsInfo.email = textField.text
    }
    
    func loadNextStoryboard() {
        
        //Will Set Use Of Email if no email address specified
        if (settingsInfo.email == nil) {
            settingsInfo.useEmail = false
        }
        DispatchQueue.main.async {
            let mainStoryboard = UIStoryboard(name: GlobalString.MainStoryboard, bundle: nil)
            let mainViewController = mainStoryboard.instantiateViewController(withIdentifier: GlobalString.MainNavigationController) as! UINavigationController
            self.present(mainViewController, animated: true)
        }
    }
}

