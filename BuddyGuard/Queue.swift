//
//  Queue.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 9/18/16.
//  Copyright © 2016 Stewart Boyd. All rights reserved.
//

import Foundation

class Queue <T> {
    var queue: [T] = []
    var syncQueue: DispatchQueue
    var length : Int {
        var len : Int?
        syncQueue.sync {
            len = queue.count
        }
        return len!
    }

    init(syncQueue: DispatchQueue?) {
        if let queue = syncQueue {
            self.syncQueue = queue
        }
        else {
            self.syncQueue = DispatchQueue(label: "synchroniation Queue")
        }
    }

    func push(item: T) {
        syncQueue.sync {
            queue.append(item)
        }
    }

    func pop() -> T {
        var item: T?
        syncQueue.sync {
            item = queue.remove(at: 0)
        }
        return item!
    }

    func currentQueue() -> [T] {
        var items: [T]?
        syncQueue.sync {
            items = self.queue
        }
        return items!
    }
}
