//
//  MasterViewController.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 1/9/17.
//  Copyright © 2017 Stewart Boyd. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import EAIntroView

class CameraAccessViewController: UIViewController {
    

    
    @IBOutlet weak var introView: EAIntroView!

    override func viewDidLoad() {
        introView.delegate = self
        print("MasaterViewController.viewDidLoad")
        let lightPurple = UIColor(red: 102.0/255.0, green: 102.0/255.0, blue: 255.0/255.0, alpha: 1)
        let lightBlue = UIColor(red: 102.0/255.0, green: 178.0/255.0, blue: 255.0/255.0, alpha: 1)
        let lightGreen = UIColor(red: 51.0/255.0, green: 255.0/255.0, blue: 153.0/255.0, alpha: 1)
        let page0 = EAIntroPage()
        page0.title = "Welcome to Buddy Guard!"
        page0.desc = "Let us explain why we'd like access to some of your phone's features"
        page0.titleIconView = UIImageView(image: UIImage(named: "Alarm"))
        page0.bgColor = lightPurple
        //page0.titleColor = UIColor.black
        //page0.descColor = UIColor.black
        
        let page1: EAIntroPage = EAIntroPage()
        page1.title = "Capture Image of Perp"
        page1.desc = "Only on alarm activation, let Buddy Guard use your camera to capture images of surroundings"
        page1.bgColor = lightBlue
        page1.titleIconView = UIImageView(image: UIImage(named: "photo-camera"))


        //let page2: EAIntroPage = EAIntroPage()
        //page2.title = "BuddyGuard would like access to your phones camera"
        //page2.titleIconView = UIImageView(image: UIImage(named: "photo-camera"))
        //page2.bgColor = lightGreen
        let page3 = EAIntroPage()

        introView.skipButton.isHidden = true
        introView.pages = [page0, page1, page3]
    }

    func loadNextStoryboard() {
        DispatchQueue.main.async {
            let welcomeStoryboard = UIStoryboard(name: GlobalString.WelcomeStoryboard, bundle: nil)
            let locationAccessViewController = welcomeStoryboard.instantiateViewController(withIdentifier: GlobalString.PushNotificationsAccessViewController) as! PushNotificationsAccessViewController
            self.present(locationAccessViewController, animated: true)
        }
    }
}

//# Mark: - EAIntroViewDelegate Methods
extension CameraAccessViewController: EAIntroDelegate {


    func intro(_ introView: EAIntroView!, pageAppeared page: EAIntroPage!, with pageIndex: UInt) {
        let pageCount: UInt = UInt(introView.pages.count)
        //on last page show access dialog
        if pageCount - 1 == pageIndex {
            print("last page appeared")
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo) {
                didComplete in
                self.loadNextStoryboard()
            }
        }
        
    }

}
