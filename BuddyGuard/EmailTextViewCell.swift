//
//  EmailTextViewCell.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 4/30/17.
//  Copyright © 2017 Stewart Boyd. All rights reserved.
//

import UIKit
import Validator

class EmailTextViewCell: ValidatorTextViewCell {

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var UISwitch: UISwitch!
    @IBAction func valueChanged(_ sender: UISwitch) {
        _valueChanged?(sender, self)
    }
    var _textFieldDidEndEditing: ((UITextField, EmailTextViewCell) -> ())?
    var _actionButtonClicked: (() ->())?
    var _valueChanged: ((_ sender: UISwitch, _ cell: EmailTextViewCell) -> ())? = nil

    var validationRuleSet: ValidationRuleSet<String>? {
        didSet { textField.validationRules = validationRuleSet }
    }

    var delegate: UITextFieldDelegate? {
        get { return textField.delegate }
        set { textField.delegate = newValue }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        textField.validateOnInputChange(enabled: true)
        textField.validationHandler = { result in self.updateValidationState(result: result) }
    }
    
    override func prepareForReuse() {
        textField.text = ""
    }

    override func actionButtonClicked(_ sender: Any) {
        super.actionButtonClicked(sender)
        _actionButtonClicked?()

    }
}


extension EmailTextViewCell: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {

        print("called")
        _textFieldDidEndEditing?(textField, self)
        
        //textField.resignFirstResponder()
    }
}

