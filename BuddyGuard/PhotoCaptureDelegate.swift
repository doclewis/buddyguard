//
//  PhotoCaptureDelegate.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 12/31/16.
//  Copyright © 2016 Stewart Boyd. All rights reserved.
//

import Foundation

protocol PhotoCapturableDelegate {
    func snappedStillImage(imageData: Data?)
    func sessionStarted(didStart: Bool)
    func sessionStopped(didStop: Bool)
}
