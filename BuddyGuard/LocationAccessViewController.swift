//
//  MasterViewController.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 1/9/17.
//  Copyright © 2017 Stewart Boyd. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import EAIntroView

class LocationAccessViewController: UIViewController {
    
    var viewControllers: [UIViewController]!
    var blurEffectView: UIVisualEffectView? = nil
    var blurEffect: UIBlurEffect!
    var completedCallback: (()->())? = nil
    var currentPage = 0
    var pendingViewController: UIViewController!
    
    @IBOutlet weak var bottomButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var introView: EAIntroView!
    
    override func viewDidLoad() {
        introView.delegate = self
        print("MasaterViewController.viewDidLoad")
        let page1: EAIntroPage = EAIntroPage();
        page1.title = "Buddy Guard,  can help do shit if it can access your location";
        page1.desc = "This is the heelloworld page"
        page1.bgColor = UIColor.white
        page1.titleIconView = UIImageView(image: UIImage(named: "location"))
        page1.titleColor = UIColor.black
        page1.descColor = UIColor.black
        let page2: EAIntroPage = EAIntroPage()
        page2.title = "BuddyGuard would like access to your phones camera"
        page2.titleIconView = UIImageView(image: UIImage(named: "location"))
        let page3 = EAIntroPage()
        introView.pages = [page1, page2, page3]
        introView.skipButton.isHidden = true
    }
    func loadNextStoryboard() {
        DispatchQueue.main.async {
            let welcomeStoryboard = UIStoryboard(name: GlobalString.WelcomeStoryboard, bundle: nil)
            let locationAccessViewController = welcomeStoryboard.instantiateViewController(withIdentifier: GlobalString.PushNotificationsAccessViewController) as! PushNotificationsAccessViewController
            self.present(locationAccessViewController, animated: true)
        }
    }
    
}

//# Mark: - EAIntroViewDelegate Methods
extension LocationAccessViewController: EAIntroDelegate {
    
    func intro(_ introView: EAIntroView!, pageAppeared page: EAIntroPage!, with pageIndex: UInt) {
        let pageCount: UInt = UInt(introView.pages.count)
        //on last page show access dialog
        if pageCount - 1 == pageIndex {
            print("last page appeared")
            self.loadNextStoryboard()
        }
        
    }
    //- (void)introWillFinish:(EAIntroView *)introView wasSkipped:(BOOL)wasSkipped;
    //- (void)introDidFinish:(EAIntroView *)introView wasSkipped:(BOOL)wasSkipped;
    //- (void)intro:(EAIntroView *)introView pageAppeared:(EAIntroPage *)page withIndex:(NSUInteger)pageIndex;
    //- (void)intro:(EAIntroView *)introView pageStartScrolling:(EAIntroPage *)page withIndex:(NSUInteger)pageIndex;
    //- (void)intro:(EAIntroView *)introView pageEndScrolling:(EAIntroPage *)page withIndex:(NSUInteger)pageIndex;
    
    
}
