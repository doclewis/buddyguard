//
//  Perceptable.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 12/31/16.
//  Copyright © 2016 Stewart Boyd. All rights reserved.
//

import Foundation

protocol  Perceptable {
    func start()
    func startOnDelay()
    func stop()
}
