            //
//  PhotoCapture.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 8/21/16.
//  Copyright © 2016 Stewart Boyd. All rights reserved.
//

import Foundation
import AVFoundation
import Photos


enum PhotoCaptureError: Error {
    case noAvailableDevices
}

enum SetupResult {
    case success
    case cameraNotAuthorized
    case configurationFailed
    case noCameraDevices
}
            
            
class PhotoCapture {
    var capturePhotoOutput: AVCapturePhotoOutput
    var photoSettings: AVCapturePhotoSettings
    var photoCaptureDelegate: AVCapturePhotoCaptureDelegate
    let session = AVCaptureSession()
    var backCameraDevice: AVCaptureDevice?
    var frontCameraDevice: AVCaptureDevice?
    var otherCameraDevice: AVCaptureDevice?
    var backCameraDeviceInput: AVCaptureDeviceInput?
    var frontCameraDeviceInput: AVCaptureDeviceInput?
    var otherCameraDeviceInput: AVCaptureDeviceInput?
    var currentCameraPosition: AVCaptureDevicePosition?
    let sessionQueue: DispatchQueue
    var setupResult: SetupResult = .success
    var delegate: PhotoCapturableDelegate?
    var isStarted: Bool = false

    var isStopped: Bool {
        return !isStarted
    }

    var isDeviceAvailable: Bool {
        if let _ = backCameraDevice {
            return true
        }
        else if let _ = frontCameraDevice {
            return true
        }
        else if let _ = otherCameraDevice {
            return true
        }
        else {
            return false
        }
    }

    var connection: AVCaptureConnection? {
        return capturePhotoOutput.connection(withMediaType: AVMediaTypeVideo)
    }

    var hasOutputConnection: Bool {
        return connection != nil
    }

    init(photoCaptureDelegate: AVCapturePhotoCaptureDelegate) throws{
        /*
         Populates attributes for camera devices if available. If none
         available then throws a PhotoCaptureError exception. Additionally,
         creates output and adds to session. Creates inputs but does not add them to session
         */
        self.photoCaptureDelegate = photoCaptureDelegate
        photoSettings = AVCapturePhotoSettings(format: [AVVideoCodecKey : AVVideoCodecJPEG])
        capturePhotoOutput = AVCapturePhotoOutput()
        sessionQueue = DispatchQueue(label: "Photo Capture Session Queue")
        checkAuthorizationStatus()

    }

    func checkAuthorizationStatus() {
        switch AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) {
        case .authorized:
            //The user has previously granted access to the camear
            self.setupResult = .success
        case .notDetermined:
            // The user has not yet been presented with the option to grant video access.
            // We suspend the session queue to delay session setup until the access request has completed to avoid
            // asking the user for audio access if video access is denied.
            // Note that audio access will be implicitly requested when we create an AVCaptureDeviceInput for audio during session setup.
            sessionQueue.suspend()
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo) { isGranted in
                if !isGranted {
                    self.setupResult = .cameraNotAuthorized
                }
                self.sessionQueue.resume()
            }
        default:
            //The user has previously denied access
            setupResult = .cameraNotAuthorized
        }
    }

    func setOutput(output: AVCapturePhotoOutput) {
        self.sessionQueue.async {
            if self.session.canAddOutput(output) {
                self.session.addOutput(output)
            }
            else {
                print("couldnt add Output")
            }
        }
    }

    func autoSetCameras() throws{

        let deviceDiscoverySession = AVCaptureDeviceDiscoverySession(deviceTypes: [.builtInWideAngleCamera], mediaType: AVMediaTypeVideo, position: .unspecified)
        guard let devices = deviceDiscoverySession?.devices else {
            print("no devices found")
            setupResult = .noCameraDevices
            throw PhotoCaptureError.noAvailableDevices
        }
        for device in devices {
            setCamera(device: device)
        }

    }

    func setCamera(device: AVCaptureDevice) {
        switch device.position {
        case .back:
            backCameraDevice = device
            backCameraDeviceInput = try? AVCaptureDeviceInput(device: backCameraDevice)
        case .front:
            frontCameraDevice = device
            frontCameraDeviceInput = try? AVCaptureDeviceInput(device: frontCameraDevice)
        case .unspecified:
            otherCameraDevice = device
            otherCameraDeviceInput = try? AVCaptureDeviceInput(device: otherCameraDevice)
            print("unspecified camera found")
        }
    }

    func setSessionPreset(preset: String = AVCaptureSessionPresetHigh) {
        sessionQueue.async {
            guard self.session.canSetSessionPreset(preset) else {
                print("Could not set sessionPreset. Use default")
                return
            }
            self.session.sessionPreset = preset
        }
    }

    func defaultConfiguration() throws {

        try autoSetCameras()
        setSessionPreset()
        setOutput(output: capturePhotoOutput)
    }

    func startSession() {
        
        sessionQueue.async {
            self.executeOnSuccessConfig {
                self.session.startRunning()
                self.delegate?.sessionStarted(didStart: true)
                self.isStarted = true
            }
        }
    }

    func stopSession() {
        sessionQueue.async {
            self.executeOnSuccessConfig {
                self.session.stopRunning()
                self.delegate?.sessionStopped(didStop: true)
                self.isStarted = false
            }
        }
    }

    func executeOnSuccessConfig(execBlock: () -> ()) {
        switch self.setupResult {
        case .success:
            print("start session ")
            execBlock()
            print("completed starting session")
        case .cameraNotAuthorized:
            print("not fucking authorized you bitch")
        case .configurationFailed:
            print("unable to capture media")
        case .noCameraDevices:
            print("your shit aint got no devices")
        }
    }

    func addInput(_ cameraPosition: AVCaptureDevicePosition){
        /*
         In general, Removes any inputs from the session before adding
         the input corresponding to the camera position. If the suggested
         cameraPosition is the same as currentCameraPosition then the 
         method immediately returns true

         Returns true if successfully added input. False otherwise
        */
        sessionQueue.async {
            if cameraPosition != self.currentCameraPosition {

                var wasAdded: Bool
                self.session.beginConfiguration()
                let inputCountRemoved = self.removeInputs()
                print("number of inputsRemoved = \(inputCountRemoved)")

                switch cameraPosition {
                case .back:
                    if self.session.canAddInput(self.backCameraDeviceInput) {
                        self.session.addInput(self.backCameraDeviceInput)
                        wasAdded = true
                        self.currentCameraPosition = .back
                    }
                    else {
                        wasAdded = false
                    }
                case .front:
                    if self.session.canAddInput(self.frontCameraDeviceInput) {
                        self.session.addInput(self.frontCameraDeviceInput)
                        wasAdded = true
                        self.currentCameraPosition = .front
                    }
                    else {
                        wasAdded = false
                    }
                case .unspecified:
                    
                    if self.session.canAddInput(self.otherCameraDeviceInput) {
                        self.session.addInput(self.otherCameraDeviceInput)
                        wasAdded = true
                        self.currentCameraPosition = .unspecified
                    }
                    else {
                        wasAdded = false
                    }
                }
                self.session.commitConfiguration()
                print("camera was added sucessfully? \(wasAdded)")
            } else {
                print("camera already correct position")
            }
        }
    }

    func removeInputs() -> Int {
        /*
         Synopsis. Removes set inputs from current session. Should
         be called sandwiched between beginConfigurations and endConfigurations.

         ``Returns`` 
         Count of inputs who were removed
        */
        var count = 0
        if let inputs = self.session.inputs{
            for input in inputs as! [AVCaptureInput] {
                self.session.removeInput(input)
                self.currentCameraPosition = nil
                count += 1
            }
        }
        return count
        
    }

    func imageAllBlack(imageData: Data) -> Bool{
        /*
         Returns true if all pixels of image are black (indicates bad pic taken.
         Else otherwise
        */
        return true
    }

    func snapStillImage() {
        /*
         snapStillImage takes an image with camera and stores it to the
         camera roll
        */
        print("snapStillImage")
        guard isStarted else {
            print("session not started. No image taken")
            return
        }
        guard hasOutputConnection else {
            print("no connection established. No Image taken")
            return
        }
        let photoSettings = AVCapturePhotoSettings(from: self.photoSettings)
        self.sessionQueue.async {
            print("b4 snap")
            self.capturePhotoOutput.capturePhoto(with: photoSettings, delegate: self.photoCaptureDelegate)
            print("after snap")
        }
    }
}
