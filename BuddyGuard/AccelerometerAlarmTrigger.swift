//
//  AccelerometerAlarmTrigger.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 12/31/16.
//  Copyright © 2016 Stewart Boyd. All rights reserved.
//


import UIKit
import CoreMotion
import AVFoundation


class AccelerometerAlarmTrigger : AlarmTriggerable {
    
    //a button press would surpass the trigger threshold with with this sensitivity
    static let highSensitivityThreshold = AccelerometerState(x: 0.05, y: 0.05, z: 0.05)
    //picking up the phone from a stationary point will surpass the trigger threshold with this sensitivty
    static let mediumSensitivityThreshold = AccelerometerState(x: 0.3, y: 0.3, z: 0.3)
    //shaking the phone whilst holding it will suprass the trigger threshold with this sensitivity
    static let lowSensitivityThreshold = AccelerometerState(x: 1.5, y: 1.5, z: 1.5)
    var initialState: GeneralState?
    var currentState: GeneralState?
    var threshold: GeneralState
    var updateInterval: Double
    var delegate: AlarmTriggerableDelegate?
    
    var isCancelled : Bool = false
    var motionManager = CMMotionManager()
    var numberOfInitialStateSamples = 10
    var initialUpdateFrequencyMultiplier = 10.0
    fileprivate var semaphore = DispatchSemaphore(value: 1)
    var initialStateBlock : BlockOperation?
    var runStateBlock : BlockOperation?
    
    lazy var stateQueue: OperationQueue = {
        var stateQueue = OperationQueue()
        stateQueue.name = "State Queue"
        stateQueue.maxConcurrentOperationCount = 1
        return stateQueue
    }()
    
    lazy var calculationsQueue : OperationQueue = {
        var calculationsQueue = OperationQueue()
        calculationsQueue.name = "Accelerometer Updates Queue"
        calculationsQueue.maxConcurrentOperationCount = 1
        return calculationsQueue
    }()
    
    var currentUpdateInterval: Double {
        get { return motionManager.accelerometerUpdateInterval }
        set { motionManager.accelerometerUpdateInterval = newValue }
    }
    
    var initialUpdateInterval : Double {
        return updateInterval / initialUpdateFrequencyMultiplier
    }
    
    /// AccelerometerAlarmTrigger is a class for triggering an Alarm
    /// based off accelerometer data
    ///
    /// - Parameters:
    ///   - updateInterval: How often to take readings from acceleromter
    ///   - threshold: threshold of which to initate
    required init(updateInterval : Double, threshold : GeneralState){
        self.updateInterval = updateInterval
        self.threshold = threshold
    }
    


    //# MARK: - AlarmTriggerable Protocol Methods

    /// Sets the initialState and then compares back to it. This call is asynchronous
    func start(){

        if motionManager.isAccelerometerAvailable {
            establishInitialState()
            runCompare()
        }
    }

    /// Stop/cancels all operations in queue as well as stalling the accelerometer
    /// updates. It additionally creates a new semaphore object for the case where
    /// stop killed the blocks execution before the semephare could be signaled (incremented)
    func stop(){

        if motionManager.isAccelerometerAvailable {
            //stop all in queue
            isCancelled = true
            stateQueue.cancelAllOperations()
            calculationsQueue.cancelAllOperations()
            initialState = nil
            stopAccelerometerUpdates()
            // potentially a thread may be blocked on a semaphore.
            // if the accelerometerQueues are canceled. There is potential one of the
            //threads never is signaled. Signal before resetting semaphore
            self.semaphore.signal()
            semaphore = DispatchSemaphore(value: 1)
        }
    }

    func startWithState(initialState: GeneralState) {
        if motionManager.isAccelerometerAvailable {
            self.initialState = initialState;
            runCompare()
        }
    }
    
    func reset() {
        isCancelled = false
    }
    /// Adds to queue a block wwhich uses the startAccelerometerUpdatesToQueue
    /// to calculate the average of the of CMAcceleration struct over a number of
    /// samples data (Specified by numOfInitialStateSamples int). When that many
    /// samples are taken the initialState is set and the sempahore is again incremented
    func establishInitialState() {

        currentUpdateInterval = initialUpdateInterval
        let initialStateBlock = BlockOperation{
            switch self.semaphore.wait(timeout: DispatchTime.distantFuture) {
            case .success:
                var count = 0
                var (xtotal, ytotal, ztotal) = (0.0, 0.0, 0.0)
                
                self.motionManager.startAccelerometerUpdates(to: self.calculationsQueue){accelerometerData, error in
                    if let accData = accelerometerData {
                        xtotal += accData.acceleration.x
                        ytotal += accData.acceleration.y
                        ztotal += accData.acceleration.z
                        count += 1
                        //print("x = \(xtotal), y = \(ytotal), z = \(ztotal), count = \(count)")
                        if count == self.numberOfInitialStateSamples {
                            self.initialState = AccelerometerState(x: xtotal, y: ytotal, z: ztotal) / Double(count)
                            self.delegate?.initializedInitialState(self.initialState)
                            self.stopAccelerometerUpdates()
                            self.semaphore.signal()
                            //print("setIniitlaSate")
                        }
                    }
                }
            case .timedOut:
                print("establishInitialState. No timeout should happen on semaphore")
            }
        }
        stateQueue.addOperation(initialStateBlock)
    }

    /// Checks initalState versus currentState
    ///
    /// - Returns: true if initialSate and currentState are within threshold. False otherwise
    func isCurrentStateWithinThreshold() -> Bool? {

        return (initialState?.nearlyEqual(currentState!, threshold: threshold))
    }

    
    //# MARK: - AlarmTriggerable Helper Methods

    /// Stops accelerometer updates
    func stopAccelerometerUpdates() {
        motionManager.stopAccelerometerUpdates()
        delegate?.stoppedReading()
    }
    
    /// Adds an Operation to the stateQueue. This operation
    /// ideally adds, to the calculationsQueue a handler function which will
    /// compare back to the set initial state.
    ///
    /// To synchrnoize matters (runCompare cant run at the sametime as estitialState
    /// a semaphore is used. runCompare decrements the semaphore upon ente increments
    /// again immedialtey if the queue has been canceled or no initialStaten set.
    /// If this is not the case then runCompare employs a handler to be caCoreMotion.startAcceleromertUpdatesToQueue
    /// and then incrememts semaphore.
    ///
    /// If no initial state is set the semaphore is signaled (presuming that runCompare was queued
    /// up before
    func runCompare(){
        
        runStateBlock = BlockOperation{
            
            print("runCompare")
            //decrement semaphore if available. If not wait
            //Only available if initialState has been set and completed.
            //Or this block was arrived at before establishInitialState
            switch self.semaphore.wait(timeout: DispatchTime.distantFuture) {
            case .success:
                if !self.isCancelled{
                    if let _ = self.initialState {
                        self.currentUpdateInterval = self.updateInterval
                        self.motionManager.startAccelerometerUpdates(to: self.calculationsQueue, withHandler: self.getCurrentState)
                    }
                } else {
                    print("this shit was canceled")
                }
            case .timedOut:
                print("runCompare timedout. Semaphore shouldnt time out")
            }
            self.semaphore.signal()
        }
        stateQueue.addOperation(runStateBlock!)
    }
    
    //(CMAccelerometerData?, Error?) -> Swift.Void
    /// This method is inteded as a handler for the startAcceleromterUpdatesToQuee method.
    /// It is called at certain intervals perscribed by currentUpdateInterval.
    // Only when isCurrentStateWithinThreshold fails is the delegate method called
    ///
    /// - Parameters:
    ///   - accelerometerData: CMAccelerometerData - used to set currentState
    ///   - error: Error
    func getCurrentState(_ accelerometerData : CMAccelerometerData?, error : Error? ) {

        if let ad = accelerometerData {
            //print("Read from alarm trigger = \(ad)")
            //print(UIApplication.shared.backgroundTimeRemaining)
            currentState = AccelerometerState(accelerometerData: ad)
            if let isWithinThreshold = isCurrentStateWithinThreshold(){
                if !isWithinThreshold{
                    //print("initialState = \(String(describing: self.initialState?.x)), \(String(describing: self.initialState?.y)), \(String(describing: self.initialState?.z))")
                    //print("currentState = \(ad.acceleration.x), \(ad.acceleration.y), \(ad.acceleration.z)")
                    delegate?.triggered()
                    stopAccelerometerUpdates()
                }
            }
        }
    }
}
