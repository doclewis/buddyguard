//
//  alarm.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 5/30/16.
//  Copyright © 2016 Stewart Boyd. All rights reserved.
//

import Foundation
import AVFoundation


class Alarm{

    var soundPlayer: SoundPlayable?
    private var _percepts = [Perceptable]()
    var delay = 0.0; //number of seconds until begin alarm
    var playOnDelayCallback : ((Bool?) -> ())? = nil
    var vibratePercept: VibratePercept?
    var ledPercept: LedPercept?
    fileprivate var timerDelay : Timer?

    var doesLedDisplay: Bool {
        return ledPercept != nil
    }
    
    var doesVibrate: Bool {
        return vibratePercept != nil
    }
    
    var percepts: [Perceptable] {
        get {
            var outPercepts = [Perceptable]()
            if let percept1 = ledPercept {
                outPercepts.append(percept1)
            }
            if let percept2 = vibratePercept {
                outPercepts.append(percept2)
            }
            return outPercepts + _percepts
        }
        set {
            ledPercept = nil
            vibratePercept = nil
            _percepts = newValue
        }
    }
    
    var volume: Float?{
        /*
         Sets volume of audioPlayer. Only allows range
         to be between 0 and 1 inclusive
         */
        
        get{
            return soundPlayer?.volume;
        }
        set{
            if let val = newValue {
                soundPlayer?.volume = val;
            }
        }
    }
    
    var duration: Double?{
        return soundPlayer?.duration
    }

    /// Alarm is an object that contains and AudioPlayer and
    /// list of Percepts which are all started when Alarm starts (or fires)
    ///
    /// - Parameters:
    ///   - audioPlayer: AudioPlayer which will be played on start of ALarm
    ///   - delay: Double Delay in seconds before audioPlayer and Percepts are started
    ///   - percepts: List of objects which implement Perceptable protocol. Started on Alarm start
    init(audioPlayer: SoundPlayable? = nil,
         delay : Double  = 0.0,
         percepts: [Perceptable] = []) {
        
        self.soundPlayer = audioPlayer
        self.delay = delay
        self.percepts = percepts
        
    }

    /// Convenience constructor for Alarm. Allows users to use
    /// some default percepts.
    ///
    /// - Parameters:
    ///   - audioPlayer: AudioPlayer which will be played on start of ALarm
    ///   - delay: Double Delay in seconds before audioPlayer and Percepts are started
    ///   - doesLEDDisplay: Bool which when true will create an LedPercept
    ///   - doesVibrate: Bool which when true will create a vibratePercept
    ///   - runLoop: Some percepts require a runLoop. This allows the client to specify
    convenience init(audioPlayer: SoundPlayable? = nil,
                     delay: Double = 0.0,
                     doesLEDDisplay: Bool,
                     doesVibrate: Bool,
                     runLoop: RunLoop) {

        self.init(audioPlayer: audioPlayer, delay: delay, percepts: [])
        if doesVibrate {
            vibratePercept = Alarm.vibratePerceptConstructor(initialDelay: 0, delayBetweenCalls: 1.0, runLoop: runLoop)
        }
        if doesLEDDisplay {
            ledPercept = Alarm.ledPerceptConstructor(initialDelay: 0, delayBetweenCalls: 0.5, runLoop: runLoop)
        }
    }

    /// Helper method for creating LedPercept
    ///
    /// - Parameters:
    ///   - initialDelay: Double specifying delay (on top of) Alarm delay
    ///   - delayBetweenCalls: Double specifying how often LedPercept should repeat
    ///   - runLoop: RunLoop
    /// - Returns: LedPercept
    static func ledPerceptConstructor(initialDelay: Double, delayBetweenCalls: Double, runLoop: RunLoop)-> LedPercept {
        return LedPercept(timesToCall: Repeater.reapeatIndefinitely,
                          initialDelay: initialDelay,
                          delayBetweenCalls: delayBetweenCalls,
                          runLoop: runLoop)
    }

    /// Helper method for creating VibratePercept
    ///
    /// - Parameters:
    ///   - initialDelay: Double specifying delay (on top of) Alarm delay
    ///   - delayBetweenCalls: Double specifying how often VibratePercept should repeat
    ///   - runLoop: RunLoop
    /// - Returns: VibratePercept
    static func vibratePerceptConstructor(initialDelay: Double, delayBetweenCalls: Double, runLoop: RunLoop)-> VibratePercept {
        return VibratePercept(timesToCall: VibratePercept.repeatIndefinitely,
                                        initialDelay: initialDelay,
                                        delayBetweenCalls: delayBetweenCalls,
                                        runLoop: runLoop)
    }

    /// Method for adding to internal _percepts list.
    ///
    /// - Parameter perceptable: Perceptable object which is to be included in list of internal _percepts
    func addPercept(perceptable: Perceptable) {
        _percepts.append(perceptable)
    }

    
    /// Launches all included percepts
    func startPercepts() {
        for percept in percepts {
            percept.start()
        }
    }

    /// Stops all included percepts
    func stopPercepts() {
        for percept in percepts {
            percept.stop()
        }
    }

    /// Stopping alarm will stop soundPlayer and
    /// all percepts that are currently executing. If
    /// alarm never started will do nothing, but won't
    /// cause exception
    ///
    /// - Returns: if soundPlayer stop was successful
    func stop() -> Bool? {
        stopPlayTimer()
        let didStop = self.soundPlayer?.stop()
        stopPercepts()
        return didStop
    }

    /// Method used for stopping alarm based on condition. If
    /// condition not met the no stop is calledd
    ///
    /// - Parameter willStop: If true will call stop method
    /// - Returns: false if willStop wont allow stop. Otherwise returns
    ///            the results of stop call
    func stopIfTrue(_ willStop : Bool) -> Bool?{

        if willStop{
            return self.stop();
        } else {
            return false;
        }
    }
    
    /// Plays the soundPlayer as well as starts Percepts
    ///
    /// - Returns: Bool? indicating if soundPlayer started succesfully
    func start() -> Bool?{

        print("alarm.play")
        let played = soundPlayer?.play()
        startPercepts()
        return played
    }

    
    /// Unlike start, this method uses the delay attribute to start the
    /// alarm at a specified time in the future
    ///
    /// - Parameter runLoop: RunLoop indicating what run loop to execute on
    func startOnDelay(runLoop: RunLoop = RunLoop.current){

        print("play on delay")
        startPlayTimer(delay: self.delay, runLoop: runLoop)
    }

    /// Plays soundPlayer on delay by creating Timerobject and adding to runLoop
    ///
    /// - Parameters:
    ///   - delay: Delay in seconds before soundPlayer starts
    ///   - runLoop: The runloop whichc to execute the Timer object
    fileprivate func startPlayTimer(delay: Double, runLoop: RunLoop) {

        print("startPlayTimer with delay = \(delay)")
        stopPlayTimer()
        timerDelay = Timer(timeInterval: delay, repeats: false) { timer in
            print("timerDelay has started play")
            let played = self.start()
            self.playOnDelayCallback?(played)
        }
        //timerDelay = Timer(timeInterval: delay, target: self, selector: #selector(Alarm.timerFired(_:)), userInfo: nil, repeats: false)
        if let t = timerDelay {
            print("add to runloop")
            runLoop.add(t, forMode: RunLoopMode.defaultRunLoopMode)
        }
        else {
            print("Could not create timer")
        }
    }

    /// kills the current Timer
    fileprivate func stopPlayTimer() {

        if let currentTimer = timerDelay {
            currentTimer.invalidate()
            timerDelay = nil
        }
    }
}
