

import UIKit
import Validator
import Toast_Swift

struct SectionInfo {

    var index: Int
    var sectionTitle: String?
    var rowCount: Int
    
    
    init(index: Int, sectionTitle: String?, rowCount: Int) {
        self.index = index
        self.sectionTitle = sectionTitle
        self.rowCount = rowCount
    }
}


class Sensitivity {
    var index: Int
    var threshold: GeneralState
    
    init(index: Int, threshold: GeneralState) {
        self.index = index
        self.threshold = threshold
    }
}


struct SensitivityConverter {
    let max: Double
    let min: Double
    //fileprivate var _index: Int = 1
    //fileprivate var _value: Double?
    
    static let sensitivity0 = Sensitivity(index: 0, threshold: AccelerometerAlarmTrigger.lowSensitivityThreshold)
    static let sensitivity1 = Sensitivity(index: 1, threshold: AccelerometerAlarmTrigger.mediumSensitivityThreshold)
    static let sensitivity2 = Sensitivity(index: 2, threshold: AccelerometerAlarmTrigger.highSensitivityThreshold)
    
    
    let sensitivtiesText = ["Low Sensitivity", "Medium Sensitivty", "High Sensitivity"]
    let sensitivities: [Sensitivity]
    
    
    init (max: Double, min: Double) {
        self.max = max
        self.min = min
        self.sensitivities = [SensitivityConverter.sensitivity0,
                              SensitivityConverter.sensitivity1,
                              SensitivityConverter.sensitivity2]
    }
    
    func indexFromVal(_ val: Double) -> Int {
        let increment = (max - min) / Double(sensitivities.count)
        return forceIndexInRange(Int(val / increment))
    }
    
    func valFromIndex(_ index: Int) -> Double {
        return Double(index) * (max - min) / Double(sensitivities.count - 1) + min
    }
    
    func forceIndexInRange(_ index: Int) -> Int {
        return SensitivityConverter.forceInRange(index, max: sensitivities.count - 1, min: 0)
    }
    
    static func forceInRange<T: Comparable>(_ index: T, max: T,  min: T) -> T{
        var newIndex = index
        if index > max {
            newIndex = max
        }
        else if index < min {
            newIndex = min
        }
        return newIndex
    }
}


final class SettingsTableViewController2: UITableViewController {

    var settings = SettingsInfo.singleton
    let alarmOptionsSection = SectionInfo(index: 0, sectionTitle: "Alarm Options", rowCount: 4)
    let timeSection = SectionInfo(index: 1, sectionTitle: "Time [s]", rowCount: 2)
    let sensitivitySection = SectionInfo(index: 2, sectionTitle: nil, rowCount: 1)
    let miscSection = SectionInfo(index: 3, sectionTitle: "Email", rowCount: 2)
    //let serialQueue = DispatchQueue(label: "Serial Queue")
    let emailSender = EmailSender()
    var ledSwitch: UISwitch?
    var useCameraSwitch: UISwitch?


    override func viewDidLoad() {
        super.viewDidLoad()
        emailSender.delegate = self
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100.0
        settings.sensitivityConverter = SensitivityConverter(max: Double(1.0),
                                                             min: Double(0.0))
    }
    
}

// MARK: - Methods for TableView
extension SettingsTableViewController2 {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case alarmOptionsSection.index: return alarmOptionsSection.sectionTitle
        case timeSection.index: return timeSection.sectionTitle
        case sensitivitySection.index: return sensitivitySection.sectionTitle
        case miscSection.index: return miscSection.sectionTitle
        default: return nil
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case alarmOptionsSection.index: return alarmOptionsSection.rowCount
        case timeSection.index: return timeSection.rowCount
        case sensitivitySection.index: return sensitivitySection.rowCount
        case miscSection.index: return miscSection.rowCount
        default: return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell?
        
        switch indexPath.section {
            
        case alarmOptionsSection.index:
            
            let switchCell = tableView.dequeueReusableCell(withIdentifier: "SwitchTableViewCell2", for: indexPath) as! SwitchTableViewCell2
            //stringCell.validationRuleSet = ValidationRuleSet<String>()
            cell = switchCell
            
            switch indexPath.row {
                
            case 0:
                switchCell.titleLabel.text = "LED"
                switchCell.statusLabel.text = nil
                switchCell.uiSwitch.isOn = settings.ledOnAlarm
                switchCell._valueChanged = ledOnAlarmValueChanged
                switchCell.accessoryType = .none
                ledSwitch = switchCell.uiSwitch
            case 1:
                switchCell.titleLabel.text = "Vibrate"
                switchCell.statusLabel.text = nil
                switchCell.uiSwitch.isOn = settings.vibrateOnAlarm
                switchCell._valueChanged = vibrateOnAlarmValueChanged
                switchCell.accessoryType = .none
            case 2:
                switchCell.titleLabel.text = "Uses Camera"
                switchCell.statusLabel.text = nil
                switchCell.uiSwitch.isOn = settings.usePhotoCapture
                switchCell._valueChanged = usesCameraOnAlarm
                setUsesCameraDisclosure(cell: switchCell, doesUseCamera: settings.usePhotoCapture)
                useCameraSwitch = switchCell.uiSwitch
            case 3:
                switchCell.titleLabel.text = "Use Touch ID"
                switchCell.statusLabel.text = nil
                switchCell.uiSwitch.isOn = settings.useTouchID
                switchCell._valueChanged = useTouchIDValueChanged
                switchCell.accessoryType = .none
                cell = switchCell
            default:
                break
            }
            
        case timeSection.index:
            
            let stepCell = tableView.dequeueReusableCell(withIdentifier: "StepTableViewCell2", for: indexPath) as! StepTableViewCell2
            //numericCell.validationRuleSet = ValidationRuleSet<Float>()
            cell = stepCell
            
            switch indexPath.row {
                
            case 0:
                stepCell.titleLabel.text = "Until Armed"
                stepCell.statusLabel.text = "\(settings.timeUntilAlarmArms)"
                stepCell.stepper.value = settings.timeUntilAlarmArms
                stepCell._valueChanged = timeUntilArmedStepValueChanged
            case 1:
                stepCell.titleLabel.text = "Sound"
                stepCell.statusLabel.text = "\(settings.timeUntilAlarmSounds)"
                stepCell.stepper.value = settings.timeUntilAlarmSounds
                stepCell._valueChanged = timeUntilSoundStepValueChanged
            default:
                break
            }
        case sensitivitySection.index:
            let sliderCell = tableView.dequeueReusableCell(withIdentifier: "SliderTableViewCell2", for: indexPath) as! SliderTableViewCell2
            cell = sliderCell
            switch indexPath.row {
            case 0:
                sliderCell.titleLabel.text = "Sensitivity"
                sliderCell.statusLabel.text = nil
                sliderCell.slider.value = Float(settings.sensitivity)
                sliderCell.slider.addTarget(self, action: #selector(self.sensitivitySliderTouchUpInside(_:)), for: .touchUpInside)
            default:
                sliderCell.titleLabel.text = nil
                
            }
        case miscSection.index:
            
            switch indexPath.row {
                
            case 0:

                let emailCell = tableView.dequeueReusableCell(withIdentifier: "EmailTextViewCell", for: indexPath) as! EmailTextViewCell
                emailCell.titleLabel.text = nil
                emailCell.UISwitch.isOn = settings.useEmail
                emailCell._valueChanged = useEmailValueChanged
                
                    
                emailCell.actionButton.setTitle("Send Test Email", for: .normal)
                emailCell._actionButtonClicked = sendEmail
                let emailPattern = EmailValidationPattern.simple
                let emailRule = ValidationRulePattern(pattern: emailPattern, error: ValidationError(message: "😫"))
                emailCell.validationRuleSet = ValidationRuleSet<String>()
                emailCell.validationRuleSet?.add(rule: emailRule)
                emailCell.textField.text = settings.email
                if let result = settings.email?.validate(rule: emailRule) {
                    emailCell.updateValidationState(result: result)
                } else {
                    emailCell.updateValidationState(result: .valid)
                }
                
                if !emailCell.UISwitch.isOn{
                    emailCell.textField.isHidden = true
                    emailCell.actionButton.isHidden = true
                    emailCell.statusLabel.isHidden = true
                }
                emailCell.accessoryType = .none
                emailCell.delegate = emailCell
                emailCell._textFieldDidEndEditing = emailChanged
                cell = emailCell
            case 1:
                let updatedPWButtonCell = tableView.dequeueReusableCell(withIdentifier: "ButtonTableViewCell", for: indexPath) as! ButtonTableViewCell
                if hasPassword {
                    updatedPWButtonCell.button.setTitle("Update Password", for: .normal)
                    updatedPWButtonCell._buttonClicked = {
                        self.performSegue(withIdentifier: GlobalString.segueFromSettingsToConfirmPW, sender: self)
                    }
                } else {
                    updatedPWButtonCell.button.setTitle("Create Password", for: .normal)
                    updatedPWButtonCell._buttonClicked = {
                        self.performSegue(withIdentifier: GlobalString.SegueSettings2CreatePW, sender: self)
                    }
                }

                cell = updatedPWButtonCell
            default:
                break
            }
            
            
        default:
            break
        }
        
        return cell!
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.section == alarmOptionsSection.index && indexPath.row == 2 {
            if settings.usePhotoCapture {
                self.performSegue(withIdentifier: GlobalString.SegueCameraCaptureToSettings, sender: self)
            }
        }
        else if indexPath.section == miscSection.index && indexPath.row == 1 {
            //self.performSegue(withIdentifier: "toTestEmailSegue", sender: self)
        }
        
    }
    

}

//# MARK: - Methods for updating Settings
extension SettingsTableViewController2 {

    
    func ledOnAlarmValueChanged(_ sender: UISwitch, _ cell: SwitchTableViewCell2) {
        
        print("ledOnAlarmValueChanged -> \(sender.isOn)")
        settings.ledOnAlarm = sender.isOn
        if (settings.ledOnAlarm && settings.usePhotoCapture) {
            useCameraSwitch?.isOn = false
            settings.usePhotoCapture = false
            let ledUseCameraToastText = "Cannot Set LED and Use Camera"
            view.window?.makeToast(ledUseCameraToastText, duration: 1.0, position: ToastPosition.bottom)
        }
    }
    
    func vibrateOnAlarmValueChanged(_ sender: UISwitch, _ cell: SwitchTableViewCell2) {
        print("vibrateOnAlarmValueChanged -> \(sender.isOn)")
        settings.vibrateOnAlarm = sender.isOn
    }

    func usesCameraOnAlarm(_ sender: UISwitch, _ cell: SwitchTableViewCell2) {
        print("usesCameraOnAlarm -> \(sender.isOn)")
        settings.usePhotoCapture = sender.isOn
        setUsesCameraDisclosure(cell: cell, doesUseCamera: settings.usePhotoCapture)
        //if set usePhotoCapture to on and the led is also on then turn off led and
        //adjust settings
        if (settings.usePhotoCapture && settings.ledOnAlarm) {
            ledSwitch?.isOn = false
            settings.ledOnAlarm = false
            let ledUseCameraToastText = "Cannot Set LED and Use Camera"
            view.window?.makeToast(ledUseCameraToastText, duration: 1.0, position: ToastPosition.bottom)
        }

    }

    func setUsesCameraDisclosure(cell: SwitchTableViewCell2, doesUseCamera: Bool) {
        if doesUseCamera {
            cell.accessoryType = .disclosureIndicator
        } else {
            cell.accessoryType = .none
        }

    }
    func timeUntilArmedStepValueChanged(_ sender: UIStepper, timeUntilArmedLabel: UILabel) {
        print("timeUntilArmedStepValueChanged -> \(sender.value)")
        settings.timeUntilAlarmArms = sender.value
        timeUntilArmedLabel.text = "\(settings.timeUntilAlarmArms)"
    }

    func timeUntilSoundStepValueChanged(_ sender: UIStepper, timeUntilSoundLabel: UILabel) {
        print("timeUntilSoundStepValueChanged -> \(sender.value)")
        settings.timeUntilAlarmSounds = sender.value
        timeUntilSoundLabel.text = "\(settings.timeUntilAlarmSounds)"
    }

    func sensitivitySliderTouchUpInside(_ sender: UISlider) {
        print("sensitivitySliderTouchUpInside")
        settings.sensitivity = Double(sender.value)
        let index = settings.sensitivityConverter.indexFromVal(settings.sensitivity)
        let sensitvity = settings.sensitivityConverter.sensitivities[index]
        let sensitivityString = settings.sensitivityConverter.sensitivtiesText[index]
        let toastText = "Set to \(sensitivityString)"
        sender.value = Float(settings.sensitivityConverter.valFromIndex(index))
        view.window?.makeToast(toastText, duration: 1.0, position: ToastPosition.bottom)
        print("completed sensitivitySliderTouch")
    }

    func useTouchIDValueChanged(_ sender: UISwitch, _ cell: SwitchTableViewCell2) {
        print("useTouchIDValueChanged -> \(sender.isOn)")
        settings.useTouchID = sender.isOn
        //enforceUseTouchIDRule(doesUseTouchID: sender.isOn)
    }
    
    func useEmailValueChanged(_ sender: UISwitch, _ cell: EmailTextViewCell) {
        print("useEmailValueChanged -> \(sender.isOn)")
        settings.useEmail = sender.isOn
        if sender.isOn {
            cell.textField.isHidden = false
            cell.actionButton.isHidden = false
            cell.statusLabel.isHidden = false
        }
        else{
            cell.textField.isHidden = true
            cell.actionButton.isHidden = true
            cell.statusLabel.isHidden = true

            
        }
    }

    func emailChanged(_ sender: UITextField, _ cell: EmailTextViewCell) {
        print("emailChanged -> \(String(describing: sender.text))")
        var isValid = false
        let potentialEmail = sender.text
        if let validationRuleSet = cell.validationRuleSet {
            if let validation = potentialEmail?.validate(rules: validationRuleSet) {
                switch validation {
                case .valid:
                    print("email was valid and was set")
                    isValid = true
                    settings.email = potentialEmail!
                default:
                    print("email not valid and NOT set")
                }
            } else {
                print("validation was nil")
            }
        } else {
            print("no validation rules in set")
        }
        cell.actionButton.isEnabled = isValid
    }

    func sendEmail() {
        if let to = SettingsInfo.singleton.email {
            
            let fpathPreamble = Bundle.main.path(forResource: "test_preamble", ofType: "html")
            let fpathMain = Bundle.main.path(forResource: "goto_find_my_iphone", ofType: "html")
            let bodyPreamble = Utility.Read(fpath: fpathPreamble)!
            let bodyMain = Utility.Read(fpath: fpathMain)!
            DispatchQueue.global(qos: .default).async {
                _ = self.emailSender.send(to: to, subject: "Test - Buddy Guard Alarm Triggered", body: bodyPreamble + bodyMain)
                
            }
        }
        print("sendEmail")
    }

}

extension SettingsTableViewController2: EmailSenderDelegate {
    func successCallback(result: String?) {
        view.makeToast("Test Email was sent")
    }

    func failureCallback(error: Error?) {
        view.makeToast("Email Send Error")
    }

    var hasPassword: Bool {
        //probably best to just save pw to "default" as user doesn't need personal account.
        return UserDefaults.standard.object(forKey: "default") != nil
    }
}
