//
//  State.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 8/21/16.
//  Copyright © 2016 Stewart Boyd. All rights reserved.
//

import Foundation
import CoreMotion
import CoreLocation

class GeneralState : Equatable, CustomStringConvertible{
    var x : Double
    var y : Double
    var z : Double
    
    /// General State allows for the definition of three
    /// doubles. Perfect for definiing state of a physical space
    ///
    /// - Parameters:
    ///   - x: Double
    ///   - y: Double
    ///   - z: Double
    required init(x : Double, y : Double, z : Double){
        self.x = x
        self.y = y
        self.z = z
    }
    
    var description : String{
        return "GeneralState( x = \(x), y = \(y), z = \(z))"
    }
    
    
    /// Determines equality given a certain threshold
    ///
    /// - Parameters:
    ///   - to: GeneralState - Object to compare to
    ///   - threshold: GeneralState - Object which the "to" object and "self" object must be within
    /// - Returns: Bool - True if the comparison was within threshold false otherwise
    func nearlyEqual(_ to: GeneralState, threshold : GeneralState) -> Bool{

        return nearlyEqual(to, thresholdX: threshold.x, thresholdY: threshold.y, thresholdZ: threshold.z)
    }
    
    /// Determines equality given a certain threshold (as defined by three Doubles
    ///
    /// - Parameters:
    ///   - to: GeneralState - Object to compare to
    ///   - thresholdX: Double: Threshold for x
    ///   - thresholdY: Double - Threshold for y
    ///   - thresholdZ: Double - Threshold for z
    /// - Returns: Bool - True if the comparison was within threshold false otherwise
    func nearlyEqual(_ to: GeneralState, thresholdX : Double, thresholdY: Double, thresholdZ: Double) -> Bool{
        
        func isWithinThreshold(_ a : Double, b: Double, threshold : Double) -> Bool{
            return (b - threshold <= a && a <= b + threshold)
        }
        
        return (isWithinThreshold(x, b: to.x, threshold: thresholdX) &&
            isWithinThreshold(y, b: to.y, threshold: thresholdY) &&
            isWithinThreshold(z, b: to.z, threshold: thresholdZ))
    }
    
    /// Determines equality give a certain threshold (as defined by a Double) which wil be used for
    /// all three parameters
    ///
    /// - Parameters:
    ///   - to: GeneralState - Object to compare to
    ///   - threshold: Double - Threshold for x, y and z
    /// - Returns: Bool - True if the comparison was within threshold false otherwise
    func nearlyEqual(_ to: GeneralState, threshold: Double) -> Bool{
        return nearlyEqual(to, thresholdX: threshold, thresholdY: threshold, thresholdZ: threshold)
    }
}


class AccelerometerState : GeneralState {
    
    required init(x: Double, y: Double, z: Double) {
        super.init(x: x, y: y, z: z)
    }
    
    /// Convenience Constructor for defining state based on CMAccelerator data
    ///
    /// - Parameter acceleration: CMAcceleration
    convenience init(acceleration : CMAcceleration){
        self.init(x: acceleration.x, y: acceleration.y, z: acceleration.z)
    }
    
    /// Convenience Constructor
    ///
    /// - Parameter accelerometerData: CMAccelerometerData
    convenience init(accelerometerData : CMAccelerometerData){
        self.init(acceleration: accelerometerData.acceleration)
    }
}


class GPSState : GeneralState {

    /// Alias for x
    var latitude : Double {
        get { return x }
        set { x = newValue }
    }

    /// Alias for y
    var longitude : Double {
        get { return y }
        set { y = newValue }
    }
    
    required init(x: Double, y: Double, z: Double) {
        super.init(x: x, y: y, z: 0.0)
    }
    
    /// Convenicnce constructor for GPS Data that is only dependent on 
    /// 2 independent values
    ///
    /// - Parameters:
    ///   - latitude: <#latitude description#>
    ///   - longitude: <#longitude description#>
    convenience init(latitude : Double, longitude : Double){
        self.init(x: latitude, y: longitude, z: 0.0)
    }
    
}

//# MARK: - Operator Method Definitions

func + <T: GeneralState>(left : T, right : T) -> T{
    
    return T(x: left.x + right.x, y: left.y + right.y, z: left.z + right.z)
}

func + <T: GeneralState>(left : T, right : Double) -> T{
    
    return T(x: left.x + right, y: left.y + right, z: left.z + right)
}

func - <T: GeneralState>(left : T, right : T) -> T{
    
    return T(x: left.x - right.x, y: left.y - right.y, z: left.z - right.z)
}

func - <T: GeneralState>(left : T, right : Double) -> T{
    
    return T(x: left.x - right, y: left.y - right, z: left.z - right)
}

func / <T: GeneralState>(left : T, right : T) -> T{
    
    return T(x: left.x / right.x, y: left.y / right.y, z: left.z / right.z)
}

func / <T: GeneralState>(left : T, right : Double) -> T{
    
    return T(x: left.x / right, y: left.y / right, z: left.z / right)
}

func == <T: GeneralState>(left : T, right : T) -> Bool{
    
    if left.x != right.x{
        return false
    }
    else if left.y != right.y {
        return false
    }
    else if left.z != right.z {
        return false
    }
    else{
        return true
    }
}
