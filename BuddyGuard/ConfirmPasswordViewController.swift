//
//  ConfirmPasswordViewController.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 6/3/17.
//  Copyright © 2017 Stewart Boyd. All rights reserved.
//

import UIKit
import LocalAuthentication

class ConfirmPasswordViewController: UIViewController, PasswordSubstantiatable {
    
    let MyKeychainWrapper = KeychainWrapper()
    let notificationCenter = NotificationCenter.default
    var numberpadViewController: NumberpadViewController!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var bottomLayout: NSLayoutConstraint!

    override func viewDidLoad() {
        numberpadViewController.passwordLength = 4
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
   }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == GlobalString.ConfirmPasswordNumberpadContainer){
            numberpadViewController = (segue.destination as! NumberpadViewController)
            numberpadViewController.verificationAuthorized = verificationAuthorized
            numberpadViewController.attemptPassword = authenticateTextPassword(text:)
            
        }

    }
        
    func beganConfirmPassWord(_ textField: UITextField) {
        
    }
    
    func finishedConfirmPassWord(_ textField: UITextField) {
        
    }

    /// Gets pwField text and verifies entered password
    func processTextPasswordEntry() {
        confirmPasswordTextField.resignFirstResponder()
        authenticateTextPassword(text: confirmPasswordTextField.text)
    }

    func authenticateTextPassword(text: String?) -> Bool {
        if(text == MyKeychainWrapper.myObject(forKey: "v_Data") as? String){
            verificationAuthorized()
            return true
        }
        else{
            verificationRejected()
            return false
        }
    }
    
    func verificationAuthorized() {
        //Goto
        performSegue(withIdentifier: GlobalString.SegueConfirmPW2CreatePW, sender: self)
    }
    
    func verificationRejected() {
        SimpleAnimation.shake(view: view)
    }
}
