//
//  PasswordSubstantiatable.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 6/4/17.
//  Copyright © 2017 Stewart Boyd. All rights reserved.
//

import Foundation

protocol PasswordSubstantiatable {
    func verificationAuthorized()
    func verificationRejected()
}
