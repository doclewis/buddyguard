//
//  SimpleAnimation.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 6/4/17.
//  Copyright © 2017 Stewart Boyd. All rights reserved.
//

import UIKit
 

class SimpleAnimation {

    
    static func pulseAnimation(duration: CFTimeInterval = 1, fromValue: Double = 0.7, toValue: Double = 1) -> CABasicAnimation {
        return setAnimation(animation: CABasicAnimation(keyPath: "opacity"), duration: duration, fromValue: fromValue, toValue: toValue)
    }

    static func scaleAnimationX(duration: CFTimeInterval = 1, fromValue: Double = 0.97, toValue: Double = 1.03) -> CABasicAnimation {
        return setAnimation(animation: CABasicAnimation(keyPath: "transform.scale.x"), duration: duration, fromValue: fromValue, toValue: toValue)
    }

    static func scaleAnimationY(duration: CFTimeInterval = 1, fromValue: Double = 0.97, toValue: Double = 1.03) -> CABasicAnimation {
        return setAnimation(animation: CABasicAnimation(keyPath: "transform.scale.y"), duration: duration, fromValue: fromValue, toValue: toValue)
    }
    /// Creates an animation where the screen shakes
    static func shake(view: UIView){
        let anim = CAKeyframeAnimation( keyPath:"transform" )
        anim.values = [
            NSValue( caTransform3D:CATransform3DMakeTranslation(-5, 0, 0 ) ),
            NSValue( caTransform3D:CATransform3DMakeTranslation( 5, 0, 0 ) )
        ]
        anim.autoreverses = true
        anim.repeatCount = 2
        anim.duration = 7/100
        view.layer.add( anim, forKey:nil )
    }

    static func setAnimation(animation: CABasicAnimation, duration: CFTimeInterval, fromValue: Double, toValue: Double) -> CABasicAnimation{
        animation.duration = duration
        animation.fromValue = fromValue
        animation.toValue = toValue
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        animation.autoreverses = true
        animation.repeatCount = .greatestFiniteMagnitude
        return animation
        
    }
}
