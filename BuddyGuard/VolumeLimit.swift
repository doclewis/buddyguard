//
//  VolumeLimit.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 12/19/16.
//  Copyright © 2016 Stewart Boyd. All rights reserved.
//

import Foundation

class VolumeLimit{
    static let maxVolume : Float = 1.0
    static let minVolume : Float = 0.0
    
    
    class func valueIsInRange(_ value: Float) -> Float {
        if value < minVolume { return minVolume }
        if value > maxVolume{ return maxVolume }
        return value
    }
    
    class func valueIsInRange(_ value : Double) -> Double {
        return Double(VolumeLimit.valueIsInRange(Float(value)))
    }
}

