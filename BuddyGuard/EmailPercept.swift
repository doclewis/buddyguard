//
//  EmailPercept.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 4/29/17.
//  Copyright © 2017 Stewart Boyd. All rights reserved.
//

import Foundation

class EmailPercept: Perceptable {
    
    var sender: EmailSender?
    var to: String
    var subject: String
    var body: String

    init(sender: EmailSender?, to: String, subject: String, body: String) {
        self.sender = sender
        self.to = to
        self.subject = subject
        self.body = body
    }

    func start() {
        print("EmailPercept.send")
        _ = sender?.send(to: to, subject: subject, body: body)
    }
    
    func stop() {
        //does nothing 
    }
    
    func startOnDelay() {
        //does nothing
    }
}
