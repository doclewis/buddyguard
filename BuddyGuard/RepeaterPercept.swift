//
//  Perceptable.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 12/19/16.
//  Copyright © 2016 Stewart Boyd. All rights reserved.
//

import Foundation
import Photos


class RepeaterPercept: Perceptable {
    static let repeatIndefinitely = Repeater.reapeatIndefinitely
    internal let repeater: Repeater
    
    init(timesToCall: Int, initialDelay: Double, delayBetweenCalls: Double, runLoop: RunLoop, completedCallback: ((Bool) -> ())? = nil) {
        repeater = Repeater(timesToCall: timesToCall,
                                 delayBetweenCalls: delayBetweenCalls,
                                 initialDelay: initialDelay,
                                 runLoop: runLoop,
                                 completedCallback: completedCallback,
                                 functionToCall: nil
                                 )
        
            
    }
    func start() {
        repeater.start()
    }
    
    func startOnDelay() {
        repeater.initialDelay = repeater.delayBetweenCalls
        repeater.start()
    }
    
    internal func stop() {
        repeater.stop()
    }
}
