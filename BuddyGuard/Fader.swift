//
//  Fader.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 8/21/16.
//  Copyright © 2016 Stewart Boyd. All rights reserved.
//

import Foundation
import AVFoundation

class Fader : Repeater{
    
    static let defaultFadeDurationSeconds = 3.0
    static let defaultVelocity = 2.0
    static let defaultVolumeAlterationsPerSecond = 30.0
    static let defaultNumberOfCalls = 90
    static let defaultDelayBetweenCalls = 1.0 / Fader.defaultVolumeAlterationsPerSecond
    let player: AVAudioPlayer?
    var fromVolume : Float
    var toVolume : Float
    var velocity : Double = Fader.defaultVelocity

    var isFadeCompleted: Bool {
        return numberOfCalls >= timesToCall
    }
    
    var volumeAlterationsPerSecond : Double {
        return 1.0 / delayBetweenCalls
    }
    
    var fadeCompletedCallback : ((Bool)->())? {
        return completedCallback
    }

    /// Fader is a subclass of Repeater. It is used to adjust the volume
    /// of an AVAudioPlayer
    ///
    /// - Parameters:
    ///   - player: AVAudioPlayer - object to have sound level adjusted
    ///   - fromVolume: Float - Initial Volume to start Fade level at
    ///   - toVolume: Float - Final volume to end Fade level at
    ///   - timesToCall: Int - Number of times to call repeater
    ///   - delayBetweenCalls: Double - delay between updates to fader volume
    ///   - velocity: Double - There's an equation that defines the curve for the fade. This is a parameter of it
    ///   - runLoop: RunLoop
    ///   - fadeCompletedCallback: Callback executed upon completion of fader
    init(player: AVAudioPlayer, fromVolume: Float = 0, toVolume: Float = 1,
         timesToCall : Int, delayBetweenCalls : Double,
         velocity: Double = Fader.defaultVelocity,
         runLoop: RunLoop = RunLoop.current,
         fadeCompletedCallback: ((Bool)->())? = nil) {
        
        self.player = player
        self.fromVolume = VolumeLimit.valueIsInRange(fromVolume)
        self.toVolume = VolumeLimit.valueIsInRange(toVolume)
        self.velocity = velocity
        //functionToCall is an instance method and cant be connected
        //at initialization.
        super.init(timesToCall: timesToCall, delayBetweenCalls: delayBetweenCalls,
                   runLoop: runLoop,
                   completedCallback: fadeCompletedCallback,
                   functionToCall: nil)
    }

    /// Sets the functionToCall to the updateVolume method
    /// and starts the repaeter
    func fade() {
        print("fade")
        self.player?.volume = Float(self.fromVolume)
        functionToCall = updateVolume
        //Nothing to do if fromVolume == toVolume except calling callback
        if self.fromVolume == self.toVolume {
            self.fadeCompletedCallback?(true)
        }
        else{
            start()
        }
    }

    /// Method called by repeater. Used to update the volume based on 
    /// class attributes
    ///
    /// - Parameters:
    ///   - numCall: Int - call number
    ///   - totalNumCall: Int - total of number times to call
    func updateVolume(_ numCall : Int, totalNumCall : Int){
        if isFadeCompleted{
            player?.volume = Float(self.toVolume)
            stopTimers()
            self.fadeCompletedCallback?(true)
            return
        }
        let currentTimeFrom0To1 = self.completionStatus0to1()
        let volumeMultiplier = Fader.fadeInVolumeMultiplier(currentTimeFrom0To1, velocity: velocity)
        let newVolume = self.fromVolume + (self.toVolume - self.fromVolume) * volumeMultiplier
        self.player?.volume = Float(newVolume)
        //print("player.volume = \(player?.volume)")
    }
    
    /// Helper which gets the completion percentage as deciaml
    ///
    /// - Returns: completion percentage as decimal
    func completionStatus0to1() -> Double{
        
        /*
         
         ``Returns``
         
         The percentage completion state of the current timer. Guaranteed
         to be between 0 and 1
         */
        var result = Double(numberOfCalls) / Double(timesToCall)
        result = VolumeLimit.valueIsInRange(result)
        return result
    }
    
    // Graph:
    /// Function which defines fader and technically gives volume
    /// based on "time" or completion state
    /// See the following for equation: https://www.desmos.com/calculator/wnstesdf0h
    ///
    /// - Parameters:
    ///   - timeFrom0To1: Double - completion percentage as decimal
    ///   - velocity: parameter that defines exponential
    /// - Returns: Float - volume as a function of completion percentage
    class func fadeInVolumeMultiplier(_ timeFrom0To1: Double, velocity: Double) -> Float {
        let time = Double(VolumeLimit.valueIsInRange(Float(timeFrom0To1)))
        return Float(pow(M_E, velocity * (time - 1)) * time)
    }
}
