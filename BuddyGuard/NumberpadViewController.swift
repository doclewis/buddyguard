//
//  NumberpadViewController.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 7/2/17.
//  Copyright © 2017 Stewart Boyd. All rights reserved.
//

import UIKit
import LocalAuthentication


class NumberpadViewController: UIViewController {
    
    private var currentString = ""
    //private var numberOfDots: Integer = 4
    var attemptPassword: ((String) -> (Bool))?
    var verificationAuthorized: (() -> ())?
    @IBOutlet weak var verticalStackView: UIStackView!
    @IBOutlet weak var dottedPreview: DottedPreview!
    @IBOutlet weak var oneButton: UIButton!
    @IBOutlet weak var twoButton: UIButton!
    @IBOutlet weak var threeButton: UIButton!
    @IBOutlet weak var fourButton: UIButton!
    @IBOutlet weak var fiveButton: UIButton!
    @IBOutlet weak var sixButton: UIButton!
    @IBOutlet weak var sevenButton: UIButton!
    @IBOutlet weak var eightButton: UIButton!
    @IBOutlet weak var nineButton: UIButton!
    @IBOutlet weak var zeroButton: UIButton!
    @IBOutlet weak var touchIdButton: UIButton!
    
    
    var passwordLength: Int {
        get {
            return dottedPreview.numberOfDots
        }
        set {
            dottedPreview.numberOfDots = newValue
        }
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.layer.backgroundColor = UIColor.clear.cgColor
        dottedPreview.dotsNumberChangeAnimationDuration = 0.1
        dottedPreview.progressChangeAnimationDuration = 0.1
        dottedPreview.pauseBetweenConsecutiveAnimations = 0.0
        formatButton(button: oneButton)
        formatButton(button: twoButton)
        formatButton(button: threeButton)
        formatButton(button: fourButton)
        formatButton(button: fiveButton)
        formatButton(button: sixButton)
        formatButton(button: sevenButton)
        formatButton(button: eightButton)
        formatButton(button: nineButton)
        formatButton(button: zeroButton)
        
    }
    
    func formatButton(button: UIButton) {
        button.layer.backgroundColor = UIColor.white.withAlphaComponent(0.0).cgColor
        //button.layer.backgroundColor = UIColor.blue.cgColor
        let f = CGRect(x: 0, y: 0, width: button.frame.width / 2.0, height: button.frame.height / 2.0)
        let v = UIView(frame: f)
        v.translatesAutoresizingMaskIntoConstraints = false
        v.isUserInteractionEnabled = false
        button.addSubview(v)
        v.centerXAnchor.constraint(equalTo: button.centerXAnchor).isActive = true
        v.centerYAnchor.constraint(equalTo: button.centerYAnchor).isActive = true
        v.heightAnchor.constraint(equalTo: button.heightAnchor, multiplier: 0.5).isActive = true
        v.widthAnchor.constraint(equalTo: button.widthAnchor, multiplier: 0.5).isActive = true
        v.layer.borderWidth = 2.0
        v.layer.cornerRadius = button.frame.width / 4
        v.layer.borderColor = UIColor.black.cgColor
    }

    @IBAction func oneTouched(_ sender: Any) {
        numberTouched(val: "1")
    }
    @IBAction func twoTouched(_ sender: Any) {
        numberTouched(val: "2")
    }
    @IBAction func threeTouched(_ sender: Any) {
        numberTouched(val: "3")
    }
    @IBAction func fourTouched(_ sender: Any) {
        numberTouched(val: "4")
    }
    @IBAction func fiveTouched(_ sender: Any) {
        numberTouched(val: "5")
    }
    @IBAction func sixTouched(_ sender: Any) {
        numberTouched(val: "6")
    }
    @IBAction func sevenTouched(_ sender: Any) {
        numberTouched(val: "7")
    }
    @IBAction func eightTouched(_ sender: Any) {
        numberTouched(val: "8")
    }
    @IBAction func nineTouched(_ sender: Any) {
        numberTouched(val: "9")
    }
    @IBAction func zeroTouched(_ sender: Any) {
        numberTouched(val: "0")
    }
    @IBAction func touchIDTouched(_ sender: Any) {
    }

    @IBAction func deleteTouched(_ sender: Any) {
        removeTouched()
    }

    func removeTouched() {

        print("old currentString = \(currentString)")
        let lasti = currentString.characters.count - 1
        if lasti >= 0 {
            let index = currentString.index(currentString.startIndex, offsetBy: lasti)
            let x = currentString.substring(to: index)
            currentString = x
            print("set to \(lasti)")
            dottedPreview.setProgress(lasti, animated: true)
        }


    }
    func numberTouched(val: String) {

        
        currentString += val
        //on second to last entry, need to check password
        if currentString.characters.count >= passwordLength {
            print("attempt password")
            print("\(currentString)")
            dottedPreview.setProgress(currentString.characters.count, animated: true)
            if attemptPassword?(currentString) == true {
                
            }
        } else {
            dottedPreview.setProgress(currentString.characters.count, animated: true)
            print("keep going")
            
        }
    }
    @IBAction func useTouchIDTouched(_ sender: Any) {
        
        let touchIDManager = TouchIDManager()
        
        touchIDManager.authenticateUser(success: { () -> () in
            DispatchQueue.main.async {
                print("success bitch")
                self.verificationAuthorized?()
            }
            
        }, failure: { (evaluationError: NSError) -> () in
            switch evaluationError.code {
            case LAError.Code.systemCancel.rawValue:
                print("Authentication cancelled by the system")
            //self.statusLabel.text = "Authentication cancelled by the system"
            case LAError.Code.userCancel.rawValue:
                print("Authentication cancelled by the user")
                
            //self.statusLabel.text = "Authentication cancelled by the user"
            case LAError.Code.userFallback.rawValue:
                print("User wants to use a password")
                
            case LAError.Code.touchIDNotEnrolled.rawValue:
                print("TouchID not enrolled")
                
            case LAError.Code.passcodeNotSet.rawValue:
                print("Passcode not set")
            default:
                print("Authentication failed")
            }
        })
    }
}
