
import UIKit
import Validator


struct ValidationError: Error {
    
    public let message: String
    
    public init(message m: String) {
        message = m
    }
}


class ValidatorTextViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!
    
    @IBAction func actionButtonClicked(_ sender: Any) {
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func updateValidationState(result: ValidationResult) {
        switch result {
        case .valid:
            statusLabel.text = "Email Valid! ✅"
        case .invalid(_):
            statusLabel.text = "Email Invalid! ❌"
        }
    }
    
}
