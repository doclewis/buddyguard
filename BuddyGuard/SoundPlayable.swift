//
//  SoundMaker.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 12/11/16.
//  Copyright © 2016 Stewart Boyd. All rights reserved.
//

import Foundation

protocol SoundPlayable {
    var volume: Float {get set}
    var duration: Double {get}
    func play() -> Bool
    func stop() -> Bool
}
