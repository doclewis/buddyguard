//
//  MainViewController.swift
//  BuddyGuard
//
//  Created by Justin Salazar on 5/26/16.
//  Copyright © 2016 Stewart Boyd. All rights reserved.
//

import UIKit
import AVFoundation
import CoreMotion


class MainViewController: UIViewController ,UINavigationControllerDelegate, AVAudioPlayerDelegate, UIPickerViewDataSource, UIPickerViewDelegate {


    
    let MyKeychainWrapper = KeychainWrapper()
    let settings = SettingsInfo.singleton
    let alarmDataSource = AlarmAudioDataSource.data
    @IBOutlet weak var armButton: UIButton!
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var alarmAudioDisplay: UITextField!
    @IBOutlet weak var notForProdGoToCreatePw: UIButton!
    
    var alarmPicker: UIPickerView!
    var isPlayingTest = false

    var hasPw: Bool {
        //probably best to just save pw to "default" as user doesn't need personal account.
        return UserDefaults.standard.object(forKey: "default") != nil
    }
    
    // MARK: - UIViewController Override Methods

    override func viewDidLoad() {
        super.viewDidLoad()
        notForProdGoToCreatePw.isHidden = true
        //Configure the Audio Session
        let dataSource = AlarmAudioDataSource.getData(by: settings.alarmAudioFilename)
        AudioSessionConfigurator().defaultConfigure()
        alarmAudioDisplay.text = dataSource?.altName
        
        UIApplication.shared.beginReceivingRemoteControlEvents()
        // Do any additional setup after loading the view.
        navigationController?.delegate = self
        alarmPicker = UIPickerView()
        alarmPicker.delegate = self
        alarmPicker.dataSource = self
        alarmAudioDisplay.inputView = alarmPicker
        let doneToolbar = KeyboardToolbarFactory.create(title: "Select", target: self, selector: #selector(selectAlarmAudio))
        alarmAudioDisplay.inputAccessoryView = doneToolbar

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        //self.navigationController?.isNavigationBarHidden = false;
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //self.navigationController?.isNavigationBarHidden = true;
    }

    override func viewDidAppear(_ animated: Bool) {

        armButton.layer.add(SimpleAnimation.pulseAnimation(), forKey: "animateOpacity")
        armButton.layer.add(SimpleAnimation.scaleAnimationX(), forKey: "animateScaleX")
        armButton.layer.add(SimpleAnimation.scaleAnimationY(), forKey: "animateScaleY")
    }

    @IBAction func lockButtonClicked(_ sender: UIButton) {
        
        //create alert password not created.
        
        if !hasPw{
            performSegue(withIdentifier: GlobalString.segueToCreatePassword, sender: self)
        } else {
            performSegue(withIdentifier: GlobalString.segueToArmLock, sender: self)
        }
    }

    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
    }

    @IBAction func unwindToMain(_ segue: UIStoryboardSegue){}
    
    @IBAction func unwindToMainDisarm(_ segue: UIStoryboardSegue){}

    // MARK: - Select Alarm Audio

    func selectAlarmAudio() {
        let row = alarmPicker.selectedRow(inComponent: 0)
        let dataSource = alarmDataSource[row]
        alarmAudioDisplay.text = dataSource.altName
        settings.alarmAudioFilename = dataSource.name
        settings.alarmAudioFilenameExtension = dataSource.ex
        alarmAudioDisplay.resignFirstResponder()
    }


    // MARK: - Test SOund
    
    @IBAction func testSound(_ sender: Any) {
        if !isPlayingTest {
            let fpath = Bundle.main.path(forResource: settings.alarmAudioFilename, ofType: settings.alarmAudioFilenameExtension)
            if let path = fpath {
                let audioPlayer = try? AudioPlayer(filepath: path,
                                                   fromVolume: 1,
                                                   toVolume: 1,
                                                   numberOfLoops: 0)
                if let ap = audioPlayer {
                    isPlayingTest = true
                    ap.audioPlayer.delegate = self
                    ap.play()
                    view.makeToast("Test Alarm")
                }
            } else {
                print("\(settings.alarmAudioFilename) does not exist")
            }
        } else {
            print("Already Playing a Test.")
        }
    }

    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer,
                                     successfully flag: Bool) {
        if flag {
            isPlayingTest = false
        }
    }
    
    // MARK: - UIPicker Data Source and Delegeta Methods
    @available(iOS 2.0, *)
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return alarmDataSource.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return alarmDataSource[row].altName
    }
}
