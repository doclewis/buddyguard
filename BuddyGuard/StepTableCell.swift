//
//  StepTableCell.swift
//  BuddyGuard
//
//  Created by Stewart Boyd on 10/10/16.
//  Copyright © 2016 Stewart Boyd. All rights reserved.
//


import UIKit

@IBDesignable
open class StepperTableCell: UITableViewCell {
    var label: UILabel = UILabel()
    var stepLabel: UILabel = UILabel()
    var stepper: UIStepper = UIStepper()
    
    @IBInspectable open var isCollapsable: Bool = true {
        didSet {
            print("isCollabpsable is set = \(isCollapsable)")
        }
    }
    
    @IBInspectable open var labelText: String = "Default" {
        didSet {
            label.text = labelText
        }
    }
    
    @IBInspectable open var value: Double = 1 {
        didSet {
            stepper.value = value
        }
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
    }
    
    required public init(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)!
        stepper.isUserInteractionEnabled = true
        label.translatesAutoresizingMaskIntoConstraints = false
        stepLabel.translatesAutoresizingMaskIntoConstraints = false
        stepper.translatesAutoresizingMaskIntoConstraints = false
        
    }
    
    override open func layoutSubviews() {
        
        super.layoutSubviews()
        
        contentView.addSubview(label)
        contentView.addSubview(stepLabel)
        contentView.addSubview(stepper)
        
        label.heightAnchor.constraint(equalTo: self.layoutMarginsGuide.heightAnchor, multiplier: 0.9)
        label.leadingAnchor.constraint(equalTo: self.layoutMarginsGuide.leadingAnchor).isActive = true
        label.centerYAnchor.constraint(equalTo: self.layoutMarginsGuide.centerYAnchor).isActive = true

        stepLabel.heightAnchor.constraint(equalTo: self.layoutMarginsGuide.heightAnchor, multiplier: 0.9)
        stepLabel.trailingAnchor.constraint(greaterThanOrEqualTo: stepper.leadingAnchor, constant: -10).isActive = true
        stepLabel.centerYAnchor.constraint(equalTo: self.layoutMarginsGuide.centerYAnchor).isActive = true
        
        stepper.heightAnchor.constraint(equalTo: self.layoutMarginsGuide.heightAnchor, multiplier: 0.9)
        stepper.trailingAnchor.constraint(equalTo: self.layoutMarginsGuide.trailingAnchor).isActive = true
        stepper.centerYAnchor.constraint(equalTo: self.layoutMarginsGuide.centerYAnchor).isActive = true
    }
}

