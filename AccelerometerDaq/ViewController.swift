//
//  ViewController.swift
//  GetAcc
//
//  Created by Stewart Boyd on 7/11/16.
//  Copyright © 2016 Stewart Boyd. All rights reserved.
//

import CoreMotion
import UIKit
import MessageUI

import UIKit

extension String {
    func appendLineToURL(fileURL: NSURL) throws {
        try self.stringByAppendingString("\n").appendToURL(fileURL)
    }
    
    func appendToURL(fileURL: NSURL) throws {
        let data = self.dataUsingEncoding(NSUTF8StringEncoding)!
        try data.appendToURL(fileURL)
    }
}

extension NSData {
    func appendToURL(fileURL: NSURL) throws {
        if let fileHandle = try? NSFileHandle(forWritingToURL: fileURL) {
            defer {
                fileHandle.closeFile()
            }
            fileHandle.seekToEndOfFile()
            fileHandle.writeData(self)
        }
        else {
            try writeToURL(fileURL, options: .DataWritingAtomic)
        }
    }
}

class ViewController: UIViewController, MFMailComposeViewControllerDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var fileNameTextField: UITextField!
    @IBOutlet weak var mainButton: NSLayoutConstraint!
    @IBOutlet weak var mainButton1: UIButton!
    @IBOutlet weak var sendEmailButton: UIButton!
    
    let mailComposer = MFMailComposeViewController()
    lazy var textToWrite = [NSIndexPath:NSOperation]()
    var queue = NSOperationQueue()
    let fileManager = NSFileManager.defaultManager()
    
    
    let mainButtonText1 = "Click to Start Reading Accelerometer Data"
    let mainButtonText2 = "Click to Save Data"
    let MyMotionManager = CMMotionManager()
    var recordData = true
    var buttonRecord = true
    var fileName = "accelerometerData.csv"
    var filePath : NSURL?
    let accelerometerUpdateInterval = 0.01
    var start : NSDate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //fileNameTextField.hide
        mainButton1.setTitle(mainButtonText1, forState: .Normal)
        MyMotionManager.accelerometerUpdateInterval = accelerometerUpdateInterval
        determineFileName()
        fileNameTextField.delegate = self
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func determineFileName(){
        //only set filename if text field filled out
        if let text = fileNameTextField.text{
            //as long as the text field isn't blank proceed to update it
            if text != "" {
                fileName = "\(text).csv"
            }
        }
        if let dir = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.AllDomainsMask, true).first {
            filePath = NSURL(fileURLWithPath: dir).URLByAppendingPathComponent(fileName)
        }
        else{
            print("filePath not constructured")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func mainButton1Click(sender: UIButton) {
        print("buttonClicked")
        if buttonRecord{
            startRecording()
        }
        else{
            stopRecording()
        }
        buttonRecord = !buttonRecord
        print("buttonFinished Click")
    }
    
    @IBAction func sendEmail(sender: UIButton) {
        //Check to see the device can send email.
        if( MFMailComposeViewController.canSendMail() ) {
            print("Can send email.")
            
            
            mailComposer.mailComposeDelegate = self
            
            //Set the subject and message of the email
            mailComposer.setSubject("Have you heard a swift?")
            mailComposer.setMessageBody("This is what they sound like.", isHTML: false)
            
            if let url = filePath {
                
                print("File path loaded.")
                print("\(url)")
                
                if fileManager.fileExistsAtPath(url.path!) {
                    print("FILE AVAILABLE")
                } else {
                    print("FILE NOT AVAILABLE")
                }
                if let fileData = NSData(contentsOfFile: url.path!) {
                    print("File data loaded.")
                    mailComposer.addAttachmentData(fileData, mimeType: "text/csv", fileName: fileName)
                }
                else{
                    print("FileData not loaded")
                }
            }
            self.presentViewController(mailComposer, animated: true, completion: nil)
        }
    }
    
    func deleteFilePath(fpath : String){
        do {
            try fileManager.removeItemAtPath(fpath)
        }
        catch let error as NSError {
            print("Ooops! Something went wrong: \(error)")
        }
    }
    
    func mailComposeController(controller: MFMailComposeViewController,
                               didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        // Check the result or perform other tasks.
        
        // Dismiss the mail compose view controller.
        if let e = error {
            print("error")
            print(e)
        }
        else{
            print("NO Error")
        }
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    func writeAccData(accData : CMAccelerometerData?, e : NSError?) -> Void{
        let time = NSDate()
        let interval = time.timeIntervalSinceDate(start!)
        let ad = MyMotionManager.accelerometerData!
        let text = "\(interval), \(ad.acceleration.x), \(ad.acceleration.y),\(ad.acceleration.z)\n"
        print(text)
        writeText(text)
    }
    
    func writeText(textToWrite : String){
        print("writeText")
        if let path = filePath{
            let fileManager = NSFileManager.defaultManager()
            print(path.path)
            print(path.absoluteString)
            if fileManager.fileExistsAtPath(path.path!){
                print("FILE AVAILABLE")
                try! textToWrite.appendToURL(path)
            } else {
                print("FILE NOT AVAILABLE")
                try! textToWrite.writeToURL(path, atomically: false, encoding: NSUTF8StringEncoding)
            }
            
            if fileManager.fileExistsAtPath(path.path!) {
                print("FILE AVAILABLE")
            } else {
                print("FILE NOT AVAILABLE")
            }
        }
        else{
            print("Error with filepath")
        }
        print("finishWriteText")
    }
    
    func startRecording(){
        print("startRecording")
        determineFileName()
        start = NSDate()
        writeText("Time, Acc X, Acc Y, Acc Z\n")
        if MyMotionManager.accelerometerAvailable {
            
            MyMotionManager.startAccelerometerUpdatesToQueue(queue, withHandler: self.writeAccData)
        }
        mainButton1.setTitle(mainButtonText2, forState: .Normal)
        
        
    }
    func stopRecording(){
        print("stopRecording")
        mainButton1.setTitle(mainButtonText1, forState: .Normal)
        recordData = false;
        if MyMotionManager.accelerometerAvailable {
            MyMotionManager.stopAccelerometerUpdates()
        }
    }
    
}
